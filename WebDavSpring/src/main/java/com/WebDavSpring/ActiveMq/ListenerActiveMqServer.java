package com.WebDavSpring.ActiveMq;


import com.WebDavSpring.ActiveMq.support.EndPointArrayList;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;


/**
 * Created by А д м и н on 19.08.2019.
// */
////запускается самым последним после инитиализации всех обьектов
//    //прослушиваем данные событие и подписываемся на ActiveMq канал
@Component
public class ListenerActiveMqServer implements ApplicationListener<ContextRefreshedEvent>
{


    @Autowired
    private EndPointArrayList point;

    @Autowired
    private EndPointListener listener;

    @Value("${user.login.admin.insert}")
    private String[] adminLogin;

    @Value("${user.password.admin.insert}")
    private String[] passwordLogin;

    @Autowired
    ServiceUser mysqlServiceUser;

    @Autowired
    PasswordEncoder passwordEncoder;

    public void Start()
    {
        //добавляем в массив endpoint
        point.setEndPointToArraqyList("Admin-Changes-Users" , "VirtualTopic.Commit_Changes_Users");
        point.setEndPointToArraqyList("Lazy-Admin-Changes-Users" , "VirtualTopic.Lazy_Changes_Users");
        point.setEndPointToArraqyList("Admin-Advisory-Users" , "ActiveMQ.Advisory.Connection");
        point.setEndPointToArraqyList("Admin-HttpServer" , "VirtualTopic.HttpServer");


        //Добавляем слушателей ко всему списку
        listener.updateListener();
        //Обновляем ListenerEndPoint у Spring JMS
        listener.updateEndPoint();
        checkAndInsertAdminTobase(adminLogin , passwordLogin);


       // listener.testBrowser();

    }



    private void checkAndInsertAdminTobase(String[] usernameArr , String[] passwordArr)
    {
        for(int f = 0 ; f < usernameArr.length; f++)
        {
            if(usernameArr[f] != null)
            {
                String username = usernameArr[f];
                String password = passwordArr[f];

                boolean userModel = mysqlServiceUser.existsByUsername(username);

                if(userModel == false)
                {

                    //  String date = "2016-08-22 14:30";
                    UsersModel newAdmin = new UsersModel();
                    newAdmin.setUsername(username);
                    newAdmin.setPassword(passwordEncoder.encode(password));
                    newAdmin.setPhone("9126340354");
                    newAdmin.setEmail("gawric@mail.ru");
                    newAdmin.setAuthdate(LocalDateTime.now().toString());
                    newAdmin.setRegdate(LocalDateTime.now().toString());
                    newAdmin.setRole(getRole(username));

                    mysqlServiceUser.saveUsers(newAdmin);
                    System.out.println("User "+username+" добавлен в базу данных");
                }
            }
        }

    }

    private String getRole(String username)
    {
        if(username.equals("admin"))
        {
            return "admin";
        }
        else
        {
            return "user";
        }

    }









    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        Start();
    }
}