package com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq;

import com.WebDavSpring.ActiveMq.queue.QueueRunnablePriorityWorkerToClient;
import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import java.util.concurrent.ConcurrentHashMap;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;

/**
 * Created by А д м и н on 19.08.2019.
// */
@Component
public class SendMessage {

    @Autowired
    private QueueRunnablePriorityWorkerToClient queueClient;

    @Autowired
    private JmsTemplate template;

    @Value("${amqclient.ssl.key}")
    private String key;

    public void sendFileInfoObject(FileInfoObjectTopic infoObjectTopic, String destinationName)
    {
        String endpoint = "VirtualTopic."+destinationName;
        send( infoObjectTopic,  endpoint);
    }

    public void sendAnswerGenLink(FileInfoObjectTopic infoObjectTopic, String destinationName)
    {
        String endpoint = "VirtualTopic."+destinationName;
        send( infoObjectTopic,  endpoint);
    }



    private void send(FileInfoObjectTopic infoObjectTopic, String endPoint)
    {
        //очень важно клонировать обьект !!!!
        FileInfoObjectTopic infoObjectTopicClone = (FileInfoObjectTopic)infoObjectTopic.clone();
        //такие запросы как Actual передаются в TextMessage[1];
        String parties = infoObjectTopicClone.getTextmessage()[4];

        if(parties == null)
        {
            parties = infoObjectTopicClone.getTextmessage()[1];
        }

        PriorityQueueRunnableClientModel priorityQueueRunnableClientModel = new PriorityQueueRunnableClientModel( getPriorityRequest(parties) ,  parties ,  infoObjectTopicClone);
        priorityQueueRunnableClientModel.setDestinationName(endPoint);

        queueClient.addDataToQueue(priorityQueueRunnableClientModel);
        priorityQueueRunnableClientModel = null;
    }

    public void sendFileInfoObjectToHttpServer(FileInfoObjectTopic infoObjectTopic, String destinationName)
    {

        String endpoint = "VirtualTopic."+destinationName;
        System.out.println("Конечная точка "+endpoint);
        template.convertAndSend(endpoint , infoObjectTopic);

    }

    public void sendLazyFileInfoObject(FileInfoObjectTopic infoObjectTopic, String destinationName)
    {
        String endpoint = "VirtualTopic.Lazy."+destinationName;
        System.out.println("Конечная точка "+endpoint);
        template.convertAndSend(endpoint , infoObjectTopic);
    }

    private int getPriorityRequest(String parties)
    {

        ConcurrentHashMap<String , Integer> allRequest = staticVariable.getAllRequestPriority();

        if(allRequest != null)
        {
            //если запрос не известен то мы даем самый низкий приоритет
            if(allRequest.containsKey(parties))
            {
                return allRequest.get(parties);
            }
            else
            {
                return 9;
            }
        }
        else
        {
            return 9;
        }

    }



}
