package com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq;

import com.WebDavSpring.ActiveMq.thread.support.syncCreateAllListFolder.SyncCreateAllList;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.model.syncCreateModel.SyncCreateAllListModel;
import com.WebDavSpring.WebDavSpring.model.syncSinglListModel.SyncSinglRequestModel;
import com.WebDavSpring.WebDavSpring.model.wokingModel.SyncModel;
import org.apache.tomcat.jni.FileInfo;

/**
 * Created by А д м и н on 07.05.2020.
 */
public class TrainingSendMessage {

    public void getListSynCreateAllList(SyncCreateAllListModel alllistModel)
    {
        long user_id = alllistModel.getUserModelServer().getId();

        //формирование данных
        long serverVersionBases = alllistModel.getMysqlServiceFileList().findVersionBasesUserId(user_id);
        long serverSizeBases;
        //Обновление VerionBases
        alllistModel.setClientListFileInfo(updateVersionBases(alllistModel.getClientListFileInfo() ,  serverVersionBases));

        if(alllistModel.getClientListFileInfo() != null)
        {
            serverSizeBases = alllistModel.getClientListFileInfo().length;
        }
        else
        {
            serverSizeBases = 0;
        }

        alllistModel.getModelObjectClient().setListFileInfo(alllistModel.getClientListFileInfo());

        //отправка данных
        String[] textMessage = {String.valueOf(serverVersionBases) , "SYNCREATEALLLIST" , String.valueOf(serverSizeBases) , String.valueOf(alllistModel.getUserModelServer().getId()) , alllistModel.getUserRequestParties() };
        alllistModel.getModelObjectClient().setTextmessage(textMessage);
        alllistModel.getMessage().sendFileInfoObject(alllistModel.getModelObjectClient() , "User." + alllistModel.getUserModelServer().getUsername());
        alllistModel.setClientListFileInfo(null);
    }

    private FileInfoModel[] updateVersionBases(FileInfoModel[] arr , long serverVersionBases)
    {
        if(arr != null)
        {
            for(int i = 0 ; i < arr.length; i++)
            {
                if(arr[i] != null)arr[i].setVersionUpdateBases(serverVersionBases);
            }

            return arr;
        }
        else
        {
            return null;
        }

    }

    public void getListSynSingList(SyncModel syncModel , SyncSinglRequestModel requestModel, String parties)
    {
        long user_id = requestModel.getUserModelServer().getId();
        String username = requestModel.getUserModelServer().getUsername();

        //формирование данных
        long serverVersionBases = syncModel.getMysqlServiceFileList().findVersionBasesUserId(user_id);
        long serverSizeBases;

        if(requestModel.getClientListFileInfo() != null)
        {
            serverSizeBases = requestModel.getClientListFileInfo().length;
        }
        else
        {
            serverSizeBases = 0;
        }

        requestModel.getModelObjectClient().setListFileInfo(requestModel.getClientListFileInfo());

        //отправка данных
        String[] textMessage = {String.valueOf(serverVersionBases) , "SYNSINGLLIST" , String.valueOf(serverSizeBases) , String.valueOf(requestModel.getUserModelServer().getId()) , parties };
        requestModel.getModelObjectClient().setTextmessage(textMessage);
        System.out.println("Отправка SINSINGLLIST юзеру "  + requestModel.getUserModelServer().getUsername());
        syncModel.getMessage().sendFileInfoObject(requestModel.getModelObjectClient() , "User."+username);
        requestModel.setClientListFileInfo(null);
    }

}
