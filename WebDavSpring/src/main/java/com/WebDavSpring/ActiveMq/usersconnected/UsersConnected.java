package com.WebDavSpring.ActiveMq.usersconnected;

import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.ConnectedIdRunnableModel;
import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.ConnectedModel;

import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.UsersSessionIdModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUser;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUsersConnected;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUsersSessionId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by А д м и н on 07.04.2020.
 */

@Component
public class UsersConnected
{

    @Autowired
    private ServiceUsersConnected serviceConnected;

    @Autowired
    private ServiceUser serviceUsers;

    @Autowired
    private ServiceUsersSessionId sessionIdUsers;


    //Запускается после создание класса!
    @PostConstruct
    public void init()
    {
        clearAll();
    }
    private void clearAll()
    {
        System.out.println("*****UsersConnected->PostConstruct: Перезапуск сервера!!!*****");
        System.out.println("*****UsersConnected->PostConstruct: Все записи подключений очищены и даже админские!!*****");
        serviceConnected.clearAllConnected();
    }
    private void clearAdmin()
    {
        UsersModel user = serviceUsers.findByUsername("admin");

        if(user != null)
        {
            int admin_user_id = user.getId();

            if(admin_user_id != 0)
            {
                System.out.println("*****UsersConnected->PostConstruct: Записи с админскими подключениями очищены!!*****");

                clearAdminRecordConnected(admin_user_id);
                clearAdminRecordSession(admin_user_id);
            }
            else
            {
                System.out.println("*****UsersConnected->PostConstruct: Записи с подключениями | очистки Не Требуют!!*****");
            }
        }
        else {
            System.out.println("*****UsersConnected->PostConstruct: Записи с подключениями | очистки Не Требуют!!*****");
        }
    }

    public void addConnectedToService(String username , String clientId , String connectionId , ConnectedIdRunnableModel modelRunnable)
    {
        UsersModel users = serviceUsers.findByUsername(username);


        if(users != null)
        {
            modelRunnable.setServerConnectedId(connectionId);
            modelRunnable.setUserid(users.getId());


            List<ConnectedModel> connectedModel = getModel(users.getId());

            if(!connectedModel.isEmpty())
            {
                ConnectedModel model = serviceConnected.findBySessionid(connectionId);
                ConnectedModel modelPublisher = serviceConnected.findPublisher(users.getId());

                if(serviceConnected.findBySessionid(connectionId) != null)
                {
                    if(modelPublisher == null)
                        model.setPublisher(true);

                    serviceConnected.addConnectedUsersId(model);
                }
                else
                {
                    ConnectedModel usersConnectedModel = new ConnectedModel();
                    usersConnectedModel.setUsername(username);
                    usersConnectedModel.setUsersid(users.getId());
                    usersConnectedModel.setClientid(clientId);
                    usersConnectedModel.setSessionid(connectionId);

                    if(modelPublisher == null)
                    {
                        if(username.equals("admin"))
                        {
                            usersConnectedModel.setPublisher(false);
                        }
                        else
                        {
                            usersConnectedModel.setPublisher(true);
                        }
                    }

                    serviceConnected.addConnectedUsersId(usersConnectedModel);
                }
                //просто добавляем без других проверок это пока для статистики
                sessionIdUsers.addUsersSessionId(createUsersSessionIdModel(connectionId ,  users.getId()));
            }
            else
            {
                createUsersConnectedModel( username ,  connectionId ,  clientId ,  users.getId());
            }
        }
        else
        {
            System.out.println("UsersConnected->addConnectedToService: Критическая ошибка пользователь не добавлен в список подключенных!");
        }

    }



    private void clearAdminRecordConnected(int admin_user_id)
    {
         serviceConnected.deleteAllConnectionUsers(admin_user_id);
    }
    private void clearAdminRecordSession(int admin_user_id)
    {
        sessionIdUsers.deleteAllSessionUsers(admin_user_id);
    }


    public void removeSessionID(String connectionId)
    {
        serviceConnected.deleteConnectionBySessionId(connectionId);
        sessionIdUsers.deleteConnectionByClientId(connectionId);


    }


    private void createUsersConnectedModel(String username , String connectionId , String clientId , int users_id)
    {
        ConnectedModel usersConnectedModel = new ConnectedModel();
        usersConnectedModel.setUsername(username);
        usersConnectedModel.setUsersid(users_id);
        usersConnectedModel.setClientid(clientId);
        usersConnectedModel.setSessionid(connectionId);

        ConnectedModel publicsherMysql = serviceConnected.findPublisher(users_id);

        //если у нас нет издателя для остальных
        if(publicsherMysql == null)
        {
            if(username.equals("admin"))
            {
                usersConnectedModel.setPublisher(false);
            }
            else
            {
                usersConnectedModel.setPublisher(true);
            }

        }


        serviceConnected.addConnectedUsersId(usersConnectedModel);
        sessionIdUsers.addUsersSessionId(createUsersSessionIdModel(connectionId ,  users_id));
    }



    private UsersSessionIdModel createUsersSessionIdModel(String sessionId , int users_id)
    {
        UsersSessionIdModel sessionIdModel = new UsersSessionIdModel();

        sessionIdModel.setUserid(users_id);
        sessionIdModel.setSessionid(sessionId);

        return sessionIdModel;
    }

    private List<ConnectedModel> getModel(int user_id)
    {
        return serviceConnected.findByUsersid(user_id);
    }

}
