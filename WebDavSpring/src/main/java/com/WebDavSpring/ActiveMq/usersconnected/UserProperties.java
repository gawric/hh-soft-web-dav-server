package com.WebDavSpring.ActiveMq.usersconnected;


import com.WebDavSpring.ActiveMq.queue.QueueRunnablePriorityWorkerToServer;
import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.ConnectedModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUsersConnected;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by А д м и н on 20.04.2020.
 */
@Component
public class UserProperties
{


    @Autowired
    private QueueRunnablePriorityWorkerToServer serviceQueueRunnable;

    public boolean isWorkingUser(int userid) {


        boolean check = serviceQueueRunnable.isWorking(userid);

        return check;
    }


}
