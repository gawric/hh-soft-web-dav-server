package com.WebDavSpring.ActiveMq.thread.support.pastePartiesFolder;

import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceListFiles;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.PriorityQueue;

/**
 * Created by А д м и н on 13.01.2020.
 */
public class SinglUpdateParties {

    private ServiceListFiles mysqlServiceFileList;

    //массив с данными о манипуляция клиента
    private FileInfoModel[] clientListFileInfo;

    private PriorityQueue<Integer> myPrioritySinglUpdatePartiesQueue;

    //прокладка со всеми данными для клиента
    private FileInfoObjectTopic modelObjectClient;

    //для отправки сообщений клиентам
    private SendMessage message;

    private UsersModel userModelServer;

    public SinglUpdateParties(ServiceListFiles mysqlServiceFileList , FileInfoModel[] clientListFileInfo , PriorityQueue<Integer> myPrioritySinglUpdatePartiesQueue , FileInfoObjectTopic modelObjectClient , SendMessage message , UsersModel userModelServer)
    {
        this.mysqlServiceFileList = mysqlServiceFileList;
        this.clientListFileInfo = clientListFileInfo;
        this.myPrioritySinglUpdatePartiesQueue = myPrioritySinglUpdatePartiesQueue;
        this.modelObjectClient = modelObjectClient;
        this.message = message;
        this.userModelServer = userModelServer;
    }



    public long getNewVersionRows()
    {
        long pasteVersionRows = mysqlServiceFileList.getUserMaxVersionRows(userModelServer.getId()) + 1;

        return pasteVersionRows;
    }


    private void insertFileInfo(FileInfoModel[] clientListFileInfo , int user_id)
    {

        mysqlServiceFileList.updateSinglList(clientListFileInfo , user_id , true);
    }


    public void releasesSinglUpdateParties(ThreadPoolTaskExecutor task)
    {
        int objectIndex = myPrioritySinglUpdatePartiesQueue.size() + 1;
        myPrioritySinglUpdatePartiesQueue.add(objectIndex);

        SinglUpdateSend send = new SinglUpdateSend( mysqlServiceFileList ,   modelObjectClient ,  message ,  userModelServer ,  clientListFileInfo ,  "SINGLUPDATEPARTIES");
        task.execute(send);

        insertFileInfo(clientListFileInfo , userModelServer.getId());


        //getListSynSingList( userModelServer ,  clientListFileInfo , "SINGLUPDATEPARTIES");

        myPrioritySinglUpdatePartiesQueue.remove(objectIndex);
    }

    public void releasesSinglBeginUpdateParties()
    {
        int objectIndex = myPrioritySinglUpdatePartiesQueue.size() + 1;
        myPrioritySinglUpdatePartiesQueue.add(objectIndex);

        SinglUpdateSend send = new SinglUpdateSend( mysqlServiceFileList ,   modelObjectClient ,  message ,  userModelServer , clientListFileInfo , "SINGLBEGINUPDATEPARTIES");
        send.run();

        //getListSynSingList( userModelServer ,  clientListFileInfo , "SINGLBEGINPARTIES");

        myPrioritySinglUpdatePartiesQueue.remove(objectIndex);
    }




    public void releasesSinglEndUpdateParties()
    {

        while (true)
        {
            try
            {
                Thread.sleep(50);

            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            //если последний update отправлен
            if(myPrioritySinglUpdatePartiesQueue.size() == 0 )
            {

                insertFileInfo(clientListFileInfo , userModelServer.getId());

                SinglUpdateSend send = new SinglUpdateSend( mysqlServiceFileList ,   modelObjectClient ,  message ,  userModelServer , clientListFileInfo , "SINGLENDUPDATEPARTIES");
                send.run();
                //Отправка сообщений
                //getListSynSingList( userModelServer ,  clientListFileInfo , "SINGLENDUPDATEPARTIES");
                break;
            }
        }


    }






}
