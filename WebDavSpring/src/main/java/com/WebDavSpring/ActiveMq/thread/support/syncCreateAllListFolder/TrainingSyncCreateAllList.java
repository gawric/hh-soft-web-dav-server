package com.WebDavSpring.ActiveMq.thread.support.syncCreateAllListFolder;

import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.model.syncCreateModel.SyncCreateAllListModel;

/**
 * Created by А д м и н on 07.05.2020.
 */
public class TrainingSyncCreateAllList {

    private SyncCreateAllListModel alllistModel;
    private String username;
    private SyncCreateAllList allList;

    public TrainingSyncCreateAllList(SyncCreateAllListModel alllistModel)
    {
        this.alllistModel = alllistModel;
        this.username = alllistModel.getUserModelServer().getUsername();
        allList = new SyncCreateAllList(alllistModel);
    }

    public void creatAllList(long user_id)
    {
        //не тестировалась для всех запросов, нужны тесты

        equalsRequest(user_id);

    }

    private void equalsRequest(long user_id)
    {
        if(alllistModel.getUserRequestParties().equals(staticVariable.BEGINPARTIES))
        {
            allList.createAllListBEGINPARTIES();
        }
        else if(alllistModel.getUserRequestParties().equals(staticVariable.PARTIES))
        {

            allList.createAllListPARTIES(user_id);

        }
        else if(alllistModel.getUserRequestParties().equals(staticVariable.ENDPARTIES))
        {
            allList.createALllListENDPARTIES(user_id);
        }
    }



}
