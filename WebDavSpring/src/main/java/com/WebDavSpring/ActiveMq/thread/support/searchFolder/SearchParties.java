package com.WebDavSpring.ActiveMq.thread.support.searchFolder;

import com.WebDavSpring.ActiveMq.thread.support.SearchFilesServer;
import com.WebDavSpring.WebDavSpring.model.searchModel.SearchModelObj;
import com.WebDavSpring.WebDavSpring.model.searchModel.SearchModelRequest;

/**
 * Created by А д м и н on 07.05.2020.
 */
public class SearchParties {

    private SearchModelObj searchModelObj;
    private SearchModelRequest searchModelRequest;
    private  SearchFilesServer searchFilesServer;

    public SearchParties(SearchModelObj searchModelObj , SearchModelRequest searchModelRequest) {
        this.searchModelObj = searchModelObj;
        this.searchModelRequest = searchModelRequest;
        searchFilesServer = new SearchFilesServer( searchModelObj.getMessage() , searchModelRequest.getModelObjectClient(), searchModelRequest.getReceivedConnectionID());
    }

    public void startSEACRHBEGINFILES() {

        searchFilesServer.SendBeginClientInfo(searchModelRequest.getUserModelServer());
    }


    public void startSEARCHENDFILES() {

        searchFilesServer.SendEndClientInfo(searchModelRequest.getUserModelServer());
    }

    public void startPARTIESFILES() {

        searchFilesServer.SendPartiesClientInfo(searchModelRequest.getUserModelServer());
    }




}
