package com.WebDavSpring.ActiveMq.thread.support.publisherFolder;

import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.model.wokingModel.SyncModel;

/**
 * Created by А д м и н on 06.05.2020.
 */
public class TrainingPublisher {

    private SyncModel syncModel;
    private UsersModel userModelServer;
    private String[] receivedConnectionID;
    private Publisher publisher = null;

    public TrainingPublisher(SyncModel syncModel , UsersModel userModelServer , String[] receivedConnectionID)
    {
        this.syncModel = syncModel;
        this.userModelServer = userModelServer;
        this.receivedConnectionID = getReceivedConnectionID(receivedConnectionID);

    }


    public void startPublisher(FileInfoObjectTopic modelObjectClient)
    {

       // syncModel.getMessage().checkWeb();


        int userid = Integer.parseInt(modelObjectClient.getTextmessage()[2]);

        publisher = new Publisher( syncModel.getServiceConnected() ,   userModelServer , modelObjectClient , syncModel.getMessage());
        publisher.getPublisher( userid ,  receivedConnectionID);

    }

    public void startSend()
    {
        boolean isWorking = syncModel.getUserProperties().isWorkingUser(userModelServer.getId());

        if(publisher != null)
            publisher.sendStatusPublisher(staticVariable.STATUSPUBLISHER ,  receivedConnectionID , isWorking);
    }

    private String[] getReceivedConnectionID(String[] receivedConnectionID)
    {
        return receivedConnectionID.clone();
    }
}
