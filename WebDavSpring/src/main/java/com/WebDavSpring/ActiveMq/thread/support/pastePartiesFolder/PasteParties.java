package com.WebDavSpring.ActiveMq.thread.support.pastePartiesFolder;

import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariableBases;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceListFiles;

import java.util.PriorityQueue;

/**
 * Created by А д м и н on 13.01.2020.
 */
public class PasteParties {

    private ServiceListFiles mysqlServiceFileList;
    //массив с данными о манипуляция клиента
    private FileInfoModel[] clientListFileInfo;

    private PriorityQueue<Integer> myPriorityPasteQueue;

    //прокладка со всеми данными для клиента
    private FileInfoObjectTopic modelObjectClient;

    //для отправки сообщений клиентам
    private SendMessage message;

    private UsersModel userModelServer;


    public PasteParties(ServiceListFiles mysqlServiceFileList , FileInfoModel[] clientListFileInfo , PriorityQueue<Integer> myPriorityPasteQueue , FileInfoObjectTopic modelObjectClient , SendMessage message , UsersModel userModelServer)
    {
        this.mysqlServiceFileList = mysqlServiceFileList;
        this.clientListFileInfo = clientListFileInfo;
        this.myPriorityPasteQueue = myPriorityPasteQueue;
        this.modelObjectClient = modelObjectClient;
        this.message = message;
        this.userModelServer = userModelServer;

    }

    public void setNewVersionRows()
    {
        long pasteVersionRows = mysqlServiceFileList.getUserMaxVersionRows(userModelServer.getId()) + 1;
        staticVariableBases.setLastVersionRows(userModelServer.getId() , pasteVersionRows);
    }

    public long getNewVersionRows()
    {
        return staticVariableBases.getLastVersionRows(userModelServer.getId());
    }

    public void releasesPasteEndparties(long newVersionUpdateRows)
    {
        //реализация удаления по 1 файлу
        //передаем массив для удаления куска или 1 запись для удаления 1 строки
       // mysqlServiceFileList.deleteSinglFileUserTableInfo(clientListFileInfo);

        while (true)
        {
            try
            {
                Thread.sleep(50);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            //если последний update отправлен
            if(myPriorityPasteQueue.size() == 0 )
            {
                // System.out.println("Запрос обработан отправка клиенту  "+userRequestParties);

                //Все добавляем в базу
                mysqlServiceFileList.saveListFilesVersion(clientListFileInfo , newVersionUpdateRows);

                //Отправка сообщений
                getListSynSingList( userModelServer ,  clientListFileInfo , "PASTEENDPARTIES");
                break;
            }
        }
    }
    public void releasesPasteParties(long newVersionUpdateRows)
    {
        int objectIndex = myPriorityPasteQueue.size() + 1;
        myPriorityPasteQueue.add(objectIndex);

        //Все добавляем в базу
        mysqlServiceFileList.saveListFilesVersion(clientListFileInfo , newVersionUpdateRows);

        //Отправка сообщений
        getListSynSingList( userModelServer ,  clientListFileInfo , "PASTEPARTIES");
        myPriorityPasteQueue.remove(objectIndex);
    }

    private void getListSynSingList(UsersModel userModelServer , FileInfoModel[] listFileInfo , String parties)
    {
        long user_id = userModelServer.getId();
        String username = userModelServer.getUsername();

        //формирование данных
        long serverVersionBases = mysqlServiceFileList.findVersionBasesUserId(user_id);
        long serverSizeBases;

        if(listFileInfo != null)
        {
            serverSizeBases = listFileInfo.length;
        }
        else
        {
            serverSizeBases = 0;
        }

        modelObjectClient.setListFileInfo(listFileInfo);

        //отправка данных
        String[] textMessage = {String.valueOf(serverVersionBases) , "SYNSINGLLIST" , String.valueOf(serverSizeBases) , String.valueOf(userModelServer.getId()) , parties };
        modelObjectClient.setTextmessage(textMessage);
        System.out.println("Отправка SINSINGLLIST юзеру "  + userModelServer.getUsername());
        message.sendFileInfoObject(modelObjectClient , "User."+username);
        listFileInfo = null;
    }

}
