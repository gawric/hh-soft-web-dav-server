package com.WebDavSpring.ActiveMq.thread.support;

import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;

/**
 * Created by А д м и н on 21.04.2020.
 */
public class StopRequest {

    private UsersModel userModelServer;
    private FileInfoObjectTopic modelObjectClient;
    private SendMessage message;

    public StopRequest(UsersModel userModelServer , FileInfoObjectTopic modelObjectClient , SendMessage message) {
        this.userModelServer = userModelServer;
        this.modelObjectClient = modelObjectClient;
        this.message = message;

    }

    public void sendSTOPNODELETEBASES(String[] connectionId) {
        getListStopResponceNODELETEBASES( userModelServer , "STOPPUBLISHERNODELETEBASES" , "" ,  connectionId , "");
    }

    private void getListStopResponceNODELETEBASES(UsersModel userModelServer , String parties , String isPublisher , String[] connectionId , String isWorking)
    {

        String username = userModelServer.getUsername();

        //5 - заглушка раньше было connectionID
        String[] textMessage = {"" , parties , "", String.valueOf(userModelServer.getId()), parties  , "" , isPublisher , isWorking};
        modelObjectClient.setTextmessage(textMessage);
        modelObjectClient.setConnectionId(connectionId);
        message.sendFileInfoObject(modelObjectClient , "User."+username);


        System.out.println("***** StopRequest: Отправка запроса  для "+userModelServer.getUsername()+"  "+connectionId+"  >>> Всем остановить сканирование STOPPUBLISHERNODELETEBASES ");
    }



}
