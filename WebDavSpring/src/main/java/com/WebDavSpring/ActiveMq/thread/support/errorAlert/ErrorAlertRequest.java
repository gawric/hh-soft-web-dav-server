package com.WebDavSpring.ActiveMq.thread.support.errorAlert;

import com.WebDavSpring.WebDavSpring.model.ErrorAlertModel;
import com.WebDavSpring.WebDavSpring.model.MailModel;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.model.wokingModel.SyncModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceErrorAlert;

import java.util.Date;

/**
 * Created by А д м и н on 09.07.2020.
 */
//0 - загрулка
//1 - request
//2 - заглушка
//3 - заглушка
//4 - ErrorName
//5 - ErrorText
public class ErrorAlertRequest {

    private String errorName;
    private String errorText;
    private SyncModel syncModel;

    private UsersModel userModelServer;

    public ErrorAlertRequest(String[] textMessageClient , SyncModel syncModel , UsersModel userModelServer){
        this.errorName = textMessageClient[4];
        this.errorText = textMessageClient[5];
        this.syncModel = syncModel;
        this.userModelServer = userModelServer;
    }

    public void saveErrorAlert(){

        ErrorAlertModel model = createModel(errorName , errorText , userModelServer.getId() ,  userModelServer.getUsername() );
        if(model != null) syncModel.getServiceErrorAlert().saveError(model);
        errorSendEmailAdmin(errorName , errorText);
    }

    private ErrorAlertModel createModel(String errorName , String errorText , int userid , String username)
    {
        ErrorAlertModel model = new ErrorAlertModel();
        model.setUsername(username);
        model.setUserid(userid);
        model.setErrorDate(new Date().toString());
        model.setErrortext(errorName);
        model.setTextblock(errorText);

        return model;
    }

    private void errorSendEmailAdmin(String errorName , String errorText)
    {

        syncModel.getJavaMail().sendMail(createMailModel(errorName , errorText));
    }

    private MailModel createMailModel(String errorName , String errorText)
    {
        MailModel mailModel = new MailModel();
        mailModel.setEmailguest("gawric@mail.ru");
        mailModel.setNamequest("WebDavServer");
        mailModel.setPhonequest("9126340354");
        mailModel.setSubject("Уведомление от сервера WebDavServer");
        mailModel.setText("\nИмя Ошибки "+errorName + " \nОписание: "+errorText);

        return mailModel;
    }
}
