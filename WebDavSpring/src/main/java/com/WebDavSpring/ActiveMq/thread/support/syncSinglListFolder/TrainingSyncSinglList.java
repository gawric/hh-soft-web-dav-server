package com.WebDavSpring.ActiveMq.thread.support.syncSinglListFolder;


import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.syncSinglListModel.SyncSinglRequestModel;
import com.WebDavSpring.WebDavSpring.model.wokingModel.SyncModel;

/**
 * Created by А д м и н on 07.05.2020.
 */
public class TrainingSyncSinglList {

    private SyncModel syncModel;
    private SyncSinglRequestModel requestModel;
    private SyncSingList syncSing;

    public TrainingSyncSinglList(SyncModel syncModel ,SyncSinglRequestModel requestModel)
    {
        this.syncModel = syncModel;
        this.requestModel = requestModel;
        syncSing = new SyncSingList( syncModel , requestModel);

    }

    public void creatSinglList()
    {
        //не тестировалась для всех запросов, нужны тесты
       // syncModel.getSupportWareHouse().updateItemWareHouseSyncCreateAllList("ADD" , requestModel.getUserRequestParties() , syncModel.getWarehouse() , requestModel.getReceivedConnectionID());
        equalsRequest();
       // syncModel.getSupportWareHouse().updateItemWareHouseSyncCreateAllList("REMOVE" , requestModel.getUserRequestParties() , syncModel.getWarehouse() , requestModel.getReceivedConnectionID());
    }

    public void equalsRequest()
    {

        if(requestModel.getUserRequestParties().equals(staticVariable.DELETEBEGINPARTIES))
        {
            //System.out.println("Нашли и запустили DELETEBEGINPARTIES");
            syncSing.startDELETEBEGINPARTIES();

        }
        else if(requestModel.getUserRequestParties().equals(staticVariable.DELETEPARTIES))
        {
            //System.out.println("Нашли и запустили DELETEPARTIES");
            syncSing.startDELETEPARTIES();
        }
        else if(requestModel.getUserRequestParties().equals(staticVariable.DELETEENDPARTIES))
        {

            //System.out.println("Нашли и запустили DELETEENDPARTIES");
            syncSing.startDELETEENDPARTIES();

        }
        else if(requestModel.getUserRequestParties().equals(staticVariable.PASTEBEGINPARTIES))
        {
            syncSing.startPASTEBEGINPARTIES();
        }
        else if(requestModel.getUserRequestParties().equals(staticVariable.PASTEPARTIES))
        {
            syncSing.startPASTEPARTIES();
        }
        else if(requestModel.getUserRequestParties().equals(staticVariable.PASTEENDPARTIES))
        {
            syncSing.startPASTEENDPARTIES();
        }
        else if(requestModel.getUserRequestParties().equals(staticVariable.SINGLBEGINUPDATEPARTIES) | requestModel.getUserRequestParties().equals(staticVariable.RENAMESINGLBEGINUPDATEPARTIES))
        {
            syncSing.startSINGLBEGINUPDATEPARTIES();
        }
        else if(requestModel.getUserRequestParties().equals(staticVariable.SINGLUPDATEPARTIES) | requestModel.getUserRequestParties().equals(staticVariable.RENAMESINGLUPDATEPARTIES))
        {
            syncSing.startSINGLUPDATEPARTIES();
        }
        else if(requestModel.getUserRequestParties().equals(staticVariable.SINGLENDUPDATEPARTIES)| requestModel.getUserRequestParties().equals(staticVariable.RENAMESINGLENDUPDATEPARTIES))
        {
            syncSing.startSINGLENDUPDATEPARTIES();
        }
       // else if(requestModel.getUserRequestParties().equals(staticVariable.SINGLENDUPDATEPARTIES)| requestModel.getUserRequestParties().equals(staticVariable.RENAMESINGLENDUPDATEPARTIES))
       // {
       //     syncSing.startSINGLENDUPDATEPARTIES();
       // }
    }
}
