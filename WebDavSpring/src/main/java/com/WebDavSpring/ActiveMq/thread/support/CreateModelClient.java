package com.WebDavSpring.ActiveMq.thread.support;

import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceAuthUser;

import java.util.List;

/**
 * Created by А д м и н on 07.10.2019.
 */
public class CreateModelClient
{
    public FileInfoObjectTopic createObjectClient(FileInfoObjectTopic modelObjectClient , String Status , FileInfoModel[] listFileInfo , long user_id , long serverVersion , String parties , String[] connectionID , String isPublisher , ServiceAuthUser serviceAuthUser)
    {
        String[] textMessage = null;

        if(listFileInfo != null)
        {
            modelObjectClient.setListFileInfo(listFileInfo);
            textMessage = new String[]{String.valueOf(serverVersion), Status, String.valueOf(listFileInfo.length), String.valueOf(user_id), parties , "" , isPublisher};
        }
        else
        {
            textMessage = new String[]{String.valueOf(serverVersion), Status, String.valueOf(0), String.valueOf(user_id), parties , ""};
        }


        modelObjectClient.setConnectionId(connectionID);
        modelObjectClient.setTextmessage(textMessage);
        modelObjectClient.setWebdavnamespace(serviceAuthUser.getNameSpaceUser(user_id));

        return modelObjectClient;
    }


    public FileInfoModel[] ConvertListToArrayList(List<FileInfoModel> list)
    {
        FileInfoModel[] arr = new FileInfoModel[list.size()];

        // ArrayList to Array Conversion
        for (int i =0; i < list.size(); i++)
            arr[i] = list.get(i);

        return arr;
    }

}
