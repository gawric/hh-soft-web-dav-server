package com.WebDavSpring.ActiveMq.thread.support.getListFolder;


import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.ActiveMq.thread.support.CreateModelClient;
import com.WebDavSpring.MailSender.JavaMail;
import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariableBases;
import com.WebDavSpring.WebDavSpring.model.*;
import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.ConnectedModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceAuthUser;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceClientInfo;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceListFiles;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUsersConnected;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by А д м и н on 07.10.2019.
 */
public class GetList
{
    private ServiceListFiles mysqlServiceFileList;
    private SendMessage message;
    private FileInfoObjectTopic modelObjectClient;
    private String username;
    private int sizeArr = 1000;
    private long clientVersionBases;
    private String[] receivedConnectionID;
    private ServiceUsersConnected serviceConnected;
    private ServiceClientInfo serviceClientInfo;

    public GetList(ServiceClientInfo serviceClientInfo , ServiceListFiles mysqlServiceFileList , SendMessage message , FileInfoObjectTopic modelObjectClient , String username , long clientVersionBases ,  String[] receivedConnectionID , ServiceUsersConnected serviceConnected)
    {
        this.mysqlServiceFileList = mysqlServiceFileList;
        this.message = message;
        this.modelObjectClient = modelObjectClient;
        this.username = username;
        this.clientVersionBases = clientVersionBases;
        this.receivedConnectionID = receivedConnectionID;
        this.serviceConnected =  serviceConnected;
        this.serviceClientInfo = serviceClientInfo;

    }

    public void updateClientInfo(String[] textMessage , String username , JavaMail javaMail)
    {

        Runnable newTaskClientInfoUpdate = () ->
        {
            ClientInfoSystemModel infoModel = createInfoModel(textMessage , username);
            ClientInfoSystemModel mysqlClientInfoModel = serviceClientInfo.findByMacClientInfo(infoModel.getMacnetwork());
            addClientInfoModel(mysqlClientInfoModel , infoModel , javaMail);

        };
        new Thread(newTaskClientInfoUpdate).start();

    }

    private void sendMail(JavaMail javaMail , String username , String macadress)
    {
        String errorName = " Обнаружено новое подключение ";
        String errorText = " Обнаружено новое подключение от юзера " + username + " c мак адресом " + macadress;
        MailModel mailModel = createMailModel(errorName , errorText);
        //закрыты порты у провайдера поэтому не работает данная фишка!
        //javaMail.sendMail(mailModel);
    }

    private MailModel createMailModel(String errorName , String errorText)
    {
        MailModel mailModel = new MailModel();
        mailModel.setEmailguest("gawric@mail.ru");
        mailModel.setNamequest("WebDavServer");
        mailModel.setPhonequest("9126340354");
        mailModel.setSubject("Уведомление от сервера WebDavServer");
        mailModel.setText("\nИмя Ошибки "+errorName + " \nОписание: "+errorText);

        return mailModel;
    }
    private void addClientInfoModel(ClientInfoSystemModel mysqlClientInfoModel , ClientInfoSystemModel infoModelClient , JavaMail javaMail)
    {
        if(mysqlClientInfoModel != null)
        {
            mysqlClientInfoModel.setRecordingtime(infoModelClient.getRecordingtime());
            serviceClientInfo.addClientInfo(mysqlClientInfoModel);
        }
        else
        {
            sendMail(javaMail , infoModelClient.getUsername() , infoModelClient.getMacnetwork());
            serviceClientInfo.addClientInfo(infoModelClient);
        }
    }

    private ClientInfoSystemModel createInfoModel(String[] textMessage , String username)
    {
        ClientInfoSystemModel infoModel = new ClientInfoSystemModel();
        infoModel.setIpLocal(textMessage[2]);
        infoModel.setIpNetwork(textMessage[2]);
        infoModel.setMacnetwork(textMessage[4]);
        infoModel.setOsUserName(textMessage[5]);
        infoModel.setOsVerison(textMessage[6]);

        LocalDateTime timeDate = LocalDateTime.now();
        infoModel.setRecordingtime(timeDate.toString());
        infoModel.setUsername(username);
       // infoModel.setIpNetwork(message.);
        return infoModel;
    }

    public  void getListReleases(UsersModel userModelServer , ServiceAuthUser serviceAuthUser , long clientVersionRows)
    {
        long user_id = userModelServer.getId();
        //true - пустая
        boolean empty  = mysqlServiceFileList.existsTableFileInfo(user_id);

        ConnectedModel usersConnected = null;

        usersConnected = checkPublisher(usersConnected);
        usersConnected = createConnected(usersConnected);

        //проверка что база у нас вообще есть
        if(empty)
        {
            System.out.println("GetList->getListReleases: База Данных пустая! записи отсутствуют!" + userModelServer.getUsername());
            sendMessageSYNCREATEALLLISTEmpty(userModelServer , usersConnected , serviceAuthUser);
        }
        else
        {
            //получаем версию базы данных сервера и версию базы данных юзера
            long serverVersionBases = mysqlServiceFileList.findVersionBasesUserId(user_id);
            staticVariableBases.setLastVersionBases(user_id , serverVersionBases );
            compareVersion(clientVersionRows , serverVersionBases ,  userModelServer ,  usersConnected ,  serviceAuthUser ,  user_id);


        }
    }





    private void compareVersion(long clientVersionRows , long serverVersionBases , UsersModel userModelServer , ConnectedModel usersConnected , ServiceAuthUser serviceAuthUser , long user_id)
    {

        //сравниваем
        //true - нужно обновлять
        if(сheckVersionBases(clientVersionBases ,serverVersionBases ))
        {
            sendMessageSYNCREATEALLLISTCLient(userModelServer , serverVersionBases , usersConnected , serviceAuthUser);
        }
        else
        {
            //Нашли что в базе есть обновленные данные
            if(isCheckBaseRows(mysqlServiceFileList , user_id , clientVersionRows))
            {
                sendMessageSendSinglClient(userModelServer , serverVersionBases , clientVersionRows , usersConnected , serviceAuthUser );
            }
            else
            {
                sendMessageACTUALListClient( userModelServer ,  serverVersionBases , usersConnected , serviceAuthUser);
           }

        }
    }

    private ConnectedModel createConnected(ConnectedModel usersConnected)
    {
        //если в нашей базе такой сесси вообще нет!
        if(usersConnected == null){
            usersConnected = new ConnectedModel();
            usersConnected.setPublisher(false);
        }

        return  usersConnected;
    }
    private ConnectedModel checkPublisher(ConnectedModel usersConnected)
    {
        for(String item : receivedConnectionID) {

            usersConnected =   serviceConnected.findByClientid(item);

            if(usersConnected.isPublisher())
                break;

        }

        return usersConnected;
    }
    //отправляем если база вообще пустая
    private void sendMessageSYNCREATEALLLISTEmpty(UsersModel user , ConnectedModel usersConnected , ServiceAuthUser serviceAuthUser )
    {
        FileInfoModel[] listFileInfo = {};
        CreateModelClient model = new CreateModelClient();
        String parties = "BEGINPARTIES";



        //только очищаем базу клиента т.к данных в нашей базе нет
        modelObjectClient = model.createObjectClient(modelObjectClient , "Empty" ,  listFileInfo , user.getId(),  0 , parties , receivedConnectionID , String.valueOf(usersConnected.isPublisher()) , serviceAuthUser);

        System.out.println("GetList-> sendMessageSYNCREATEALLLISTEmpty: Отправка клиенту фалага что нужно пересканировать webdavclient" + user.getUsername());
        message.sendFileInfoObject(modelObjectClient , "User."+username);
    }

    private Boolean isCheckBaseRows(ServiceListFiles mysqlServiceFileList , long user_id , long clientVersionRows)
    {
        long serverVersionRows = mysqlServiceFileList.getUserMaxVersionRows(user_id);
        staticVariableBases.setLastVersionRows(user_id , serverVersionRows );
        System.out.print("Версия serverVersionRows данных у нашего сервера " +serverVersionRows +"\n");
        System.out.print("Версия serverVersionRows данных у клиента " +clientVersionRows +"\n");

        if(serverVersionRows > clientVersionRows)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void sendMessageSendSinglClient(UsersModel user , long serverVersionBases , long versionupdaeRows , ConnectedModel usersConnected , ServiceAuthUser serviceAuthUser)
    {
        long user_id = user.getId();




        sendMessageSend(user ,  serverVersionBases ,  usersConnected ,  serviceAuthUser , user_id , versionupdaeRows);
        sendMessageSendTemp(user ,  serverVersionBases ,  usersConnected ,  serviceAuthUser , user_id , versionupdaeRows);

        System.out.println("GetList-> : Отправка части файлов SINSINGLLIST ENDPARTIES" + user.getUsername());
    }


    private void sendMessageSend(UsersModel user , long serverVersionBases , ConnectedModel usersConnected , ServiceAuthUser serviceAuthUser , long user_id , long versionupdaeRows)
    {
        CreateModelClient model = new CreateModelClient();
        List<Long> allRowsLong = mysqlServiceFileList.getVersionUpdateRowsLongByUserIdAndByVersionRows(user_id , versionupdaeRows);
        List<Long> arr = new ArrayList<>();


        sendMessageBeginSinglSingParties( user ,  serverVersionBases ,  usersConnected ,   model , serviceAuthUser);
        sendParties(allRowsLong , arr , user ,  serverVersionBases ,  usersConnected ,  model ,  user_id ,  serviceAuthUser);
        sendEndParties( arr ,  serviceAuthUser , usersConnected ,  serverVersionBases ,  user ,  model ,  user_id);
    }
    private void sendMessageSendTemp(UsersModel user , long serverVersionBases , ConnectedModel usersConnected , ServiceAuthUser serviceAuthUser , long user_id , long versionUpdateRows)
    {


        CreateModelClient model = new CreateModelClient();
        List<Long> allRowsLong = mysqlServiceFileList.listFilesGetAllRow_idAndVersionRowsTemp( user_id ,  versionUpdateRows);
        List<Long> arr = new ArrayList<>();

        sendMessageBeginSinglSingParties( user ,  serverVersionBases ,  usersConnected ,   model , serviceAuthUser);
        sendPartiesTemp(allRowsLong , arr , user ,  serverVersionBases ,  usersConnected ,  model ,  user_id ,  serviceAuthUser);
        sendEndPartiesTemp( arr ,  serviceAuthUser , usersConnected ,  serverVersionBases ,  user ,  model ,  user_id);




        System.out.println("GetList-> : Отправка части файлов SINSINGLLIST ENDPARTIES" + user.getUsername());
    }


    private void sendEndPartiesTemp(List<Long> arr , ServiceAuthUser serviceAuthUser ,ConnectedModel usersConnected , long serverVersionBases , UsersModel user , CreateModelClient model , long user_id)
    {
        List<FileInfoModel> serverModel = mysqlServiceFileList.selectListTemp(arr , user_id);
        String endParties = "SINGLENDUPDATEPARTIES";
        sendSinglTemp(serverModel  , user  ,  model ,  serverVersionBases ,  usersConnected ,  endParties , serviceAuthUser);
    }

    private void sendEndParties(List<Long> arr , ServiceAuthUser serviceAuthUser ,ConnectedModel usersConnected , long serverVersionBases , UsersModel user , CreateModelClient model , long user_id)
    {
        List<List<FileInfoModel>> serverModel = mysqlServiceFileList.selectGroupAllList(arr , user_id);
        String endParties = "SINGLENDUPDATEPARTIES";
        sendSingl(serverModel  , user  ,  model ,  serverVersionBases ,  usersConnected ,  endParties , serviceAuthUser);
    }
    private void sendParties(List<Long> allRowsLong , List<Long> arr , UsersModel user , long serverVersionBases , ConnectedModel usersConnected , CreateModelClient model , long user_id , ServiceAuthUser serviceAuthUser)
    {
        int f = 0;
        for(int d = 0; d < allRowsLong.size(); d++)
        {

            if(f == sizeArr)
            {
                trainingSinglSend( user ,  serverVersionBases ,  usersConnected ,   model ,   arr ,  user_id , serviceAuthUser);
                arr.clear();
                f = 0;
            }

            arr.add(allRowsLong.get(d));

            f++;
        }
    }


    private void sendPartiesTemp(List<Long> allRowsLong , List<Long> arr , UsersModel user , long serverVersionBases , ConnectedModel usersConnected , CreateModelClient model , long user_id , ServiceAuthUser serviceAuthUser)
    {
        int f = 0;
        for(int d = 0; d < allRowsLong.size(); d++)
        {

            if(f == sizeArr)
            {
                trainingSinglSendTemp( user ,  serverVersionBases ,  usersConnected ,   model ,   arr ,  user_id , serviceAuthUser);
                arr.clear();
                f = 0;
            }

            arr.add(allRowsLong.get(d));

            f++;
        }
    }



    //отправляем если база более свежая чем у клиента
    private void sendMessageSYNCREATEALLLISTCLient(UsersModel user , long serverVersionBases , ConnectedModel usersConnected , ServiceAuthUser serviceAuthUser)
    {
        long user_id = user.getId();

        CreateModelClient model = new CreateModelClient();
        List<Long> allRowsLong = mysqlServiceFileList.getAllRowsLongByUserId(user_id);
        List<Long> arr = new ArrayList<>();

        sendBeginParties( user ,  serverVersionBases ,  usersConnected ,   model , serviceAuthUser);

        int f = 0;
        for(int d = 0; d < allRowsLong.size(); d++)
        {

            if(f == sizeArr)
            {
                trainingSend( user ,  serverVersionBases ,  usersConnected ,   model ,   arr ,  user_id , serviceAuthUser);
                arr.clear();
                f = 0;
            }

            arr.add(allRowsLong.get(d));

            f++;
        }


        List<List<FileInfoModel>> serverModel = mysqlServiceFileList.selectGroupAllList(arr , user_id);
        List<FileInfoModel> endList = converMultiListToList( serverModel);
        //3 - отпрвляем флаг что данные отправлены все, досылаем остатки и обновляем дерево
        //Конец обработки
        String parties2 = "ENDPARTIES";
        sendEndSyncCreate(endList  , user  ,  model ,  serverVersionBases ,  usersConnected ,  parties2 , serviceAuthUser);

        System.out.println("GetList-> sendMessageSYNCREATEALLLISTCLient: Отправка SYNCCREATEALLLIST ENDPARTIES" + user.getUsername());
    }

    private List<FileInfoModel> converMultiListToList(List<List<FileInfoModel>> serverModel)
    {
        HashSet<Long> set = new HashSet<>();

        List<FileInfoModel> listEnd = new ArrayList<>();

        for(int b = 0 ; b < serverModel.size(); b++)
        {
            List<FileInfoModel> listTemp = serverModel.get(b);
            for(int c = 0 ; c < listTemp.size(); c++)
            {
                FileInfoModel fim = listTemp.get(c);
                if(!set.contains(fim.getRow_id()))
                {
                    set.add(fim.getRow_id());
                    listEnd.add(fim);
                }

            }
        }
        serverModel.clear();
        set.clear();
        set = null;
        serverModel = null;
        return listEnd;
    }

    private void sendMessageBeginSinglSingParties(UsersModel user , long serverVersionBases , ConnectedModel usersConnected ,  CreateModelClient model , ServiceAuthUser serviceAuthUser)
    {
        //1 - отпрвляем флаг очистить базу
        String parties1 = "SINGLBEGINUPDATEPARTIES";

        modelObjectClient = model.createObjectClient(modelObjectClient , "SYNSINGLLIST" ,  null , user.getId(),  serverVersionBases , parties1 , receivedConnectionID , String.valueOf(usersConnected.isPublisher()) , serviceAuthUser);

        //  System.out.println("GetList-> sendMessageSYNCREATEALLLISTCLient: Отправка SYNCCREATEALLLIST BEGINParties " + user.getUsername());
        message.sendFileInfoObject(modelObjectClient , "User."+username);
    }


    private void sendBeginParties(UsersModel user , long serverVersionBases , ConnectedModel usersConnected ,  CreateModelClient model , ServiceAuthUser serviceAuthUser)
    {
        //1 - отпрвляем флаг очистить базу
        String parties1 = "BEGINPARTIES";

        modelObjectClient = model.createObjectClient(modelObjectClient , "SYNCREATEALLLIST" ,  null , user.getId(),  serverVersionBases , parties1 , receivedConnectionID , String.valueOf(usersConnected.isPublisher()) , serviceAuthUser);

      //  System.out.println("GetList-> sendMessageSYNCREATEALLLISTCLient: Отправка SYNCCREATEALLLIST BEGINParties " + user.getUsername());
        message.sendFileInfoObject(modelObjectClient , "User."+username);
    }

    private void trainingSend(UsersModel user , long serverVersionBases , ConnectedModel usersConnected ,  CreateModelClient model ,  List<Long> arr , long user_id , ServiceAuthUser serviceAuthUser)
    {
        List<List<FileInfoModel>> serverModel = mysqlServiceFileList.selectGroupAllList(arr , user_id);
        String parties = "PARTIES";

        send( serverModel  , user  ,  model ,  serverVersionBases ,  usersConnected , parties , serviceAuthUser);

    }

    private void trainingSinglSend(UsersModel user , long serverVersionBases , ConnectedModel usersConnected ,  CreateModelClient model ,  List<Long> arr , long user_id , ServiceAuthUser serviceAuthUser)
    {
        List<List<FileInfoModel>> serverModel = mysqlServiceFileList.selectGroupAllList(arr , user_id);
        String parties = "SINGLUPDATEPARTIES";

        sendSingl( serverModel  , user  ,  model ,  serverVersionBases ,  usersConnected , parties , serviceAuthUser);

    }


    private void trainingSinglSendTemp(UsersModel user , long serverVersionBases , ConnectedModel usersConnected ,  CreateModelClient model ,  List<Long> arr , long user_id , ServiceAuthUser serviceAuthUser)
    {
        List<FileInfoModel> serverModel = mysqlServiceFileList.selectListTemp(arr , user_id);
        String parties = "SINGLUPDATEPARTIES";

        sendSinglTemp( serverModel  ,  user  ,  model ,  serverVersionBases ,  usersConnected ,  parties ,  serviceAuthUser);

    }

    private void sendSinglTemp(List<FileInfoModel> serverModel  , UsersModel user  , CreateModelClient model , long serverVersionBases , ConnectedModel usersConnected , String parties , ServiceAuthUser serviceAuthUser)
    {
            FileInfoModel[] itemArray = new FileInfoModel[serverModel.size()];
            itemArray = serverModel.toArray(itemArray);
            modelObjectClient = model.createObjectClient(modelObjectClient , "SYNSINGLLIST" ,  itemArray , user.getId(),  serverVersionBases , parties , receivedConnectionID , String.valueOf(usersConnected.isPublisher()) , serviceAuthUser);
            message.sendFileInfoObject(modelObjectClient , "User."+username);
    }


    private void sendSingl(List<List<FileInfoModel>> serverModel  , UsersModel user  , CreateModelClient model , long serverVersionBases , ConnectedModel usersConnected , String parties , ServiceAuthUser serviceAuthUser)
    {


        serverModel.forEach(item->
        {
            FileInfoModel[] itemArray = new FileInfoModel[item.size()];
            itemArray = item.toArray(itemArray);
            itemArray = arrayUpdateRows(itemArray);

            modelObjectClient = model.createObjectClient(modelObjectClient , "SYNSINGLLIST" ,  itemArray , user.getId(),  serverVersionBases , parties , receivedConnectionID , String.valueOf(usersConnected.isPublisher()) , serviceAuthUser);

            message.sendFileInfoObject(modelObjectClient , "User."+username);


            //  System.out.println("GetList-> sendMessageSYNCREATEALLLISTCLient: Отправка SYNCCREATEALLLIST PARTIES " + user.getUsername() +" номер партии ");

        });
    }

    private FileInfoModel[]  arrayUpdateRows(FileInfoModel[] itemArray)
    {
        for(int i = 0 ; i < itemArray.length; i++)
        {
            FileInfoModel fim = itemArray[i];
            fim.setChangeRows("UPDATE");
        }

        return itemArray;
    }

    private void sendEndSyncCreate(List<FileInfoModel> serverModel  , UsersModel user  , CreateModelClient model , long serverVersionBases , ConnectedModel usersConnected , String parties , ServiceAuthUser serviceAuthUser)
    {

            FileInfoModel[] itemArray = new FileInfoModel[serverModel.size()];
            itemArray = serverModel.toArray(itemArray);
            modelObjectClient = model.createObjectClient(modelObjectClient , "SYNCREATEALLLIST" ,  itemArray , user.getId(),  serverVersionBases , parties , receivedConnectionID , String.valueOf(usersConnected.isPublisher()) , serviceAuthUser);
            message.sendFileInfoObject(modelObjectClient , "User."+username);

    }


    private void send(List<List<FileInfoModel>> serverModel  , UsersModel user  , CreateModelClient model , long serverVersionBases , ConnectedModel usersConnected , String parties , ServiceAuthUser serviceAuthUser)
    {


        serverModel.forEach(item->
        {
            FileInfoModel[] itemArray = new FileInfoModel[item.size()];
            itemArray = item.toArray(itemArray);


            modelObjectClient = model.createObjectClient(modelObjectClient , "SYNCREATEALLLIST" ,  itemArray , user.getId(),  serverVersionBases , parties , receivedConnectionID , String.valueOf(usersConnected.isPublisher()) , serviceAuthUser);

            message.sendFileInfoObject(modelObjectClient , "User."+username);


          //  System.out.println("GetList-> sendMessageSYNCREATEALLLISTCLient: Отправка SYNCCREATEALLLIST PARTIES " + user.getUsername() +" номер партии ");

        });
    }

    private void sendMessageACTUALListClient(UsersModel user , long serverVersionBases , ConnectedModel usersConnected , ServiceAuthUser serviceAuthUser)
    {
        long user_id = user.getId();
        FileInfoModel[] listFileInfo = {};
        CreateModelClient model = new CreateModelClient();

        modelObjectClient = model.createObjectClient(modelObjectClient , "ACTUAL" ,  listFileInfo , user_id,  serverVersionBases , null , receivedConnectionID , String.valueOf(usersConnected.isPublisher()) , serviceAuthUser);
        message.sendFileInfoObject(modelObjectClient , "User."+username);


        System.out.println("GetList-> sendMessageACTUALListClient: Отправка клиенту фалага что версия является актуально" + user.getUsername());
    }

    private boolean сheckVersionBases(long clientVersionBases, long serverVersionBase)
    {
        if (serverVersionBase > clientVersionBases | clientVersionBases > serverVersionBase)
        {
            return true;
        }
        else
        {
            return false;
        }

    }


    private boolean сheckVersionRows(long clientVersionBases, long serverVersionBase)
    {
        if (serverVersionBase > clientVersionBases)
        {
            return true;
        }
        else
        {
            return false;
        }

    }





}
