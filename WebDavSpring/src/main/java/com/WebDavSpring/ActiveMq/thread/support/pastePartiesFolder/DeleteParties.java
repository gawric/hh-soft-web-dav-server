package com.WebDavSpring.ActiveMq.thread.support.pastePartiesFolder;

import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceListFiles;

import java.util.PriorityQueue;

/**
 * Created by А д м и н on 13.01.2020.
 */
public class DeleteParties {

    private ServiceListFiles mysqlServiceFileList;
    //массив с данными о манипуляция клиента
    private FileInfoModel[] clientListFileInfo;

    private PriorityQueue<Integer> myPriorityDeleteQueue;

    //прокладка со всеми данными для клиента
    private FileInfoObjectTopic modelObjectClient;

    //для отправки сообщений клиентам
    private SendMessage message;

    private UsersModel userModelServer;

    public DeleteParties(ServiceListFiles mysqlServiceFileList , FileInfoModel[] clientListFileInfo , PriorityQueue<Integer> myPriorityDeleteQueue , FileInfoObjectTopic modelObjectClient , SendMessage message , UsersModel userModelServer)
    {
        this.mysqlServiceFileList = mysqlServiceFileList;
        this.clientListFileInfo = clientListFileInfo;
        this.myPriorityDeleteQueue = myPriorityDeleteQueue;
        this.modelObjectClient = modelObjectClient;
        this.message = message;
        this.userModelServer = userModelServer;
    }


    public void releasesDeleteParties()
    {


        mysqlServiceFileList.updateSinglList(clientListFileInfo ,  userModelServer.getId() , false);

        //Отправка сообщений
        getListSynSingList( userModelServer ,  clientListFileInfo , "DELETEPARTIES");

    }

    public void releasesDeleteEndParties()
    {
        mysqlServiceFileList.updateSinglList(clientListFileInfo ,  userModelServer.getId() , false);



                // mysqlServiceFileList.updateListFiles(clientListFileInfo);
                //Отправка сообщений
     getListSynSingList( userModelServer ,  clientListFileInfo , "DELETEENDPARTIES");


    }

    private void getListSynSingList(UsersModel userModelServer , FileInfoModel[] listFileInfo , String parties)
    {
        long user_id = userModelServer.getId();
        String username = userModelServer.getUsername();

        //формирование данных
        long serverVersionBases = mysqlServiceFileList.findVersionBasesUserId(user_id);
        long serverSizeBases;

        if(listFileInfo != null)
        {
            serverSizeBases = listFileInfo.length;
        }
        else
        {
            serverSizeBases = 0;
        }

        modelObjectClient.setListFileInfo(listFileInfo);

        //отправка данных
        String[] textMessage = {String.valueOf(serverVersionBases) , "SYNSINGLLIST" , String.valueOf(serverSizeBases) , String.valueOf(userModelServer.getId()) , parties };
        modelObjectClient.setTextmessage(textMessage);
        System.out.println("Отправка SINSINGLLIST юзеру "  + userModelServer.getUsername());
        message.sendFileInfoObject(modelObjectClient , "User."+username);
        listFileInfo = null;
    }
}
