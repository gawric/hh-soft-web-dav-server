package com.WebDavSpring.ActiveMq.thread.support.lazySinglUpdate;

import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceListFiles;

/**
 * Created by А д м и н on 30.04.2020.
 */
public class LazySinglUpdateSend implements Runnable
{
    private ServiceListFiles mysqlServiceFileList;
    //прокладка со всеми данными для клиента
    private FileInfoObjectTopic modelObjectClient;
    //для отправки сообщений клиентам
    private SendMessage message;
    private UsersModel userModelServer;
    private FileInfoModel[] listFileInfo;
    private String parties ;


    public LazySinglUpdateSend(ServiceListFiles mysqlServiceFileList ,  FileInfoObjectTopic modelObjectClient , SendMessage message , UsersModel userModelServer , FileInfoModel[] listFileInfo , String parties) {
        this.mysqlServiceFileList = mysqlServiceFileList;
        this.modelObjectClient = modelObjectClient;
        this.message = message;
        this.userModelServer = userModelServer;
        this.listFileInfo = listFileInfo;
        this.parties = parties;
    }

    @Override
    public void run(){

        long user_id = userModelServer.getId();
        String username = userModelServer.getUsername();

        //формирование данных
        long serverVersionBases = mysqlServiceFileList.findVersionBasesUserId(user_id);
        long serverSizeBases;

        if(listFileInfo != null)
        {
            serverSizeBases = listFileInfo.length;
        }
        else
        {
            serverSizeBases = 0;
        }

        modelObjectClient.setListFileInfo(listFileInfo);
        if(listFileInfo != null)
        {
            System.out.println("********************************размер отправляемого флага "+listFileInfo.length +"          PARTIES   "+parties);
        }
        else
        {
            System.out.println("********************************размер отправляемого флага "+0 +"          PARTIES   "+parties);
        }

        //отправка данных
        String[] textMessage = {String.valueOf(serverVersionBases) , "LAZYSYNSINGLLIST" , String.valueOf(serverSizeBases) , String.valueOf(userModelServer.getId()) , parties };
        modelObjectClient.setTextmessage(textMessage);
        System.out.println("Отправка LAZYSYNSINGLLIST юзеру "  + userModelServer.getUsername());
        message.sendLazyFileInfoObject(modelObjectClient , "User."+username);
        listFileInfo = null;


    }
}
