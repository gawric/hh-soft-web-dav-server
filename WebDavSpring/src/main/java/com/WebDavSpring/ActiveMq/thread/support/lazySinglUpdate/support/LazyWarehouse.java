package com.WebDavSpring.ActiveMq.thread.support.lazySinglUpdate.support;

import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by А д м и н on 13.11.2020.
 */
@Component
public class LazyWarehouse {
    private static ConcurrentHashMap<Long, Boolean> hashMapRezetLazyBases;
    private static ConcurrentHashMap<Long, Boolean> hashMapResetLazyRows;
    private static ConcurrentHashMap<Long, Boolean> hashMapNewDataLazy;



    public  void  setNewDataLazy(long user_id , Boolean isNewData)
    {
        if(hashMapNewDataLazy == null)
        {
            hashMapNewDataLazy = new ConcurrentHashMap<>();
            hashMapNewDataLazy.put(user_id , isNewData);
        }
        else
        {
            setNewData(user_id ,  isNewData);
        }
    }



    public  void  setResetBases(long user_id , Boolean isResetBases)
    {
        if(hashMapRezetLazyBases == null)
        {
            hashMapRezetLazyBases = new ConcurrentHashMap<>();
            hashMapRezetLazyBases.put(user_id , isResetBases);
        }
        else
        {
            setResetB(user_id ,  isResetBases);
        }
    }

    public  void  setResetRows(long user_id , Boolean isResetRows)
    {
        if(hashMapResetLazyRows == null)
        {
            hashMapResetLazyRows = new ConcurrentHashMap<>();
            hashMapResetLazyRows.put(user_id , isResetRows);
        }
        else
        {
            setResetR(user_id , isResetRows);
        }
    }


    public Boolean isNewDataLazy(long user_id)
    {
        if(hashMapNewDataLazy == null) {
            hashMapNewDataLazy = new ConcurrentHashMap<>();
            hashMapNewDataLazy.put(user_id , false);
            return false;
        }
        else
        {
            if(hashMapNewDataLazy.containsKey(user_id))
            {
                return hashMapNewDataLazy.get(user_id);
            }
            else
            {
                hashMapNewDataLazy.put(user_id , false);
                return hashMapNewDataLazy.get(user_id);
            }

        }

    }

    public Boolean isResetBases(long user_id)
    {
        if(hashMapRezetLazyBases == null) {
            hashMapRezetLazyBases = new ConcurrentHashMap<>();
            hashMapRezetLazyBases.put(user_id , false);
            return false;
        }
        else
        {
            if(hashMapRezetLazyBases.containsKey(user_id))
            {
                return hashMapRezetLazyBases.get(user_id);
            }
            else
            {
                hashMapRezetLazyBases.put(user_id , false);
                return hashMapRezetLazyBases.get(user_id);
            }

        }

    }



    public  Boolean isResetRows(long user_id)
    {
        if(hashMapResetLazyRows == null)
        {
            hashMapResetLazyRows = new ConcurrentHashMap<>();
            hashMapResetLazyRows.put(user_id , false);
            return false;
        }
        else
        {
            if(hashMapResetLazyRows.containsKey(user_id))
            {
                return hashMapResetLazyRows.get(user_id);
            }
            else
            {
                hashMapResetLazyRows.put(user_id , false);
                return hashMapResetLazyRows.get(user_id);
            }


        }
    }

    private  void  setNewData(long user_id , Boolean  isResetBases)
    {
        if(hashMapNewDataLazy.containsKey(user_id))
        {
            hashMapNewDataLazy.replace(user_id , isResetBases);
        }
        else
        {
            hashMapNewDataLazy.put(user_id , isResetBases);
        }
    }


    private  void  setResetB(long user_id , Boolean  isResetBases)
    {
        if(hashMapRezetLazyBases.containsKey(user_id))
        {
            hashMapRezetLazyBases.replace(user_id , isResetBases);
        }
        else
        {
            hashMapRezetLazyBases.put(user_id , isResetBases);
        }
    }

    private  void  setResetR(long user_id , Boolean  isResetBases)
    {
        if(hashMapResetLazyRows.containsKey(user_id))
        {
            hashMapResetLazyRows.replace(user_id , isResetBases);
        }
        else
        {
            hashMapResetLazyRows.put(user_id , isResetBases);
        }
    }
}
