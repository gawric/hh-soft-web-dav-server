package com.WebDavSpring.ActiveMq.thread.support.publisherFolder;

import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.ConnectedModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUsersConnected;

/**
 * Created by А д м и н on 15.04.2020.
 */
public class Publisher {


    private FileInfoObjectTopic modelObjectClient;
    private SendMessage message;
    private ServiceUsersConnected serviceConnected;
    private UsersModel userModelServer;
    private ConnectedModel model;


    public Publisher(ServiceUsersConnected serviceConnected ,  UsersModel userModelServer , FileInfoObjectTopic modelObjectClient , SendMessage message) {
        this.serviceConnected = serviceConnected;
        this.userModelServer = userModelServer;
        this.modelObjectClient = modelObjectClient;
        this.message = message;

    }

    public void getPublisher(int userId , String[] connectionId)
    {
        ConnectedModel modelPublisher = serviceConnected.findPublisher(userId);

        for(String item : connectionId)
        {
            model = serviceConnected.findByClientid(item);
            if(model.isPublisher())
                break;
        }


        if(modelPublisher == null)
        {
            model.setPublisher(true);
            serviceConnected.addConnectedUsersId(model);
        }


    }

    public void sendStatusPublisher(String parties , String[] connectionId , boolean isWorking)
    {
        if(model != null)
        {

            getListSynSingList( userModelServer ,  parties , String.valueOf(model.isPublisher()) , connectionId , String.valueOf(isWorking));
        }
        else
        {
            System.out.println("Publisher->SendStatusPublisher: Критическая ошибка не найдена модель connectionID!!! остановка скрипта" + connectionId);
        }

    }


    //6 - connectionId client
    //7 - публицист я или нет
    //8 - что-то исполняется или нет isWorking(что-бы не запускать сканирование если что-то делается)

    //6 - connectionId client
    //7 - публицист я или нет
    //8 - что-то исполняется или нет isWorking(что-бы не запускать сканирование если что-то делается)
    private void getListSynSingList(UsersModel userModelServer , String parties , String isPublisher , String[] connectionId , String isWorking) {

        String username = userModelServer.getUsername();

        //5 - connectionId не посылается в textMessage теперь идет отдельной строкой
        String[] textMessage = {"" , parties , "", String.valueOf(userModelServer.getId()), parties  , "" , isPublisher , isWorking};
        modelObjectClient.setTextmessage(textMessage);
        modelObjectClient.setConnectionId(connectionId);
        message.sendFileInfoObject(modelObjectClient , "User."+username);
        System.out.println("***** Publisher: Отправка запроса  для "+userModelServer.getUsername()+"  "+connectionId[0]+"  >>> Опеределяем статус клиента он Публицист или Нет?  "+parties+"  ***** Ответ:  " +isPublisher);
    }

}
