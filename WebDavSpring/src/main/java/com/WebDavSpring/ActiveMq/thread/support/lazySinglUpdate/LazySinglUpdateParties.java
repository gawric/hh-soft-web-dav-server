package com.WebDavSpring.ActiveMq.thread.support.lazySinglUpdate;

import com.WebDavSpring.ActiveMq.thread.support.lazySinglUpdate.support.LazyWarehouse;
import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariableBases;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceListFiles;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.PriorityQueue;



/**
 * Created by А д м и н on 30.04.2020.
 */
public class LazySinglUpdateParties {
    private ServiceListFiles mysqlServiceFileList;

    //массив с данными о манипуляция клиента
    private FileInfoModel[] clientListFileInfo;

    private PriorityQueue<Integer> myPrioritySinglUpdatePartiesQueue;

    //прокладка со всеми данными для клиента
    private FileInfoObjectTopic modelObjectClient;

    //для отправки сообщений клиентам
    private SendMessage message;

    private UsersModel userModelServer;
    private LazyWarehouse lazyWarehouse;

    public LazySinglUpdateParties(ServiceListFiles mysqlServiceFileList , FileInfoModel[] clientListFileInfo , PriorityQueue<Integer> myPrioritySinglUpdatePartiesQueue , FileInfoObjectTopic modelObjectClient , SendMessage message , UsersModel userModelServer , LazyWarehouse lazyWarehouse)
    {
        this.mysqlServiceFileList = mysqlServiceFileList;
        this.clientListFileInfo = clientListFileInfo;
        this.myPrioritySinglUpdatePartiesQueue = myPrioritySinglUpdatePartiesQueue;
        this.modelObjectClient = modelObjectClient;
        this.message = message;
        this.userModelServer = userModelServer;
        this.lazyWarehouse = lazyWarehouse;
    }

    private void insertFileInfo(FileInfoModel[] clientListFileInfo , int user_id)
    {

        mysqlServiceFileList.updateSinglList(clientListFileInfo , user_id , false);
    }

    public void releasesLazySinglBeginUpdateParties()
    {
        int objectIndex = myPrioritySinglUpdatePartiesQueue.size() + 1;
        myPrioritySinglUpdatePartiesQueue.add(objectIndex);

        LazySinglUpdateSend send = new LazySinglUpdateSend( mysqlServiceFileList ,   modelObjectClient ,  message ,  userModelServer , clientListFileInfo , "LAZYSINGLBEGINPARTIES");
        send.run();


        myPrioritySinglUpdatePartiesQueue.remove(objectIndex);
    }

    public void releasesLazySinglUpdateParties(ThreadPoolTaskExecutor task)
    {
        int objectIndex = myPrioritySinglUpdatePartiesQueue.size() + 1;
        myPrioritySinglUpdatePartiesQueue.add(objectIndex);

        checkNewData(clientListFileInfo , userModelServer.getId());
        saveNewVersionBases( lazyWarehouse ,  userModelServer.getId());

        FileInfoModel[] resize = convertVersionRowsAndBases(clientListFileInfo, userModelServer.getId());


        LazySinglUpdateSend send = new LazySinglUpdateSend( mysqlServiceFileList ,   modelObjectClient ,  message ,  userModelServer ,  resize ,  "LAZYSINGLUPDATEPARTIES");
        task.execute(send);



        insertFileInfo(resize , userModelServer.getId());




        myPrioritySinglUpdatePartiesQueue.remove(objectIndex);
    }


    public void releasesLazySinglEndUpdateParties()
    {

        while (true)
        {
            try
            {
                Thread.sleep(50);

            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            //если последний update отправлен
            if(myPrioritySinglUpdatePartiesQueue.size() == 0 )
            {
                checkNewData(clientListFileInfo , userModelServer.getId());
                saveNewVersionBases( lazyWarehouse ,  userModelServer.getId());

                FileInfoModel[] resize = convertVersionRowsAndBases(clientListFileInfo, userModelServer.getId());
                insertFileInfo(resize , userModelServer.getId());


                LazySinglUpdateSend send = new LazySinglUpdateSend( mysqlServiceFileList ,   modelObjectClient ,  message ,  userModelServer , clientListFileInfo , "LAZYSINGLENDUPDATEPARTIES");
                send.run();

                saveNewVersionRows(lazyWarehouse  , userModelServer.getId());

                //Отправка сообщений
                //getListSynSingList( userModelServer ,  clientListFileInfo , "SINGLENDUPDATEPARTIES");
                break;
            }
        }


    }

    private void saveNewVersionRows(LazyWarehouse lazyWarehouse  ,long user_id )
    {
        Boolean isNewData = lazyWarehouse.isNewDataLazy(user_id);
        System.out.print("setNewVersionRows-> isNewData  " +isNewData);
        if(isNewData)
        {
            if(!lazyWarehouse.isResetRows(user_id))
            {
                lazyWarehouse.setResetRows(user_id , true);
                lazyWarehouse.setNewDataLazy(user_id , false);
                lazyWarehouse.setResetBases(user_id , false);

                staticVariableBases.setLastVersionRows( user_id ,  0);
                mysqlServiceFileList.setVersionAllRows(user_id , 0);
                mysqlServiceFileList.deleteFileListTempAllUserId( user_id);

                System.out.print("END->saveNewVersionRows->ПЕРЕВОД isData в состояние false");
            }
        }
    }


    private FileInfoModel[] convertVersionRowsAndBases(FileInfoModel[] listFileInfo , long user_id)
    {
        if(listFileInfo == null) return null;

        for(int i=0; i < listFileInfo.length; i++)
        {
            FileInfoModel fim = listFileInfo[i];

            if(fim != null)
            {
                fim.setVersionUpdateRows(0);
                fim.setVersionUpdateBases(staticVariableBases.getLastVersionBases(user_id));
            }
        }

        return listFileInfo;
    }
    private void checkNewData(FileInfoModel[] listFileInfo, long user_id)
    {
        if(!isEmpty(listFileInfo))
        {
            System.out.print("НОВАЯ ДАТА ОБНОВИТЬ !!!!");
            lazyWarehouse.setNewDataLazy(user_id , true);
        }
    }

    private void saveNewVersionBases(LazyWarehouse lazyWarehouse , long user_id)
    {
        Boolean isNewData = lazyWarehouse.isNewDataLazy(user_id);
        System.out.println("Save New Data is  "+isNewData + "  "+"LazyWareHouse isResetBases "+  lazyWarehouse.isResetBases(user_id));
        if(isNewData)
        {
            if(!lazyWarehouse.isResetBases(user_id))
            {
                lazyWarehouse.setResetBases(user_id , true);


                long versionbases = mysqlServiceFileList.getUserMaxVersionBases(user_id) + 1;
                staticVariableBases.setLastVersionBases(user_id , versionbases);
                mysqlServiceFileList.setVersionAllRowsBases(user_id , versionbases);
            }
        }
    }

    private Boolean isEmpty(FileInfoModel[] listFileInfo)
    {
        Boolean check = true;
        if(listFileInfo == null) return check;

        for(int i =0 ; i < listFileInfo.length; i++)
        {
            FileInfoModel fim = listFileInfo[i];
            if(fim != null)
            {
                check = false;
                break;
            }
        }

        return check;
    }

}
