package com.WebDavSpring.ActiveMq.thread.support.syncSinglListFolder;

import com.WebDavSpring.ActiveMq.thread.support.StopRequest;
import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.ActiveMq.thread.support.pastePartiesFolder.DeleteParties;
import com.WebDavSpring.ActiveMq.thread.support.pastePartiesFolder.PasteParties;
import com.WebDavSpring.ActiveMq.thread.support.pastePartiesFolder.SinglUpdateParties;
import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.model.syncSinglListModel.SyncSinglRequestModel;
import com.WebDavSpring.WebDavSpring.model.wokingModel.SyncModel;

/**
 * Created by А д м и н on 09.10.2019.
 */
public class SyncSingList
{

    private SyncModel syncModel;
    private SyncSinglRequestModel requestModel;


    public SyncSingList(SyncModel syncModel , SyncSinglRequestModel requestModel){
        this.syncModel = syncModel;
        this.requestModel = requestModel;

    }

    public void startDELETEBEGINPARTIES() {
        stopRequest(requestModel.getUserModelServer() ,  requestModel.getModelObjectClient() ,  syncModel.getMessage());

        syncModel.getMysqlServiceFileList().updateListFiles(requestModel.getClientListFileInfo());
        //отправка сообщений
        syncModel.getTrainingSendMessage().getListSynSingList( syncModel ,  requestModel, staticVariable.DELETEBEGINPARTIES);


    }

    public void startDELETEPARTIES() {

        DeleteParties object = new DeleteParties( syncModel.getMysqlServiceFileList() ,  requestModel.getClientListFileInfo() , syncModel.getMyPriorityDeleteQueue() ,  requestModel.getModelObjectClient() ,  syncModel.getMessage() ,  requestModel.getUserModelServer());
        object.releasesDeleteParties();
    }

    public void startDELETEENDPARTIES() {

        DeleteParties object = new DeleteParties( syncModel.getMysqlServiceFileList() ,  requestModel.getClientListFileInfo()  , syncModel.getMyPriorityDeleteQueue() ,  requestModel.getModelObjectClient() ,  syncModel.getMessage() ,  requestModel.getUserModelServer());
        object.releasesDeleteEndParties();

    }

    public void startPASTEBEGINPARTIES() {
        stopRequest(requestModel.getUserModelServer() ,  requestModel.getModelObjectClient() ,  syncModel.getMessage());

        PasteParties object = new PasteParties( syncModel.getMysqlServiceFileList() ,  requestModel.getClientListFileInfo() , syncModel.getMyPriorityPasteQueue() ,  requestModel.getModelObjectClient() ,  syncModel.getMessage() ,  requestModel.getUserModelServer());
        object.setNewVersionRows();
        //отправка сообщений
        syncModel.getTrainingSendMessage().getListSynSingList( syncModel ,  requestModel, staticVariable.PASTEBEGINPARTIES);

    }

    public void startPASTEPARTIES() {
        PasteParties object = new PasteParties( syncModel.getMysqlServiceFileList() ,  requestModel.getClientListFileInfo() , syncModel.getMyPriorityPasteQueue() ,  requestModel.getModelObjectClient() ,  syncModel.getMessage() ,  requestModel.getUserModelServer());
        syncModel.setLastVersionUpdateRows(object.getNewVersionRows());
        object.releasesPasteParties(syncModel.getLastVersionUpdateRows());

    }

    public void startPASTEENDPARTIES()
    {
        PasteParties object = new PasteParties(syncModel.getMysqlServiceFileList() ,  requestModel.getClientListFileInfo() , syncModel.getMyPriorityPasteQueue() ,  requestModel.getModelObjectClient() ,  syncModel.getMessage() ,  requestModel.getUserModelServer());
        syncModel.setLastVersionUpdateRows(object.getNewVersionRows());
        object.releasesPasteEndparties(syncModel.getLastVersionUpdateRows());
    }

    public void startSINGLBEGINUPDATEPARTIES(){
        //Останавливаем сканирование т.к определили что запрос идет RENAME, а не просто SINGL
        if(requestModel.getUserRequestParties().equals(staticVariable.RENAMESINGLBEGINUPDATEPARTIES))
        {
            stopRequest(requestModel.getUserModelServer() ,  requestModel.getModelObjectClient() ,  syncModel.getMessage());
        }


        SinglUpdateParties SinglUpdateParties = new SinglUpdateParties(syncModel.getMysqlServiceFileList() , requestModel.getClientListFileInfo() , syncModel.getMyPriorityPasteQueue()  ,  requestModel.getModelObjectClient() ,  syncModel.getMessage() ,  requestModel.getUserModelServer());
        SinglUpdateParties.releasesSinglBeginUpdateParties();


    }

    public void startSINGLUPDATEPARTIES(){

        //Останавливаем сканирование т.к определили что запрос идет RENAME, а не просто SINGL
        if(requestModel.getUserRequestParties().equals(staticVariable.RENAMESINGLUPDATEPARTIES))
        {
            stopRequest(requestModel.getUserModelServer() ,  requestModel.getModelObjectClient() ,  syncModel.getMessage());
        }



        SinglUpdateParties singlUpdateParties = new SinglUpdateParties(syncModel.getMysqlServiceFileList() , requestModel.getClientListFileInfo(), syncModel.getMyPrioritySinglUpdateQueue() ,  requestModel.getModelObjectClient() ,  syncModel.getMessage() ,  requestModel.getUserModelServer());
        syncModel.setLastVersionUpdateRows(singlUpdateParties.getNewVersionRows());
        singlUpdateParties.releasesSinglUpdateParties(syncModel.getTask());

    }

    public void startSINGLENDUPDATEPARTIES() {
        //Останавливаем сканирование т.к определили что запрос идет RENAME, а не просто SINGL
        //SINGLEND - запрос испльзуется для обновления базы без удаления файлов
        if(requestModel.getUserRequestParties().equals(staticVariable.RENAMESINGLENDUPDATEPARTIES))
        {
            stopRequest(requestModel.getUserModelServer() ,  requestModel.getModelObjectClient() ,  syncModel.getMessage());
        }

        SinglUpdateParties singlUpdateParties = new SinglUpdateParties(syncModel.getMysqlServiceFileList() , requestModel.getClientListFileInfo() , syncModel.getMyPrioritySinglUpdateQueue() ,  requestModel.getModelObjectClient()  ,  syncModel.getMessage() ,  requestModel.getUserModelServer());
        syncModel.setLastVersionUpdateRows(singlUpdateParties.getNewVersionRows());
        singlUpdateParties.releasesSinglEndUpdateParties();

    }


    private void stopRequest(UsersModel userModelServer , FileInfoObjectTopic modelObjectClient , SendMessage message)
    {
        StopRequest stopRequest = new StopRequest( userModelServer ,  modelObjectClient ,  message);
        stopRequest.sendSTOPNODELETEBASES(requestModel.getReceivedConnectionID());
    }




}
