package com.WebDavSpring.ActiveMq.thread.support;

import com.WebDavSpring.ActiveMq.thread.support.lazySinglUpdate.support.LazyWarehouse;
import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.ActiveMq.thread.support.lazySinglUpdate.LazySinglUpdateParties;
import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.lazyModel.LazyModel;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceListFiles;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.PriorityQueue;

/**
 * Created by А д м и н on 30.04.2020.
 */
public class LazySyncSingList {

    //Какой запрос пришел от клиента на обновление /Update/EndUpdate
    private String userRequestParties;
    //Mysql взаимодействие
    private ServiceListFiles mysqlServiceFileList;
    //массив с данными о манипуляция клиента
    private FileInfoModel[] clientListFileInfo;
    //прокладка со всеми данными для клиента
    private FileInfoObjectTopic modelObjectClient;
    //для отправки сообщений клиентам
    private SendMessage message;
    private UsersModel userModelServer;

    private PriorityQueue<Integer> myPrioritySinglUpdateQueue;

    private String[] receivedConnectionID;
    private ThreadPoolTaskExecutor task;
    private LazyWarehouse lazyWarehouse;

    public LazySyncSingList(LazyModel lazyModel , String userRequestParties , LazyWarehouse lazyWarehouse)
    {
        this.userRequestParties = userRequestParties;
        this.mysqlServiceFileList = lazyModel.getMysqlServiceFileList();
        this.clientListFileInfo =  lazyModel.getClientListFileInfo();
        this.modelObjectClient = lazyModel.getModelObjectClient();
        this.message = lazyModel.getMessage();
        this.userModelServer = lazyModel.getUserModelServer();
        this.lazyWarehouse = lazyWarehouse;
        this.myPrioritySinglUpdateQueue = lazyModel.getMyPrioritySinglUpdateQueue();

        this.receivedConnectionID = lazyModel.getReceivedConnectionID();
        this.task =  lazyModel.getTask();

    }

    public void CreateLazySingList()
    {
        if(userRequestParties.equals("LAZYSINGLBEGINUPDATEPARTIES")) {

            LazySinglUpdateParties SinglUpdateParties = new LazySinglUpdateParties(mysqlServiceFileList , clientListFileInfo , myPrioritySinglUpdateQueue ,  modelObjectClient ,  message ,  userModelServer , lazyWarehouse);
            SinglUpdateParties.releasesLazySinglBeginUpdateParties();

          //  lazyWarehouse.setNewDataLazy(userModelServer.getId() , false);
            //lazyWarehouse.setResetBases(userModelServer.getId() , false);
            lazyWarehouse.setResetRows(userModelServer.getId() , false);
            System.out.println("Все CreateLazySingList  - LAZYSINGLBEGINUPDATEPARTIES переводим в состояние false");
        }
        else if(userRequestParties.equals("LAZYSINGLUPDATEPARTIES")) {

            LazySinglUpdateParties SinglUpdateParties = new LazySinglUpdateParties(mysqlServiceFileList , clientListFileInfo , myPrioritySinglUpdateQueue ,  modelObjectClient ,  message ,  userModelServer , lazyWarehouse);
            SinglUpdateParties.releasesLazySinglUpdateParties(task);


        }
        else if(userRequestParties.equals("LAZYSINGLENDUPDATEPARTIES")) {

            LazySinglUpdateParties SinglUpdateParties = new LazySinglUpdateParties(mysqlServiceFileList , clientListFileInfo , myPrioritySinglUpdateQueue ,  modelObjectClient ,  message ,  userModelServer , lazyWarehouse);
            SinglUpdateParties.releasesLazySinglEndUpdateParties();

        }

    }



}
