package com.WebDavSpring.ActiveMq.thread.support;

import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;

import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;


/**
 * Created by А д м и н on 24.01.2020.
 */
//Оповещаем всех клиентов, что мы ищем файлы для синхронизации
//обычно используется в клиенте для поиска данных в базе
public class SearchFilesServer {

    //для отправки сообщений клиентам
    private SendMessage message;
    //прокладка со всеми данными для клиента
    private FileInfoObjectTopic modelObjectClient;


    private String[] receivedConnectionID;

    public SearchFilesServer(SendMessage message ,FileInfoObjectTopic modelObjectClient  , String[] receivedConnectionID)
    {
        this.message = message;
        this.modelObjectClient= modelObjectClient;
        this.receivedConnectionID = receivedConnectionID;
    }

    public void SendBeginClientInfo(UsersModel userModelServer) {

        getListSynSingList(userModelServer , "SEARCHBEGINFILES");
    }
    public void SendPartiesClientInfo(UsersModel userModelServer) {

        getListSynSingList(userModelServer , "SEARCHPARTIESFILES");

    }
    public void SendEndClientInfo(UsersModel userModelServer) {

        getListSynSingList(userModelServer , "SEARCHENDFILES");
    }

    private void getListSynSingList(UsersModel userModelServer , String parties)
    {

        String username = userModelServer.getUsername();

        String[] textMessage = {"" , parties , "", String.valueOf(userModelServer.getId()), parties };
        modelObjectClient.setTextmessage(textMessage);
        message.sendFileInfoObject(modelObjectClient , "User."+username);

        System.out.println("***** Отправка запроса  >>> ИДЕТ ПОИСК ФАЙЛОВ НА КЛИЕНТЕ SendMessage "+parties+"  ***** ");
    }



}
