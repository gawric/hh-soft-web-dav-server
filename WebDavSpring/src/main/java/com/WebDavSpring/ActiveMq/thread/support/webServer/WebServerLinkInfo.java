package com.WebDavSpring.ActiveMq.thread.support.webServer;

import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.model.WebServerLinkInfoModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceWebServerLinkInfo;

import static com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable.hrefToLocationRootFolder;

/**
 * Created by А д м и н on 25.08.2020.
 */
public class WebServerLinkInfo {

    private ServiceWebServerLinkInfo serviceWebServerLinkInfo;
    private FileInfoObjectTopic modelObjectClient;
    private SendMessage message;


    public WebServerLinkInfo(ServiceWebServerLinkInfo serviceWebServerLinkInfo , SendMessage message)
    {
        this.serviceWebServerLinkInfo = serviceWebServerLinkInfo;
        this.message = message;
    }

    public void getLink(String token , String username)
    {
        WebServerLinkInfoModel modelToken = serviceWebServerLinkInfo.findByToken(token);

       if(modelToken != null)
       {
           sendWebServerAnswer(username , staticVariable.ANSWERWEBDAVSERVER ,modelToken.getFilesystemlink() , modelToken.getFileName() , token , modelToken.getType());
       }
       else
       {
           sendWebServerAnswer(username , staticVariable.ANSWERWEBDAVSERVER , "error", "error" , "error" , "error");
       }


    }



    private void sendWebServerAnswer(String username , String parties ,String fileSystemPath , String fileName , String tokenId , String type)
    {
        FileInfoObjectTopic modelObjectClient = new FileInfoObjectTopic();
        modelObjectClient.setUsername(username);

        String[] textMessage = {"" , parties , "", "0", parties  , fileSystemPath , type , tokenId , fileName};
        modelObjectClient.setTextmessage(textMessage);
        String request = staticVariable.ANSWERWEBDAVSERVER;
        message.sendFileInfoObjectToHttpServer(modelObjectClient, request);
        System.out.println("***** WebServer : Отправка запроса  для " +username+"  "+"  >>> Отправляем WebServer ссылку на скачивание " + fileSystemPath);
    }
}
