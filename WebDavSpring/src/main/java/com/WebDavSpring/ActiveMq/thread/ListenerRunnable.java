package com.WebDavSpring.ActiveMq.thread;

import com.WebDavSpring.ActiveMq.thread.support.*;
import com.WebDavSpring.ActiveMq.thread.support.errorAlert.ErrorAlertRequest;
import com.WebDavSpring.ActiveMq.thread.support.generatedLink.GeneratedLink;
import com.WebDavSpring.ActiveMq.thread.support.getListFolder.TrainingGetList;
import com.WebDavSpring.ActiveMq.thread.support.publisherFolder.TrainingPublisher;
import com.WebDavSpring.ActiveMq.thread.support.searchFolder.TrainingSearchParties;
import com.WebDavSpring.ActiveMq.thread.support.syncCreateAllListFolder.TrainingSyncCreateAllList;
import com.WebDavSpring.ActiveMq.thread.support.syncSinglListFolder.SyncSingList;
import com.WebDavSpring.ActiveMq.thread.support.syncSinglListFolder.TrainingSyncSinglList;
import com.WebDavSpring.ActiveMq.thread.support.webServer.WebServerLinkInfo;
import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.MailSender.JavaMail;
import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.model.searchModel.SearchModelObj;
import com.WebDavSpring.WebDavSpring.model.searchModel.SearchModelRequest;
import com.WebDavSpring.WebDavSpring.model.syncCreateModel.SyncCreateAllListModel;
import com.WebDavSpring.WebDavSpring.model.syncSinglListModel.SyncSinglRequestModel;
import com.WebDavSpring.WebDavSpring.model.wokingModel.SyncModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceErrorAlert;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceWebServerLinkInfo;

/**
 * Created by А д м и н on 20.09.2019.
 */

public class ListenerRunnable  implements Runnable
{

    private FileInfoObjectTopic modelObjectClient;
    private String username;
    //все обьекты здесь
    private SyncModel syncModel;
    private String[] receivedConnectionID;

    //Используем на клиенте и сервере
    //запрос от клиента к серверу GETLIST -
    //Ответ от сервера Empty - база пустая (нужно сканирование склиента для заполнения)
    //Ответ от сервера OK - возвращает всю базу данных
    //Ответ от сервер ACTUAL - база данных на клиенте самая новая
    private String userRequest;
    private long clientVersionBases;
    private FileInfoModel[] clientListFileInfo;
    private String userRequestParties;

    //private Long userRequestSizeDate;

    public ListenerRunnable(SyncModel syncModel)
    {
        this.syncModel = syncModel;
    }


    public void setModelObjectClient(FileInfoObjectTopic modelObjectClient)
    {
       // System.out.println(this.toString()+"Новый класс");
        this.modelObjectClient = modelObjectClient;
        this.username = modelObjectClient.getUsername();
        this.clientVersionBases = Long.parseLong(modelObjectClient.getTextmessage()[0]);
        this.userRequest = modelObjectClient.getTextmessage()[1];
        this.userRequestParties = modelObjectClient.getTextmessage()[4];
        this.receivedConnectionID = modelObjectClient.getConnectionId();

        if(modelObjectClient.getListFileInfo() != null)
        {
            FileInfoModel[] copiedArray = new FileInfoModel[modelObjectClient.getListFileInfo().length];
            System.arraycopy(modelObjectClient.getListFileInfo(), 0, copiedArray, 0, modelObjectClient.getListFileInfo().length);
            this.clientListFileInfo = copiedArray;
        }

    }


    @Override
    public void run()
    {
        UsersModel userModelServer = syncModel.getUpdateSql().getFindUserIdByUsername(username);


        //проверка что юзер у нас зареган
        if (userModelServer != null) {
            //запрос от юзера - получить актуальную базу
            if(userRequest.equals(staticVariable.GETLIST)) {

                TrainingGetList trainingGetList = new TrainingGetList( syncModel ,  userModelServer ,  receivedConnectionID);
                trainingGetList.startGetList(modelObjectClient,username,clientVersionBases , syncModel.getServiceAuthUser());
            }

            //запрос от юзера я публицист или кто-то другой
            //что-бы начать плановое сканирование папок
            //0 - заглушка версия базы
            //1 - запрос
            //2 - userid
            //3 - заглушка
            //4 - запрос
            //5 - connectionID от клиента
            else if(userRequest.equals(staticVariable.STATUSPUBLISHER)) {

                TrainingPublisher trainingPublisher =  new TrainingPublisher( syncModel ,  userModelServer ,  receivedConnectionID);
                trainingPublisher.startPublisher(modelObjectClient);
                trainingPublisher.startSend();

            }
            else if(userRequest.equals(staticVariable.ALERTERROR)) {

                ErrorAlertRequest errorAlertRequest = new ErrorAlertRequest(modelObjectClient.getTextmessage(), syncModel , userModelServer );
                errorAlertRequest.saveErrorAlert();

            }
            //запрос от юзера пересоздать всю базу
            else if(userRequest.equals(staticVariable.SYNCREATEALLLIST)) {

                String[] receivedConnectionID = getReceivedConnectionID( this.receivedConnectionID);

                SyncCreateAllListModel alllistModel = createModelAllList( syncModel ,  userModelServer);

                TrainingSyncCreateAllList trainingSyncCreateAllList = new TrainingSyncCreateAllList(alllistModel);
                trainingSyncCreateAllList.creatAllList(userModelServer.getId());


            }
            //нужно тестировать радикально переделал
            else if(userRequest.equals(staticVariable.SEARCH)) {
                String[] receivedConnectionID = getReceivedConnectionID( this.receivedConnectionID);


                SearchModelObj searchModelObj = createModelSearchObj(syncModel);
                SearchModelRequest searchModelRequest = createModelSearchRequest( userModelServer ,  userRequestParties ,  clientListFileInfo ,  receivedConnectionID ,  modelObjectClient);

                TrainingSearchParties trainingSearchParties = new TrainingSearchParties(searchModelObj , searchModelRequest);
                trainingSearchParties.startTrainingSearchParties();
            }
            //запрос от юзера перезаписать часть базы(Срабатывает при event-ах копирования/удаления/вставки)
            else if(userRequest.equals(staticVariable.SYNSINGLLIST)) {


                String[] receivedConnectionID = getReceivedConnectionID(this.receivedConnectionID);


                SyncSinglRequestModel requestModel = createModelSingRequestModel(userModelServer ,  userRequestParties ,  clientListFileInfo ,  receivedConnectionID , modelObjectClient);

                TrainingSyncSinglList trainingSyncSinglList = new TrainingSyncSinglList(syncModel , requestModel);
                trainingSyncSinglList.creatSinglList();



            }

            else if(userRequest.equals(staticVariable.CHECKWORKINGSERVER)) {
                String[] connectionID = getReceivedConnectionID( receivedConnectionID);

                CheckWorkingServer searchFilesServer = new CheckWorkingServer( syncModel.getServiceConnected() ,   userModelServer ,  modelObjectClient ,  syncModel.getMessage() ,  syncModel.getUserProperties());
                searchFilesServer.sendStatusPublisher(userModelServer.getId() ,  connectionID);
            }
            else if(userRequest.equals(staticVariable.WEBSERVERREQUEST)) {

                //textmessage - [4] token
                //textmessage - [1] request
                String token = (modelObjectClient.getTextmessage()[4]);
                WebServerLinkInfo webServerLinkInfo = new WebServerLinkInfo(syncModel.getServiceWebServerLinkInfo() , syncModel.getMessage());
                webServerLinkInfo.getLink(token , username);
            }
            else if(userRequest.equals(staticVariable.REQUESTGENLINK)) {

               String[] receivedConnectionID = getReceivedConnectionID( this.receivedConnectionID);
               long listFilesRow_id = Long.parseLong(modelObjectClient.getTextmessage()[5]);
               GeneratedLink generatedLink = new GeneratedLink(syncModel.getServiceWebServerLinkInfo() , syncModel.getMessage() , syncModel.getMysqlServiceFileList());
               generatedLink.start(username  ,  userModelServer.getId() , listFilesRow_id , receivedConnectionID , syncModel.getWebDavServerPort());
            }
            else {
                System.out.println("ListenerRunnable->run Пользователь с таким логином и пароле не найденны");
            }

        }

    }



    private SyncCreateAllListModel createModelAllList(SyncModel syncModel , UsersModel userModelServer)
    {
        SyncCreateAllListModel alllistModel = new SyncCreateAllListModel();

        alllistModel.setClientListFileInfo(clientListFileInfo);
        alllistModel.setModelObjectClient(modelObjectClient);
        alllistModel.setMyPriorityQueue(syncModel.getMyPriorityQueue());
        alllistModel.setReceivedConnectionID(receivedConnectionID);
        alllistModel.setUpdateSql(syncModel.getUpdateSql());
        alllistModel.setUserModelServer(userModelServer);
        alllistModel.setUserRequestParties(userRequestParties);
        alllistModel.setMessage(syncModel.getMessage());
        alllistModel.setMysqlServiceFileList(syncModel.getMysqlServiceFileList());
        alllistModel.setTrainingSendMessage(syncModel.getTrainingSendMessage());

        return alllistModel;
    }

    private SearchModelObj createModelSearchObj(SyncModel syncModel)
    {
        SearchModelObj searchModelObj = new SearchModelObj();
        searchModelObj.setUpdateSql(syncModel.getUpdateSql());

        searchModelObj.setMessage(syncModel.getMessage());


        return searchModelObj;
    }



    private SearchModelRequest createModelSearchRequest( UsersModel userModelServer , String userRequestParties , FileInfoModel[] clientListFileInfo , String[] receivedConnectionID , FileInfoObjectTopic fileInfoObjectTopic)
    {

        SearchModelRequest searchModelRequest = new SearchModelRequest();
        searchModelRequest.setUserModelServer(userModelServer);
        searchModelRequest.setUserRequestParties(userRequestParties);
        searchModelRequest.setModelObjectClient(fileInfoObjectTopic);
        searchModelRequest.setReceivedConnectionID(receivedConnectionID);
        searchModelRequest.setClientListFileInfo(clientListFileInfo);

        return searchModelRequest;
    }

    private SyncSinglRequestModel createModelSingRequestModel(UsersModel userModelServer , String userRequestParties , FileInfoModel[] clientListFileInfo , String[] receivedConnectionID , FileInfoObjectTopic fileInfoObjectTopic)
    {
        SyncSinglRequestModel syncSinglModelRequest = new SyncSinglRequestModel();

        syncSinglModelRequest.setUserModelServer(userModelServer);
        syncSinglModelRequest.setUserRequestParties(userRequestParties);
        syncSinglModelRequest.setModelObjectClient(fileInfoObjectTopic);
        syncSinglModelRequest.setReceivedConnectionID(receivedConnectionID);
        syncSinglModelRequest.setClientListFileInfo(clientListFileInfo);

        return syncSinglModelRequest;
    }





    private String[] getReceivedConnectionID(String[] receivedConnectionID)
    {
        return receivedConnectionID.clone();
    }






}
