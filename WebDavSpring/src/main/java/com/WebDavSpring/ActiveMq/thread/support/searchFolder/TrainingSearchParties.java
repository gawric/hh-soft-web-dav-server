package com.WebDavSpring.ActiveMq.thread.support.searchFolder;


import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.searchModel.SearchModelObj;
import com.WebDavSpring.WebDavSpring.model.searchModel.SearchModelRequest;

/**
 * Created by А д м и н on 07.05.2020.
 */
public class TrainingSearchParties {

    private SearchParties searchParties;
    private SearchModelObj searchModelObj;
    private SearchModelRequest searchModelRequest;

    public TrainingSearchParties(SearchModelObj searchModelObj , SearchModelRequest searchModelRequest) {
        this.searchModelObj = searchModelObj;
        this.searchModelRequest = searchModelRequest;
        searchParties = new SearchParties( searchModelObj ,  searchModelRequest);
    }

    public void startTrainingSearchParties() {

        //не тестировалась для всех запросов, нужны тесты
        equalsRequest();

    }

    private void equalsRequest(){

        if(searchModelRequest.getUserRequestParties().equals(staticVariable.SEARCHBEGINFILES)) {

            searchParties.startSEACRHBEGINFILES();

        }
        else if(searchModelRequest.getUserRequestParties().equals(staticVariable.SEARCHENDFILES)) {

            searchParties.startSEARCHENDFILES();

        }
        else if(searchModelRequest.getUserRequestParties().equals(staticVariable.SEARCHPARTIESFILES)) {

            searchParties.startPARTIESFILES();

        }
    }

}
