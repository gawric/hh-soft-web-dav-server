package com.WebDavSpring.ActiveMq.thread;

import com.WebDavSpring.ActiveMq.thread.support.LazySyncSingList;
import com.WebDavSpring.ActiveMq.thread.support.lazySinglUpdate.support.LazyWarehouse;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.lazyModel.LazyModel;
import com.WebDavSpring.WebDavSpring.model.UsersModel;

/**
 * Created by А д м и н on 30.04.2020.
 */
public class ListenerRunnableLazy implements Runnable {


    private FileInfoObjectTopic modelObjectClient;
    private String username;
    private LazyWarehouse lazyWarehouse;

    private String[] receivedConnectionID;



    //Используем на клиенте и сервере
    //запрос от клиента к серверу GETLIST -
    //Ответ от сервера Empty - база пустая (нужно сканирование склиента для заполнения)
    //Ответ от сервера OK - возвращает всю базу данных
    //Ответ от сервер ACTUAL - база данных на клиенте самая новая
    private String userRequest;

    private FileInfoModel[] clientListFileInfo;
    private String userRequestParties;
    private LazyModel lazyModel;

    public ListenerRunnableLazy(LazyModel lazyModel , LazyWarehouse lazyWarehouse) {

        this.lazyModel = lazyModel;
        this.lazyWarehouse = lazyWarehouse;
    }

    public void setModelObjectClient(FileInfoObjectTopic modelObjectClient) {
        initializingObjectClient(modelObjectClient);
        copyArray(modelObjectClient);
    }

    @Override
    public void run() {


        UsersModel userModelServer = getUserModelServer(username);

        lazyModel.setUserModelServer(userModelServer);
        receivedConnectionID = getCloneConnectionID();
        lazyModel.setClientListFileInfo(clientListFileInfo);
        lazyModel.setUserRequestParties(userRequestParties);
        lazyModel.setReceivedConnectionID(receivedConnectionID);
        lazyModel.setModelObjectClient(modelObjectClient);

        //запрос от юзера перезаписать часть базы(Срабатывает при event-ах копирования/удаления/вставки)
        if(userRequest.equals("LAZYSYNSINGLLIST")) {

            LazySyncSingList syncSing = new LazySyncSingList(lazyModel , userRequestParties , lazyWarehouse);
            syncSing.CreateLazySingList();
        }
    }


     private UsersModel getUserModelServer(String username) {
        return lazyModel.getUpdateSql().getFindUserIdByUsername(username);
     }
    private String[] getCloneConnectionID() {
        return this.receivedConnectionID.clone();
    }

    private void initializingObjectClient(FileInfoObjectTopic modelObjectClient) {
        // System.out.println(this.toString()+"Новый класс");
        this.modelObjectClient = modelObjectClient;
        this.username = modelObjectClient.getUsername();
        // this.clientVersionBases = Long.parseLong(modelObjectClient.getTextmessage()[0]);
        this.userRequest = modelObjectClient.getTextmessage()[1];
        // this.userRequestSizeDate =Long.parseLong(modelObjectClient.getTextmessage()[2]);
        this.userRequestParties = modelObjectClient.getTextmessage()[4];
        this.receivedConnectionID = modelObjectClient.getConnectionId();
    }
    private void copyArray(FileInfoObjectTopic modelObjectClient) {
        if(modelObjectClient.getListFileInfo() != null) {
            FileInfoModel[] copiedArray = new FileInfoModel[modelObjectClient.getListFileInfo().length];
            System.arraycopy(modelObjectClient.getListFileInfo(), 0, copiedArray, 0, modelObjectClient.getListFileInfo().length);
            this.clientListFileInfo = copiedArray;
        }
    }
}
