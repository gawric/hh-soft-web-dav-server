package com.WebDavSpring.ActiveMq.thread.support.syncCreateAllListFolder;


import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariableBases;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.syncCreateModel.SyncCreateAllListModel;

/**
 * Created by А д м и н on 07.10.2019.
 */
public class SyncCreateAllList
{


    private SyncCreateAllListModel alllistModel;
    private String username;
//    private long user_id = alllistModel.getUserModelServer().getId();

    public SyncCreateAllList(SyncCreateAllListModel alllistModel)
    {
       this.alllistModel = alllistModel;
       username = alllistModel.getUserModelServer().getUsername();
    }


    public void createAllListBEGINPARTIES() {

        long user_id = alllistModel.getUserModelServer().getId();
        alllistModel.setClientListFileInfo(null);
        setNewVersionBases(user_id);
        alllistModel.getUpdateSql().UpdateSqlClearBases(alllistModel.getUserModelServer());
        alllistModel.getMysqlServiceFileList().deleteFileListTempAllUserId(user_id);
        alllistModel.getTrainingSendMessage().getListSynCreateAllList(alllistModel);

        viewConsole("SYNCCREATEALLLIST Запрос обработан отправка клиенту " ,  alllistModel.getUserRequestParties());

    }

    public void createAllListPARTIES(long user_id) {
       // alllistModel.getSupportWareHouse().updateItemWarehouseCREATEALLLISTPARTIES("ADD" , alllistModel.getWarehouse() , alllistModel.getReceivedConnectionID());

        int objectIndex = alllistModel.getMyPriorityQueue().size() + 1;
        alllistModel.getMyPriorityQueue().add(objectIndex);

        updateDataBases(alllistModel.getClientListFileInfo() , user_id);
        sendClient(alllistModel);

        viewConsole("SYNCCREATEALLLIST Запрос обработан отправка клиенту " ,  alllistModel.getUserRequestParties());
        alllistModel.getMyPriorityQueue().remove(objectIndex);

       // alllistModel.getSupportWareHouse().updateItemWarehouseCREATEALLLISTPARTIES("REMOVE" , alllistModel.getWarehouse() , alllistModel.getReceivedConnectionID());
    }





    public void createALllListENDPARTIES(long user_id)
    {

        //Обновляет всю базу целиком и отправляет запрос на обновление всех клиентов
        //после обновления своей базы
        alllistModel.getUpdateSql().UpdateSqlAllCreateAllList(alllistModel.getClientListFileInfo() , getNewVersionBases(user_id));

        while (true)
        {
            try
            {
                Thread.sleep(50);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            //если последний update отправлен
            if(alllistModel.getMyPriorityQueue().size() == 0 )
            {
                //alllistModel.getTrainingSendMessage().getListSynCreateAllList(alllistModel);
                sendClient( alllistModel);
                break;
            }
        }


      //  System.out.println("Запрос обработан отправка клиенту  ENDPARTIES "+alllistModel.getUserRequestParties() + "username: " + username + "connectId " + alllistModel.getReceivedConnectionID());
    }



    private void viewConsole(String message , String request)
    {
        System.out.println(message+"  "+request);
    }


    private void sendClient(SyncCreateAllListModel alllistModel)
    {
        alllistModel.getTrainingSendMessage().getListSynCreateAllList(alllistModel);
    }
    private void updateDataBases(FileInfoModel[] clientFileInfo , long user_id)
    {
        alllistModel.getUpdateSql().UpdateSqlAllCreateAllList(clientFileInfo , getNewVersionBases(user_id));
    }

    public long getNewVersionBases(long user_id)
    {
        return staticVariableBases.getLastVersionBases(user_id);

    }

    public void setNewVersionBases(long user_id)
    {
        staticVariableBases.setLastVersionBases(user_id , alllistModel.getMysqlServiceFileList().getUserMaxVersionBases(alllistModel.getUserModelServer().getId()) +1 );
    }




}
