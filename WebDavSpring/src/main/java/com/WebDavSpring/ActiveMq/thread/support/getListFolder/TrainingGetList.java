package com.WebDavSpring.ActiveMq.thread.support.getListFolder;


import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.model.wokingModel.SyncModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceAuthUser;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceClientInfo;

/**
 * Created by А д м и н on 06.05.2020.
 */
public class TrainingGetList {

    private SyncModel syncModel;
    private String[] receivedConnectionIDClone;
    private UsersModel userModelServer;


    public TrainingGetList(SyncModel syncModel , UsersModel userModelServer , String[] receivedConnectionID ) {

        this.syncModel = syncModel;
        this.receivedConnectionIDClone = getReceivedConnectionID( receivedConnectionID);
        this.userModelServer = userModelServer;


    }

    public void startGetList(FileInfoObjectTopic modelObjectClient , String username , long clientVersionBases , ServiceAuthUser serviceAuthUser)
    {
        GetList list = new GetList(syncModel.getServiceClientInfo() , syncModel.getMysqlServiceFileList() , syncModel.getMessage() , modelObjectClient , username ,  clientVersionBases , receivedConnectionIDClone , syncModel.getServiceConnected());
        list.updateClientInfo(modelObjectClient.getTextmessage() , username , syncModel.getJavaMail());
        long clientVersionRows = getVersionRowsClient(modelObjectClient.getTextmessage());
        list.getListReleases(userModelServer , serviceAuthUser , clientVersionRows);
    }

    private long getVersionRowsClient(String[] clientParam)
    {
       // String[] clientParam = modelObjectClient.getTextmessage();
        return  Long.parseLong(clientParam[7]);
    }




    private String[] getReceivedConnectionID(String[] receivedConnectionID)
    {
        return receivedConnectionID.clone();
    }


}
