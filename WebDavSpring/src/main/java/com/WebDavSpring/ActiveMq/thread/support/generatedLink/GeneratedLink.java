package com.WebDavSpring.ActiveMq.thread.support.generatedLink;

import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.WebServerLinkInfoModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceListFiles;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceWebServerLinkInfo;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;

/**
 * Created by А д м и н on 27.08.2020.
 */
public class GeneratedLink {

    private ServiceWebServerLinkInfo serviceWebServerLinkInfo;
    private FileInfoObjectTopic modelObjectClient;
    private SendMessage message;
    private ServiceListFiles serviceListFiles;

    public GeneratedLink(ServiceWebServerLinkInfo serviceWebServerLinkInfo , SendMessage message , ServiceListFiles serviceListFiles)
    {
           this.serviceWebServerLinkInfo = serviceWebServerLinkInfo;
           this.message = message;
           this.serviceListFiles = serviceListFiles;
    }

    public void start(String username  , long user_id ,long listFilesRow_id , String[] connectionID , int webdavport)
    {
        FileInfoModel fim = serviceListFiles.findUserByRow_id(listFilesRow_id , user_id);
        String getLink = "error";

        if(fim != null)
        {
            WebServerLinkInfoModel model = createModel(fim ,  username , webdavport);
            serviceWebServerLinkInfo.deleteSinglToken(listFilesRow_id , user_id );
            serviceWebServerLinkInfo.saveFiles(model);
            getLink = staticVariable.HTTPSERVERURL+model.getToken();
        }



        modelObjectClient = new FileInfoObjectTopic();
        String parties = staticVariable.ANSWERGENLINK;
        String destinationName = "User."+username;
        FileInfoObjectTopic fiot =  createObjectClient(modelObjectClient , user_id , getLink , parties , connectionID);
        message.sendAnswerGenLink(fiot , destinationName);
    }

    public FileInfoObjectTopic createObjectClient(FileInfoObjectTopic modelObjectClient , long user_id , String genLink , String parties , String[] connectionID)
    {

        String[] textMessage = new String[]{"", parties, "", String.valueOf(user_id), parties , genLink };

        modelObjectClient.setConnectionId(connectionID);
        modelObjectClient.setTextmessage(textMessage);

        return modelObjectClient;
    }



    private WebServerLinkInfoModel createModel(FileInfoModel fim , String username , int webdavPortserver)
    {
        WebServerLinkInfoModel model = new WebServerLinkInfoModel();
        model.setType(fim.getType());
        model.setListFilesId(fim.getRow_id());
        model.setToken(generateRandomString(12));
        model.setFileName(fim.getFilename());
        model.setFilesystemlink(convertHrefToFileSystemPath(fim.getLocation() , webdavPortserver));
        model.setUsername(username);
        model.setUser_id(fim.getUser_id());

        return model;
    }


    private String convertHrefToFileSystemPath(String href , int webdavPortserver)
    {
        String decodeHref = decode(href);
        String prefix = getPrefix( decodeHref ,  webdavPortserver);
        String tempPath = injectFileSystemRootFolder( prefix ,  decodeHref);
        String finalPath = detectedSlash(staticVariable.hrefToLocationRootFolder ,  tempPath);
        return finalPath;
    }

    private String injectFileSystemRootFolder(String prefix , String href)
    {
        return href.replace(prefix , staticVariable.hrefToLocationRootFolder);
    }
    private String getPrefix(String href , int webdavPortserver)
    {
        String strPort = String.valueOf(webdavPortserver);
        int begin = 0;
        //+1 это начало новой строки обычно начинается "/" его мы удаляем, что-бы не мешали
        //lenght - размер 9091 что-бы мы удалил и порт тоже(localhost:9091/ - удаляем)
        int end = href.lastIndexOf(strPort ) + strPort.length() + 1;
        String prefix = href.substring(begin , end);
        return prefix;
    }

    private String decode(String value) {
        try {
            return URLDecoder.decode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "error";
        }
    }

    private String  detectedSlash(String rootPath , String tempPath)
    {
        int searh = rootPath.indexOf("\\");

        if(searh != -1)
        {
            return tempPath.replaceAll("/" , Matcher.quoteReplacement("\\"));
        }
        else
        {
            return tempPath.replaceAll(Matcher.quoteReplacement("\\") , "/");
        }
    }

    //пока не использую слишком длинный ключ
    private String generatedToken(String location)
    {
        Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

        String jws = Jwts.builder().setSubject(location).signWith(key).compact();
        String token = jws.substring(jws.length() - 10 , jws.length());
        return  token;
    }

    private  Random random = new Random((new Date()).getTime());

    public  String generateRandomString(int length) {
        char[] values = {'a','b','c','d','e','f','g','h','i','j',
                'k','l','m','n','o','p','q','r','s','t',
                'u','v','w','x','y','z','0','1','2','3',
                '4','5','6','7','8','9'};

        String out = "";

        for (int i=0;i<length;i++) {
            int idx=random.nextInt(values.length);
            out += values[idx];
        }
        return out;
    }
}
