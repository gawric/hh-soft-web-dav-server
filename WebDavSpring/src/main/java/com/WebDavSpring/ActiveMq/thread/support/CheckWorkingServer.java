package com.WebDavSpring.ActiveMq.thread.support;

import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.ActiveMq.usersconnected.UserProperties;
import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.ConnectedModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUsersConnected;

/**
 * Created by А д м и н on 20.04.2020.
 */


public class CheckWorkingServer {



    private FileInfoObjectTopic modelObjectClient;
    private SendMessage message;
    private ServiceUsersConnected serviceConnected;
    private UsersModel userModelServer;
    private UserProperties userProperties;

    public CheckWorkingServer(ServiceUsersConnected serviceConnected ,  UsersModel userModelServer , FileInfoObjectTopic modelObjectClient , SendMessage message , UserProperties userProperties) {
        this.serviceConnected = serviceConnected;
        this.userModelServer = userModelServer;
        this.modelObjectClient = modelObjectClient;
        this.message = message;
        this.userProperties = userProperties;

    }

    public void sendStatusPublisher(int userId , String[] connectionId)
    {
        //было sessionId
        ConnectedModel model = serviceConnected.findByClientid(connectionId[0]);
        boolean isWorking = userProperties.isWorkingUser(userId);
        modelObjectClient.setConnectionId(connectionId);
        String parties = staticVariable.CHECKWORKINGSERVER;

        if(model == null)
            model = new ConnectedModel();

        getListSynSingList( userModelServer ,  parties , String.valueOf(model.isPublisher()) , connectionId , String.valueOf(isWorking));
    }

    //5 - connectionId не используется теперь пересылается в отдельном виде
    private void getListSynSingList(UsersModel userModelServer , String parties , String isPublisher , String[] connectionId , String isWorking)
    {

        String username = userModelServer.getUsername();
        modelObjectClient.setConnectionId(connectionId);
        String[] textMessage = {"" , parties , "", String.valueOf(userModelServer.getId()), parties  , "" , isPublisher , isWorking};
        modelObjectClient.setTextmessage(textMessage);
        message.sendFileInfoObject(modelObjectClient , "User."+username);
        System.out.println("***** CheckWorkingServer->isWorking : Отправка запроса  для "+userModelServer.getUsername()+"  "+connectionId+"  >>> Отправляем занят сервер или нет?  "+parties+"  ***** Ответ:  " +isWorking);
    }
}
