package com.WebDavSpring.ActiveMq.queue;

import com.WebDavSpring.ActiveMq.queue.comparator.ComparatorQueueRunnableClient;
import com.WebDavSpring.ActiveMq.queue.comparator.ComparetorQueueRunnalbe;
import com.WebDavSpring.WebDavSpring.model.PriorityQueueRunnableClientModel;
import com.WebDavSpring.WebDavSpring.model.PriorityQueueRunnableServerModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created by А д м и н on 26.05.2020.
 */
@Component
public class QueueRunnablePriorityWorkerToClient {

    @Autowired
    private ThreadPoolTaskExecutor task;

    @Autowired
    QueueRunnablePriorityWorkerToServer queuServer;

    @Autowired
    private JmsTemplate template;

    private PriorityBlockingQueue<PriorityQueueRunnableClientModel> clientPriorityQueue;

    private Boolean isStopServiceQueue = false;
    private Boolean isRunning = false;

    @PostConstruct
    public void init() {
        startQueueRunnable();
    }

    public void startQueueRunnable() {

        createQueue(clientPriorityQueue);

        if(!isRunning)
            task.execute( pollDataFromQueue());

    }

    private void createQueue(PriorityBlockingQueue<PriorityQueueRunnableClientModel> integerPriorityQueue){
        if (integerPriorityQueue == null) {
            this.clientPriorityQueue = new PriorityBlockingQueue<>(100, new ComparatorQueueRunnableClient());
        }

    }

    public synchronized void addDataToQueue(PriorityQueueRunnableClientModel modelQuequ)
    {
        if(modelQuequ != null)
        {
            clientPriorityQueue.add(modelQuequ);
        }
        else
        {
            System.out.println("QueueRunnablePriorityWorkerToClient->addDataToQueue: PriorityQueueRunnableServerModel оказалось пустой!");
        }

    }

    //служебный метод для обработки данных очереди
    public Runnable pollDataFromQueue(){
        return () -> startPollData();
    }

    private void waitThread()
    {
        try
        {
            Thread.sleep(100);

        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    private void startPollData()
    {
        isRunning = true;

        while(true)
        {


            if(isStopServiceQueue)
            {
                isRunning = false;
                isStopServiceQueue = false;
                break;
            }


            waitThread();

            PriorityQueueRunnableClientModel request = null;

            try
            {
                request = clientPriorityQueue.take();
                System.out.println("Размер очереди на сервере "+queuServer.sizeQueu());
            }
            catch (InterruptedException e)
            {
                System.out.println("QueueRunnablePriorityWorkerToClient: startPollData " + "Ошибка: \n");
                e.printStackTrace();
            }

            runningRequest(request);


        }
    }

    private void runningRequest(PriorityQueueRunnableClientModel request)
    {
        if(request != null)
        {

            System.out.println("Отправка из QueueRunnableClient: Запрос-> "+request.getParties() +" приоритет запроса "+ request.getId() );
            template.convertAndSend(request.getDestinationName() , request.getInfoObjectTopic());

        }
    }

}
