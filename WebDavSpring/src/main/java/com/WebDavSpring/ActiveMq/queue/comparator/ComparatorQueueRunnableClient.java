package com.WebDavSpring.ActiveMq.queue.comparator;

import com.WebDavSpring.WebDavSpring.model.PriorityQueueRunnableClientModel;
import com.WebDavSpring.WebDavSpring.model.PriorityQueueRunnableServerModel;

import java.util.Comparator;

/**
 * Created by А д м и н on 26.05.2020.
 */
public class ComparatorQueueRunnableClient implements Comparator<PriorityQueueRunnableClientModel> {
    @Override
    public int compare(PriorityQueueRunnableClientModel o1, PriorityQueueRunnableClientModel o2) {
        return o1.getId() - o2.getId();
    }
}