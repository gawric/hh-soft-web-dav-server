//package com.WebDavSpring.ActiveMq.queue.poll;
//
//import com.WebDavSpring.ActiveMq.thread.ListenerRunnable;
//import com.WebDavSpring.ActiveMq.thread.ListenerRunnableLazy;
//import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.TrainingSendMessage;
//import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.support.SupportWareHouse;
//import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
//import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
//import com.WebDavSpring.WebDavSpring.model.PriorityQueueRunnableServerModel;
//import com.WebDavSpring.WebDavSpring.model.lazyModel.LazyModel;
//import com.WebDavSpring.WebDavSpring.model.wokingModel.SyncModel;
//
///**
// * Created by А д м и н on 18.05.2020.
// */
//public class PollQueuNewTask implements Runnable {
//
//
//    @Override
//    public void run()
//    {
//        while(true)
//        {
//            try
//            {
//                Thread.sleep(100);
//
//            } catch (InterruptedException e)
//            {
//                e.printStackTrace();
//            }
//            PriorityQueueRunnableServerModel request = integerPriorityQueue.poll();
//            if(request == null) break;
//
//            if(request.getParties().indexOf(staticVariable.LAZY) != -1){
//                updateLazyUser(request.getInfoObjectTopic());
//            }
//            else {
//                updateUser(request.getInfoObjectTopic());
//            }
//
//        }
//    }
//
//
//    private void updateUser(FileInfoObjectTopic object){
//        SyncModel syncModel = new SyncModel();
//        syncModel.setTask(task);
//        syncModel.setUpdateSql(updateSql);
//        syncModel.setMessage(messageJson);
//        syncModel.setMysqlServiceFileList(mysqlServiceFileList);
//        syncModel.setMyPriorityQueue(myPriorityQueue);
//        syncModel.setMyPriorityDeleteQueue(myPriorityDeleteQueue);
//        syncModel.setMyPriorityPasteQueue(myPriorityPasteQueue);
//        syncModel.setMyPrioritySinglUpdateQueue(myPrioritySinglUpdateQueue);
//        syncModel.setServiceConnected(serviceConnected);
//        syncModel.setWarehouse(wareHouse);
//        syncModel.setUserProperties(userProperties);
//        syncModel.setSupportWareHouse(new SupportWareHouse());
//        syncModel.setTrainingSendMessage(new TrainingSendMessage());
//        //отправляем в новый обьект
//        //каждую итерацию нужно создавать новый обьект
//        //что-бы в разных потоках обрабатывать запросы
//        ListenerRunnable workingclass = new ListenerRunnable(syncModel);
//        workingclass.setModelObjectClient(object);
//
//        task.execute(workingclass);
//    }
//
//    private void updateLazyUser(FileInfoObjectTopic object)
//    {
//
//        LazyModel lazyModel = createModel();
//
//        //отправляем в новый обьект
//        //каждую итерацию нужно создавать новый обьект
//        //что-бы в разных потоках обрабатывать запросы
//        ListenerRunnableLazy workingclass = new ListenerRunnableLazy(lazyModel);
//        workingclass.setModelObjectClient(object);
//
//        task.execute(workingclass);
//    }
//
//
//    private LazyModel createModel()
//    {
//        LazyModel lazyModel = new LazyModel();
//        lazyModel.setUpdateSql(updateSql);
//        lazyModel.setMessage(messageJson);
//        lazyModel.setMysqlServiceFileList(mysqlServiceFileList);
//        lazyModel.setMyPrioritySinglUpdateQueue(myPrioritySinglUpdateQueue);
//        lazyModel.setServiceConnected(serviceConnected);
//        lazyModel.setWarehouse(wareHouse);
//        lazyModel.setUserProperties(userProperties);
//        lazyModel.setTask(task);
//
//        return lazyModel;
//    }
//}
