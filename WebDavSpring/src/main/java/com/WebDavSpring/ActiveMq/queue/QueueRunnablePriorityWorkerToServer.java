package com.WebDavSpring.ActiveMq.queue;

import com.WebDavSpring.ActiveMq.UpdateSqlCollectionUsers;
import com.WebDavSpring.ActiveMq.queue.comparator.ComparetorQueueRunnalbe;
import com.WebDavSpring.ActiveMq.thread.ListenerRunnable;
import com.WebDavSpring.ActiveMq.thread.ListenerRunnableLazy;
import com.WebDavSpring.ActiveMq.thread.support.lazySinglUpdate.support.LazyWarehouse;
import com.WebDavSpring.ActiveMq.usersconnected.UserProperties;
import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.TrainingSendMessage;

import com.WebDavSpring.MailSender.JavaMail;
import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.PriorityQueueRunnableServerModel;
import com.WebDavSpring.WebDavSpring.model.lazyModel.LazyModel;
import com.WebDavSpring.WebDavSpring.model.wokingModel.SyncModel;
import com.WebDavSpring.WebDavSpring.services.Interface.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.PriorityQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created by А д м и н on 18.05.2020.
 */
//Очередь с приоритетом что-бы выполнять приоритетные задачи от клиент
@Component
public class QueueRunnablePriorityWorkerToServer {

    @Autowired
    private ThreadPoolTaskExecutor task;

    private PriorityQueue<Integer> myPriorityQueue = new PriorityQueue<Integer>();
    private PriorityQueue<Integer> myPriorityDeleteQueue = new PriorityQueue<Integer>();
    private PriorityQueue<Integer> myPriorityPasteQueue = new PriorityQueue<Integer>();
    private PriorityQueue<Integer> myPrioritySinglUpdateQueue = new PriorityQueue<Integer>();


    @Autowired
    private UpdateSqlCollectionUsers updateSql;

    @Autowired
    private SendMessage messageJson;

    @Autowired
    private ServiceListFiles mysqlServiceFileList;

    @Autowired
    private ServiceUsersConnected serviceConnected;

    @Autowired
    private UserProperties userProperties;

    @Autowired
    private ServiceErrorAlert serviceErrorAlert;

    @Autowired
    private JavaMail javaMail;

    @Autowired
    private ServiceClientInfo serviceClientInfo;

    @Autowired
    private ServiceWebServerLinkInfo serviceWebServerLinkInfo;

    @Autowired
    private ServiceAuthUser serviceAuthUser;

    @Autowired
    LazyWarehouse lazyWarehouse;

    @Value("${server.port}")
    int port;

    private PriorityBlockingQueue<PriorityQueueRunnableServerModel> integerPriorityQueue;

    private Boolean isStopServiceQueue = false;
    private Boolean isRunning = false;

    public void startQueueRunnable() {

        createQueue(integerPriorityQueue);

        if(!isRunning)
            task.execute( pollDataFromQueue());

    }

    private void createQueue(PriorityBlockingQueue<PriorityQueueRunnableServerModel> integerPriorityQueue){
        if (integerPriorityQueue == null) {
            this.integerPriorityQueue = new PriorityBlockingQueue<>(100, new ComparetorQueueRunnalbe());
        }

    }

    public synchronized void addDataToQueue(PriorityQueueRunnableServerModel modelQuequ){
        if(modelQuequ != null)
        {
            integerPriorityQueue.add(modelQuequ);
        }
        else
        {
            System.out.println("QueueRunnablePriority->addDataToQueue: PriorityQueueRunnableServerModel оказалось пустой!");
        }

    }

    public void setStopService(boolean isStop)
    {
        isStopServiceQueue = isStop;
    }

    public int sizeQueu()
    {
        return integerPriorityQueue.size();
    }

    public boolean isWorking(int id)
    {
        boolean check = false;

        Optional<PriorityQueueRunnableServerModel> result =
                integerPriorityQueue.stream().filter(item -> item.getId() == id).findFirst();

        if(result.isPresent())
            check = true;

        return check;
    }
    //служебный метод для обработки данных очереди
    public Runnable pollDataFromQueue(){
        return () -> startPollData();
    }

    private void startPollData()
    {
        isRunning = true;

        while(true)
        {


            if(isStopServiceQueue)
            {
                isRunning = false;
                isStopServiceQueue = false;
                break;
            }


            waitThread();

            PriorityQueueRunnableServerModel request = null;
            try
            {
                request = integerPriorityQueue.take();
            }
            catch (InterruptedException e)
            {
                System.out.println("QueueRunnablePriority: startPollData " + "Ошибка: \n");
                e.printStackTrace();
            }
            runningRequest(request);


        }
    }

    private void runningRequest(PriorityQueueRunnableServerModel request)
    {
        if(request != null)
        {
            if(request.getParties().indexOf(staticVariable.LAZY) != -1)
            {
                updateLazyUser(request.getInfoObjectTopic() , lazyWarehouse);
            }
            else
            {
               // System.out.println("***********Порядок отправки запроса КЛИЕНТУ>>> request "+request.getParties());
                updateUser(request.getInfoObjectTopic());
            }

        }
    }

    private void waitThread()
    {
        try
        {
            Thread.sleep(50);

        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    private void updateUser(FileInfoObjectTopic object){

        SyncModel syncModel = new SyncModel();

        syncModel.setTask(task);
        syncModel.setUpdateSql(updateSql);
        syncModel.setMessage(messageJson);
        syncModel.setMysqlServiceFileList(mysqlServiceFileList);
        syncModel.setMyPriorityQueue(myPriorityQueue);
        syncModel.setMyPriorityDeleteQueue(myPriorityDeleteQueue);
        syncModel.setMyPriorityPasteQueue(myPriorityPasteQueue);
        syncModel.setMyPrioritySinglUpdateQueue(myPrioritySinglUpdateQueue);
        syncModel.setServiceConnected(serviceConnected);
        syncModel.setUserProperties(userProperties);
        syncModel.setTrainingSendMessage(new TrainingSendMessage());
        syncModel.setServiceErrorAlert(serviceErrorAlert);
        syncModel.setJavaMail(javaMail);
        syncModel.setServiceClientInfo(serviceClientInfo);
        syncModel.setServiceWebServerLinkInfo(serviceWebServerLinkInfo);
        syncModel.setWebDavServerPort(port);
        syncModel.setServiceAuthUser(serviceAuthUser);
        //отправляем в новый обьект
        //каждую итерацию нужно создавать новый обьект
        //что-бы в разных потоках обрабатывать запросы
        ListenerRunnable workingclass = new ListenerRunnable(syncModel);
        workingclass.setModelObjectClient(object);

        task.execute(workingclass);
    }

    private void updateLazyUser(FileInfoObjectTopic object , LazyWarehouse lazyWarehouse)
    {

        LazyModel lazyModel = createModel();

        //отправляем в новый обьект
        //каждую итерацию нужно создавать новый обьект
        //что-бы в разных потоках обрабатывать запросы
        ListenerRunnableLazy workingclass = new ListenerRunnableLazy(lazyModel , lazyWarehouse);
        workingclass.setModelObjectClient(object);

        task.execute(workingclass);
    }


    private LazyModel createModel()
    {
        LazyModel lazyModel = new LazyModel();
        lazyModel.setUpdateSql(updateSql);
        lazyModel.setMessage(messageJson);
        lazyModel.setMysqlServiceFileList(mysqlServiceFileList);
        lazyModel.setMyPrioritySinglUpdateQueue(myPrioritySinglUpdateQueue);
        lazyModel.setServiceConnected(serviceConnected);
        lazyModel.setUserProperties(userProperties);
        lazyModel.setTask(task);

        return lazyModel;
    }
}
