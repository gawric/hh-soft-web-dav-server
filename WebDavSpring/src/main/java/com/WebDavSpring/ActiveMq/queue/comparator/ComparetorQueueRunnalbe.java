package com.WebDavSpring.ActiveMq.queue.comparator;

import com.WebDavSpring.WebDavSpring.model.PriorityQueueRunnableServerModel;

import java.util.Comparator;

/**
 * Created by А д м и н on 18.05.2020.
 */
public class ComparetorQueueRunnalbe implements Comparator<PriorityQueueRunnableServerModel> {
    @Override
    public int compare(PriorityQueueRunnableServerModel o1, PriorityQueueRunnableServerModel o2) {
        return o1.getId() - o2.getId();
    }
}
