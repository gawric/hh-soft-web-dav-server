package com.WebDavSpring.ActiveMq;


import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.UsersModel;

import com.WebDavSpring.WebDavSpring.services.Interface.ServiceListFiles;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;

/**
 * Created by А д м и н on 19.09.2019.
 */
@Component
public class UpdateSqlCollectionUsers
{

    @Autowired
    private ServiceListFiles repoListFiles;

    @Autowired
    private ServiceUser repoUsers;



    public UsersModel getFindUserIdByUsername(String username)
    {
        return repoUsers.findByUsername(username);
    }

    //Полностью обновляет всю базу данных
    public void UpdateSqlClearBases(UsersModel user)
    {

        repoListFiles.deleteAllFileUserTableInfo(user.getId());
        System.out.println("База данных юзера "+user.getUsername()+" очищена");
    }

    //Обновляет кусок данных
    public void UpdateSqlAllFileListInfo(FileInfoModel[] listAllFileInfo , long newVersionRows)
    {
        //printId(listAllFileInfo);

        repoListFiles.saveListFilesNewVersionBases(listAllFileInfo , newVersionRows);

    }


    public void UpdateSqlAllCreateAllList(FileInfoModel[] listAllFileInfo , long newVersionRows)
    {
        //printId(listAllFileInfo);

        repoListFiles.saveListFilesCreateAllListNewVersionBases(listAllFileInfo , newVersionRows);

    }

    //Обновляет кусок данных
    public void DeleteSqlAllFileListInfo(UsersModel user , FileInfoModel[] listAllFileInfo)
    {
        System.out.println("Удаление файлов кусками " + user.getUsername());
        repoListFiles.saveListFiles(listAllFileInfo);

    }


    public void printId(FileInfoModel[] listAllFileInfo)
    {
        for(int s = 0; s < listAllFileInfo.length; s++)
        {
            if(listAllFileInfo[s] != null)
            {
                System.out.println("=++++++++++++++++++++++++++++++++++++++++++=");
                System.out.println("row_id "+listAllFileInfo[s].getRow_id());
                System.out.println("filename "+listAllFileInfo[s].getFilename());
                System.out.println("user_id " + listAllFileInfo[s].getUser_id());
                System.out.println("=++++++++++++++++++++++++++++++++++++++++++=");
            }
        }
    }

}
