package com.WebDavSpring.ActiveMq;


import com.WebDavSpring.ActiveMq.queue.QueueRunnablePriorityWorkerToServer;
import com.WebDavSpring.ActiveMq.support.EndPointArrayList;
import com.WebDavSpring.ActiveMq.usersconnected.UsersConnected;
import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.PriorityQueueRunnableServerModel;
import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.ConnectedIdRunnableModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.activemq.command.ActiveMQMessage;
import org.apache.activemq.command.ConnectionInfo;
import org.apache.activemq.command.RemoveInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by А д м и н on 20.08.2019.
 */

@Component
public class EndPointListener implements JmsListenerConfigurer
{

    @Autowired
    private EndPointArrayList list;

    @Autowired
    private UsersConnected usersConnected;

    @Autowired
    private SendMessage sendMessage;





    @Autowired
    private QueueRunnablePriorityWorkerToServer queueRunnablePriority;

    //хранит последний сгенерированный ConnectionId (Нужно быть с ним очень осторожным т.к может присвоить неверный ConnectionId клиенту)
    private ConnectedIdRunnableModel runnableModel = new ConnectedIdRunnableModel();
    private JmsListenerEndpointRegistrar jmsListenerEndpointRegistrar;




    public void status(JmsListenerEndpointRegistrar jmsListenerEndpointRegistrar)
    {

        list.getListEndPoint().forEach(number ->
                {
                    //добавляем только уникальные id номера
                    if(jmsListenerEndpointRegistrar.getEndpointRegistry().getListenerContainer(number.getId()) == null)
                    {
                        jmsListenerEndpointRegistrar.registerEndpoint(number);
                    }
                }
        );
    }

    @Override
    public void configureJmsListeners(JmsListenerEndpointRegistrar jmsListenerEndpointRegistrar) {
        this.jmsListenerEndpointRegistrar = jmsListenerEndpointRegistrar;
        status(jmsListenerEndpointRegistrar);
    }

    public void updateEndPoint()
    {
        status(jmsListenerEndpointRegistrar);
    }

    //Реализация слушателя сообщений
    public void updateListener()
    {

        list.getListEndPoint().forEach(number ->
        {
            //если есть listener мы проходим без изменений
            if(number.getMessageListener() == null)
            {


                if(number.getId().equals("Admin-Changes-Users"))
                {
                    //подписываемся всеми точками на endpoint
                    number.setMessageListener(message ->
                    {
                        updateUser((TextMessage) message);
                    });

                }
                else if(number.getId().equals("Lazy-Admin-Changes-Users"))
                {

                    number.setMessageListener(message ->
                            updateLazyUser( (TextMessage) message));
                }
                else if(number.getId().equals("Admin-HttpServer"))
                {

                    number.setMessageListener(message ->
                    {
                        updateHttWebServer((TextMessage) message);
                    });
                }
                else
                {
                    if(number.getId().equals("Admin-Advisory-Users"))
                    {
                        //подписываемся всеми точками на endpoint
                        number.setMessageListener(message ->
                                updateAdmin( message , runnableModel));
                    }

                }

            }

        });
    }



    private void updateAdmin(Message message , ConnectedIdRunnableModel modelRunnable)
    {
        if(message instanceof ActiveMQMessage)
        {
            ActiveMQMessage aMsg = (ActiveMQMessage)message;

            if(aMsg.getDataStructure() instanceof  ConnectionInfo)
            {
                ConnectedEvent(aMsg , modelRunnable);


            }
            else
            {
                DisconnectEvent(aMsg);

            }

        }

    }

    private void ConnectedEvent( ActiveMQMessage aMsg , ConnectedIdRunnableModel modelRunnable) {
        ConnectionInfo  prod = (ConnectionInfo) aMsg.getDataStructure();
        //System.out.println("isConnected "+ prod.getUserName() +" ClientID:   "+prod.getClientId().toString()+" ConnectionId:  "+prod.getConnectionId());
        String convertConnectionId = prod.getConnectionId().toString();
        String convertClientId = prod.getClientId().toString();

        usersConnected.addConnectedToService(prod.getUserName(), convertClientId , convertConnectionId , modelRunnable);
    }




    private void DisconnectEvent(ActiveMQMessage aMsg) {
        RemoveInfo  prod = (RemoveInfo) aMsg.getDataStructure();
        // System.out.println("isDisconnected ObjectRemove "+prod.getObjectId());

        usersConnected.removeSessionID(prod.getObjectId().toString());
    }



    private void updateLazyUser(TextMessage message) {


        FileInfoObjectTopic object = createObject(message);
        System.out.println("//////////////////////////////////////////////////////////////");
        System.out.println("Пришел запрос LazyUserCommit "+object.getTextmessage()[4]);
        System.out.println("//////////////////////////////////////////////////////////////");

        String userRequestParties = getRequest(object);

        PriorityQueueRunnableServerModel queueModel = new PriorityQueueRunnableServerModel(getPriorityRequest(userRequestParties) , userRequestParties , object);
        queueRunnablePriority.startQueueRunnable();
        queueRunnablePriority.addDataToQueue(queueModel);

    }


    private void updateUser(TextMessage message){

        FileInfoObjectTopic object = createObject(message);

        System.out.println("//////////////////////////////////////////////////////////////");
        System.out.println("Пришел запрос UserCommit  "+object.getTextmessage()[4]);
        System.out.println("//////////////////////////////////////////////////////////////");

        String userRequestParties = getRequest(object);

        PriorityQueueRunnableServerModel queueModel = new PriorityQueueRunnableServerModel(getPriorityRequest(userRequestParties) , userRequestParties , object);
        queueRunnablePriority.startQueueRunnable();
        queueRunnablePriority.addDataToQueue(queueModel);

    }


    private void updateHttWebServer(TextMessage message){

        FileInfoObjectTopic object = createObject(message);
        object.setTextmessage(object.getTextmessage());
        System.out.println("//////////////////////////////////////////////////////////////");
        System.out.println("Пришел запрос HttpWebServer  "+object.getTextmessage()[4]);
        System.out.println("//////////////////////////////////////////////////////////////");

        String userRequestParties = getRequest(object);

        PriorityQueueRunnableServerModel queueModel = new PriorityQueueRunnableServerModel(getPriorityRequest(userRequestParties) , userRequestParties , object);
        queueRunnablePriority.startQueueRunnable();
        queueRunnablePriority.addDataToQueue(queueModel);

    }


    private FileInfoObjectTopic createObject(TextMessage message)
    {
        String json = null;
        FileInfoObjectTopic object = null;

        return convertJsonToObjectClient(json,object,message);
    }

    private String getRequest(FileInfoObjectTopic object)
    {
        String userRequestParties = object.getTextmessage()[4];
        String userRequest = object.getTextmessage()[1];

        if(!userRequestParties.equals(staticVariable.NO))
        {
            return userRequestParties;
        }
        else
        {
            return userRequest;
        }



    }

    private int getPriorityRequest(String parties)
    {

        ConcurrentHashMap<String , Integer> allRequest = staticVariable.getAllRequestPriority();

        //если запрос не известен то мы даем самый низкий приоритет
        if(allRequest.containsKey(parties))
        {
            return allRequest.get(parties);
        }
        else
        {
            return 9;
        }
    }





    private FileInfoObjectTopic  convertJsonToObjectClient( String json , FileInfoObjectTopic object , TextMessage message)
    {
        TextMessage textMessage =  message;
        try
        {
            json = textMessage.getText();
        }
        catch (JMSException e)
        {
            e.printStackTrace();
        }
        ObjectMapper objectMapper = new ObjectMapper();

        try
        {
            object = objectMapper.readValue(json, FileInfoObjectTopic.class);

        } catch (IOException e)
        {
            e.printStackTrace();
        }

        return object;

    }


}
