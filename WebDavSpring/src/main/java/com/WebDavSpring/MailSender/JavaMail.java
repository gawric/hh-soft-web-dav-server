package com.WebDavSpring.MailSender;

import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.MailModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * Created by А д м и н on 09.07.2020.
 */
@Component
public class JavaMail {

    @Autowired
    public JavaMailSender emailSender;

    public MailModel sendMail(MailModel model)
    {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setFrom(staticVariable.FROMMAIL);
        message.setTo(staticVariable.TOMAIL);
        message.setSubject(model.getSubject());

        message.setText("Отправитель: \n"+"Имя: "+model.getNamequest()+"\nEmail: "+model.getEmailguest()+"\nТелефон: "+model.getPhonequest()+"  \n \n"+"Текст Письма:   "+model.getText());


        try
        {
            emailSender.send(message);
            model.setStatus("OK");
            LocalDateTime time = LocalDateTime.now();
            System.out.println("Письмо отправленно: " + model.getSubject() +" юзером: "+ model.getNamequest() +" время: "+time);
            return model;
        }
        catch(MailException ex)
        {
            LocalDateTime time = LocalDateTime.now();
            System.out.println("Ошибка отправки письма: " + model.getSubject() +" юзером: "+ model.getNamequest() +" время: "+time);
            model.setStatus(ex.getMessage().toString());
            return model;
        }
    }
}
