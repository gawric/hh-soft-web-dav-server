package com.WebDavSpring.WebDavSpring.configuration.variable;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by А д м и н on 19.10.2020.
 */
public class staticVariableBases {

    private static ConcurrentHashMap<Long, Long> lastVersionBases;
    private static ConcurrentHashMap<Long, Long> lastVersionRows;


    public static void  setLastVersionBases(long user_id , long version)
    {
        if(lastVersionBases == null)
        {
            lastVersionBases = new ConcurrentHashMap<>();
            lastVersionBases.put(user_id , 0l);
        }
        else
        {
            setBases(user_id ,  version);
        }
    }

    public static void  setLastVersionRows(long user_id , long version)
    {
        if(lastVersionRows == null)
        {
            lastVersionRows = new ConcurrentHashMap<>();
            lastVersionRows.put(user_id , 0l);
        }
        else
        {
            setRows(user_id , version);
        }
    }


    public static long getLastVersionBases(long user_id)
    {
        if(lastVersionBases == null) {
            lastVersionBases = new ConcurrentHashMap<>();
            lastVersionBases.put(user_id , 0l);
            return 0;
        }
        else
        {
            if(lastVersionBases.containsKey(user_id))
            {
                return lastVersionBases.get(user_id);
            }
            else
            {
                lastVersionBases.put(user_id , 0l);
                return lastVersionBases.get(user_id);
            }

        }

    }



    public static long getLastVersionRows(long user_id)
    {
        if(lastVersionRows == null)
        {
            lastVersionRows = new ConcurrentHashMap<>();
            lastVersionRows.put(user_id , 0l);
            return 0;
        }
        else
        {
            if(lastVersionRows.containsKey(user_id))
            {
                return lastVersionRows.get(user_id);
            }
            else
            {
                lastVersionRows.put(user_id , 0l);
                return lastVersionRows.get(user_id);
            }


        }
    }


    private static void  setBases(long user_id , long version)
    {
        if(lastVersionBases.containsKey(user_id))
        {
            lastVersionBases.replace(user_id , version);
        }
        else
        {
            lastVersionBases.put(user_id , version);
        }
    }

    private static void  setRows(long user_id , long version)
    {
        if(lastVersionRows.containsKey(user_id))
        {
            lastVersionRows.replace(user_id , version);
        }
        else
        {
            lastVersionRows.put(user_id , version);
        }
    }
}
