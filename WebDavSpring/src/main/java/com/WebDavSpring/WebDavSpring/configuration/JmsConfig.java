package com.WebDavSpring.WebDavSpring.configuration;

import org.apache.activemq.ActiveMQSslConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;


import javax.jms.ConnectionFactory;


/**
 * Created by А д м и н on 19.08.2019.
// */
@EnableJms
@Configuration
public class JmsConfig  {

    @Value("${amqclient.ssl.key}")
    private String key;

    String BROKER_URL = "ssl://hh-soft.ru:61714";
    String BROKER_USERNAME = "admin";
    String BROKER_PASSWORD = "cthutq1986";


    @Bean
    public ConnectionFactory clientJMSConnectionFactory() {



            final ActiveMQSslConnectionFactory theSSLConnectionFactory =
                    new ActiveMQSslConnectionFactory(BROKER_URL);
            try
            {
                theSSLConnectionFactory.setUserName(BROKER_USERNAME);
                theSSLConnectionFactory.setPassword(BROKER_PASSWORD);





                theSSLConnectionFactory.setTrustStore(key);
                theSSLConnectionFactory.setTrustStorePassword("cthutq1986");
                //theSSLConnectionFactory.setTrustStoreType("PKCS12");//<-- зависимость другая
                //theSSLConnectionFactory.setKeyStore("file:///D:/java/apache-activemq-5.15.9-bin/cetrification/client/amq-client.ks");
                //theSSLConnectionFactory.setKeyStorePassword("cthutq1986");

                theSSLConnectionFactory.setUseAsyncSend(true);
                theSSLConnectionFactory.setAlwaysSessionAsync(true);

                theSSLConnectionFactory.setWatchTopicAdvisories(true);




            } catch (final Exception theException) {
                throw new Error(theException);
            }


        return theSSLConnectionFactory;
    }


    @Bean // Serialize message content to json using TextMessage
    public MessageConverter jacksonJmsMessageConverter()
    {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");

        return converter;
    }


    @Bean
    public JmsTemplate jmsTemplate()
    {
        JmsTemplate template = new JmsTemplate();
        template.setPubSubDomain(true);
        template.setMessageConverter(jacksonJmsMessageConverter());
        template.setConnectionFactory(clientJMSConnectionFactory());
        template.setPubSubDomain(true);

        return template;
    }

    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory()
    {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();

        factory.setConnectionFactory(clientJMSConnectionFactory());
        factory.setConcurrency("1-1");
        factory.setPubSubDomain(true);
        factory.setMessageConverter(jacksonJmsMessageConverter());

        return factory;
    }




//    @Bean
//    public DynamicDestinationResolver getDynamicDestinationResolver()
//    {
//        DynamicDestinationResolver resolver = new DynamicDestinationResolver();
//        resolver.resolveDestinationName();
//        return resolver;
//
//    }
//
//
//    @Override
//    public void configureJmsListeners(JmsListenerEndpointRegistrar registrar) {
//
//
//
//        registrar.registerEndpoint(endpoint);
//    }
}
