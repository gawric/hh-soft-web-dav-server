package com.WebDavSpring.WebDavSpring.configuration.variable;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by А д м и н on 01.04.2020.
 */
public class staticVariable {

    public static final String userLocatorWebDav = "userwebdav";
    public static final String adminLocatorWebDav = "adminwebdav";
    static ConcurrentHashMap<String , Integer> hashMap;



    public static final String NO = "no";
    public static final String LAZY = "LAZY";
    public static final String GETLIST = "GETLIST";
    public static final String SYNCREATEALLLIST = "SYNCREATEALLLIST";
    public static final String SEARCH = "SEARCH";
    public static final String SYNSINGLLIST = "SYNSINGLLIST";

    public static final String DELETEBEGINPARTIES = "DELETEBEGINPARTIES";
    public static final String DELETEPARTIES = "DELETEPARTIES";
    public static final String DELETEENDPARTIES = "DELETEENDPARTIES";

    public static final String PASTEBEGINPARTIES = "PASTEBEGINPARTIES";
    public static final String PASTEPARTIES = "PASTEPARTIES";
    public static final String PASTEENDPARTIES = "PASTEENDPARTIES";

    public static final String SINGLBEGINUPDATEPARTIES = "SINGLBEGINUPDATEPARTIES";
    public static final String SINGLUPDATEPARTIES = "SINGLUPDATEPARTIES";
    public static final String SINGLENDUPDATEPARTIES = "SINGLENDUPDATEPARTIES";

    public static final String RENAMESINGLBEGINUPDATEPARTIES = "RENAMESINGLBEGINUPDATEPARTIES";
    public static final String RENAMESINGLUPDATEPARTIES = "RENAMESINGLUPDATEPARTIES";
    public static final String RENAMESINGLENDUPDATEPARTIES = "RENAMESINGLENDUPDATEPARTIES";

    public static final String BEGINPARTIES = "BEGINPARTIES";
    public static final String PARTIES = "PARTIES";
    public static final String ENDPARTIES = "ENDPARTIES";

    public static final String SEARCHBEGINFILES = "SEARCHBEGINFILES";
    public static final String SEARCHENDFILES = "SEARCHENDFILES";
    public static final String SEARCHPARTIESFILES = "SEARCHPARTIESFILES";

    public static final String STATUSPUBLISHER = "STATUSPUBLISHER";
    public static final String ALERTERROR = "ALERTERROR";

    public static final String WEBSERVERREQUEST = "WEBSERVERREQUEST";
    public static final String ANSWERWEBDAVSERVER = "Answerwebdavserver";
    public static final String REQUESTGENLINK = "REQUESTGENLINK";
    public static final String ANSWERGENLINK = "ANSWERGENLINK";

    public static final String LAZYSINGLBEGINUPDATEPARTIES = "LAZYSINGLBEGINUPDATEPARTIES";
    public static final String LAZYSINGLUPDATEPARTIES = "LAZYSINGLUPDATEPARTIES";
    public static final String LAZYSINGLENDUPDATEPARTIES = "LAZYSINGLENDUPDATEPARTIES";

    public static final String CHECKWORKINGSERVER = "CHECKWORKINGSERVER";



    public static final String  hrefToLocationRootFolder = "/media/hdd_tomcat/disk/";
    //public static final String  hrefToLocationRootFolder = "D:\\XiaoMi\\XiaoMiFlash\\";



    //приоритет запросам 1 самый наивысший 9 самый низкий
    public static ConcurrentHashMap<String , Integer>  getAllRequestPriority()
    {
        if(hashMap == null)
        {
            hashMap = new ConcurrentHashMap<>();

            hashMap.put(ANSWERGENLINK , 2);
            hashMap.put(GETLIST , 2);
            hashMap.put(SYNCREATEALLLIST , 2);
            hashMap.put(SEARCH , 9);
            hashMap.put(SYNSINGLLIST , 1);
            hashMap.put(DELETEBEGINPARTIES , 2);
            hashMap.put(DELETEPARTIES , 3);
            hashMap.put(DELETEENDPARTIES , 4);
            hashMap.put(PASTEBEGINPARTIES , 2);
            hashMap.put(PASTEPARTIES , 3);
            hashMap.put(PASTEENDPARTIES , 4);
            hashMap.put(SINGLBEGINUPDATEPARTIES , 2);
            hashMap.put(SINGLUPDATEPARTIES , 3);
            hashMap.put(SINGLENDUPDATEPARTIES , 4);
            hashMap.put(RENAMESINGLBEGINUPDATEPARTIES , 2);
            hashMap.put(RENAMESINGLUPDATEPARTIES , 3);
            hashMap.put(RENAMESINGLENDUPDATEPARTIES , 4);
            hashMap.put(BEGINPARTIES , 2);
            hashMap.put(PARTIES , 3);
            hashMap.put(ENDPARTIES , 4);
            hashMap.put(SEARCHBEGINFILES , 3);
            hashMap.put(SEARCHPARTIESFILES , 4);
            hashMap.put(SEARCHENDFILES , 5);
            hashMap.put(STATUSPUBLISHER , 2);
            hashMap.put(CHECKWORKINGSERVER , 4);
            hashMap.put(LAZYSINGLBEGINUPDATEPARTIES , 5);
            hashMap.put(LAZYSINGLUPDATEPARTIES , 6);
            hashMap.put(LAZYSINGLENDUPDATEPARTIES , 7);
            hashMap.put(ALERTERROR , 7);
        }


        return hashMap;

    }

    //Настройки на MailSender
    public static String FROMMAIL = "sunil@mail.ru";
    public static String TOMAIL = "sunil@mail.ru";

    //Настройки на MailSender
    public static String LOGINEMAIL = "sunil@mail.ru";
    public static String PASSWORDEMAIL = "passwordmail";
    //для токенов
    //отправляется клиенту что-бы он смог отправить свой token по нужному адресу
    public static String HTTPSERVERURL = "http://hh-soft.ru/clientfiledownload/";

    //для spring security разрешение на доступ по url
    public static String WEBDAVPUBLICURL = "/"+staticVariable.adminLocatorWebDav+"/admin/public/*";


    //для webdavserver разрешение на скачивания файла
    public static String WEBDAVPUBLIC = staticVariable.adminLocatorWebDav+"/admin/public";


}
