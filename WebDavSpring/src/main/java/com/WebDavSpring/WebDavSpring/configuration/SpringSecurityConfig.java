package com.WebDavSpring.WebDavSpring.configuration;

//import com.WebDavSpring.ActiveMq.ListenerActiveMqServer;

import com.WebDavSpring.WebDavSpring.WebDav.Server.servlet.WebDavServlet;
import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.ObjectAuthModel;

import com.WebDavSpring.WebDavSpring.services.Interface.ServiceAuthUser;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUser;
import com.WebDavSpring.WebDavSpring.services.MysqlUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.context.properties.EnableConfigurationProperties;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;

import javax.servlet.http.HttpServlet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import static com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable.WEBDAVPUBLICURL;

/**
 * Created by А д м и н on 09.08.2019.
 */
@Configuration
@EnableConfigurationProperties
@PropertySource(value = "classpath:application.properties")
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    MysqlUserDetailsService userDetailsService;

    @Autowired
    private ServiceUser repository;

    @Autowired
    private ServiceAuthUser authUser;

    //@Autowired
    //ListenerActiveMqServer listActiveMq;

    @Override
    public void configure(AuthenticationManagerBuilder builder) throws Exception {
        builder.userDetailsService(userDetailsService);
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {


        http.cors().and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(WEBDAVPUBLICURL).permitAll()
                .anyRequest()
                .authenticated()
                .and().httpBasic()
                .and().sessionManagement().disable();


    }



    @Override
    public void configure(WebSecurity web) throws Exception {
    // add it

        web.httpFirewall(allowUrlEncodedSlashHttpFirewall());
       // web.httpFirewall(allowUrlEncodedSlashHttpFirewall()).debug(true);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ServletRegistrationBean<HttpServlet> webDavSigleServlet() {
        ServletRegistrationBean<HttpServlet> servRegBean = new ServletRegistrationBean<>();
        ObjectAuthModel objAuth = getAuthModel( repository , authUser);
        servRegBean.setServlet(new WebDavServlet(objAuth));
        //разрешенные имена

        servRegBean.addUrlMappings("/"+staticVariable.userLocatorWebDav+"/*");
        servRegBean.addUrlMappings("/"+staticVariable.adminLocatorWebDav+"/*");
        servRegBean.addUrlMappings("/"+staticVariable.adminLocatorWebDav+"/public");
        servRegBean.setLoadOnStartup(1);

        return servRegBean;
    }

    private ObjectAuthModel getAuthModel(ServiceUser repository , ServiceAuthUser authUser)
    {
        ObjectAuthModel objAuth = new ObjectAuthModel();
        objAuth.setRepository(repository);
        objAuth.setAuthUser(authUser);

        return objAuth;
    }




    //если его вкл. Basic Autentivication перестает работать
//	@Bean
//	public ServletRegistrationBean<HttpServlet> defaultServlet() {
//		ServletRegistrationBean<HttpServlet> servRegBean = new ServletRegistrationBean<>();
//		servRegBean.setServlet(new DefaultServlet());
//		servRegBean.addUrlMappings("/");
//		servRegBean.setLoadOnStartup(2);
//		return servRegBean;
//	}

    @Bean
    public HttpFirewall allowUrlEncodedSlashHttpFirewall() {
        StrictHttpFirewall fireWall = new StrictHttpFirewall();
        //fireWall.setAllowUrlEncodedSlash(true);
        Collection<String> listHttpMethods = new ArrayList<>();
        listHttpMethods.add("HEAD");
        listHttpMethods.add("DELETE");
        listHttpMethods.add("POST");
        listHttpMethods.add("GET");
        listHttpMethods.add("MOVE");
        listHttpMethods.add("COPY");
        listHttpMethods.add("OPTIONS");
        listHttpMethods.add("PUT");
        listHttpMethods.add("PROPFIND");
        listHttpMethods.add("MKCOL");
        fireWall.setAllowedHttpMethods(listHttpMethods);



        return fireWall;
    }


    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.mail.ru");
        mailSender.setPort(465);

        mailSender.setUsername(staticVariable.LOGINEMAIL);
        mailSender.setPassword(staticVariable.PASSWORDEMAIL);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtps");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.starttls.required", "true");
        props.put("mail.debug", "true");



        return mailSender;
    }

    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(5);
        pool.setMaxPoolSize(10);
        pool.setWaitForTasksToCompleteOnShutdown(true);
        return pool;
    }




}