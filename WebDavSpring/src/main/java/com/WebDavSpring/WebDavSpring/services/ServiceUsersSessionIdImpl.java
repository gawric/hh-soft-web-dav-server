package com.WebDavSpring.WebDavSpring.services;

import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.UsersSessionIdModel;
import com.WebDavSpring.WebDavSpring.repository.UsersSessionIdRepository;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUsersPublisher;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUsersSessionId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by А д м и н on 07.04.2020.
 */
@Service("ServiceUsersSessionId")
public class ServiceUsersSessionIdImpl implements ServiceUsersSessionId
{
    @Autowired
    private UsersSessionIdRepository usersPublisherRepository;



    @Override
    public List<UsersSessionIdModel> findByUserid(int userid)
    {
        return null;
    }

    @Override
    public void addUsersSessionId(UsersSessionIdModel sessionIdModel)
    {
         usersPublisherRepository.save(sessionIdModel);
    }

    @Override
    public void deleteUsersSessionId(UsersSessionIdModel sessionIdModel)
    {
        usersPublisherRepository.delete(sessionIdModel);
    }

    @Override
    public void deleteAllSessionUsers(int userid)
    {
        usersPublisherRepository.deleteAllSessionUsers(userid);
    }

    @Override
    public void deleteConnectionByClientId(String sessionid)
    {
        usersPublisherRepository.deleteConnectionByClientId(sessionid);
    }
}
