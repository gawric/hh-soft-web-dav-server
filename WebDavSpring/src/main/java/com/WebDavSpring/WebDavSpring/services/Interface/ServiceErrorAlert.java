package com.WebDavSpring.WebDavSpring.services.Interface;

import com.WebDavSpring.WebDavSpring.model.ErrorAlertModel;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;

/**
 * Created by А д м и н on 09.07.2020.
 */
public interface ServiceErrorAlert {

    void saveError(ErrorAlertModel errorModel);
    void deleteError(ErrorAlertModel errorModel );
    void deleteError(int row_id);
}
