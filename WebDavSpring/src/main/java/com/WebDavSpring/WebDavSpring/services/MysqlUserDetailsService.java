package com.WebDavSpring.WebDavSpring.services;

import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Created by А д м и н on 27.08.2019.
 */
@Component
public class MysqlUserDetailsService implements UserDetailsService
{
    @Autowired
    private ServiceUser repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        UsersModel user = repository.findByUsername(username);

        if(user == null)
        {
            throw new UsernameNotFoundException("User not found");
        }

        List<SimpleGrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority(user.getRole()));

        return new User(user.getUsername(), user.getPassword() ,authorities );
    }

    public Iterable<UsersModel> getAllUsersModel()
    {
        return repository.findByAllUser();
    }
}