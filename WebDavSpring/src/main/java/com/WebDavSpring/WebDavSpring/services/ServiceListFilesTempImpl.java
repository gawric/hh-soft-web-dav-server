package com.WebDavSpring.WebDavSpring.services;

import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.repository.ListFilesRepository;
import com.WebDavSpring.WebDavSpring.repository.ListFilesTempRepository;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceListFilesTemp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


/**
 * Created by А д м и н on 22.10.2020.
 */
@Service("ServiceListFilesTemp")
public class ServiceListFilesTempImpl implements ServiceListFilesTemp {

    @Autowired
    private ListFilesTempRepository listTempRepository;


    @Override
    public void copyGroupListFilesToTemp(long user_id, long b_row_id, long e_row_id, String changeRows) {
        try
        {
            listTempRepository.copyGroupListFilesToTemp(user_id, b_row_id, e_row_id , changeRows);
        }catch (org.springframework.dao.DataIntegrityViolationException b)
        {
            System.out.println("ServiceListFilesTempImpl-> не возможно скопировать данные, найден дубликат в primary key   "+b.toString());
        }

    }

    @Override
    public void updateSinglFileChangeRows(long user_id, long row_id, long changeRows) {
        listTempRepository.updateSinglFileChangeRows(user_id , row_id , changeRows);
    }

    @Override
    public void copySinglFilesToTemp(long user_id, long row_id, String changeRows) {
        listTempRepository.copySinglFilesToTemp(user_id, row_id, changeRows);
    }

    @Override
    public List<FileInfoModel> listFilesGetTempDublicationRow_id(long user_id) {
        return listTempRepository.listFilesGetTempDublicationRow_id(user_id);
    }

    @Override
    public void deleteSinglTempFiles(long row_id, long user_id) {
        listTempRepository.deleteSinglTempFiles(row_id , user_id);
    }

    //удаление файла по внутреннему id номеру Temp, а не по row_id
    @Override
    public void deleteSinglTempFilesId(long id, long user_id) {
        listTempRepository.deleteSinglTempFilesId(id , user_id);
    }

    @Override
    public List<Long> listFilesGetAllRow_id(long user_id) {
        return listTempRepository.listFilesGetAllRow_id(user_id);
    }

    @Override
    public List<Long> listFilesGetAllRow_id(long user_id , long versioUpdateRows) {
        return listTempRepository.listFilesGetAllRow_idAndVersionRows(  user_id ,  versioUpdateRows );
    }

    @Transactional
    @Override
    public void deleteAllUserId(long user_id) {
        listTempRepository.deleteAllUserId(user_id);
    }

    @Override
    public List<FileInfoModel> singleListFilesTemp(long user_id, long row_id) {
        return listTempRepository.singleListFilesTemp( user_id,  row_id);

    }


}
