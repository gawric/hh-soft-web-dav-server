package com.WebDavSpring.WebDavSpring.services.Interface;

import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.UsersModel;

import java.util.List;

/**
 * Created by А д м и н on 19.09.2019.
 */

public interface ServiceUser
{
    Iterable<UsersModel> findByAllUser();
    UsersModel findByUsername(String username);
    UsersModel findByFileInfoModel(long row_id);

    void saveUsers(UsersModel model);
    boolean existsByUsername(String username);
}
