package com.WebDavSpring.WebDavSpring.services.Interface;

import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.ConnectedModel;
import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.UsersSessionIdModel;
import org.springframework.data.repository.query.Param;

import java.util.List;


/**
 * Created by А д м и н on 07.04.2020.
 */
public interface ServiceUsersConnected {

    ConnectedModel findByUsername(String username);
    List<ConnectedModel> findByUsersid(int id);
    ConnectedModel findBySessionid(String sessionid);
    ConnectedModel findByClientid(String clientid);

    void addConnectedUsersId(ConnectedModel connectedIdModel);
    void deleteConnectedUsersId(ConnectedModel connectedIdModel);
    void deleteAllConnectionUsers(int usersid);
    void deleteConnectionByClientId(String clientid);
    void deleteConnectionBySessionId(String sessionid);
    void clearAllConnected();

    ConnectedModel findPublisher(int userid);
}
