package com.WebDavSpring.WebDavSpring.services.Interface;


import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by А д м и н on 22.10.2020.
 */
public interface ServiceListFilesTemp {

    void copyGroupListFilesToTemp(long user_id ,  long b_row_id ,  long e_row_id,  String changeRows);
    void updateSinglFileChangeRows(long user_id ,  long row_id  ,  long changeRows);
    void copySinglFilesToTemp( long user_id , long row_id , String changeRows);
    List<FileInfoModel> listFilesGetTempDublicationRow_id(long user_id );
    void deleteSinglTempFiles(long row_id , long user_id);
    void deleteSinglTempFilesId(long id , long user_id);
    List<Long> listFilesGetAllRow_id( long user_id );
    void deleteAllUserId(long user_id);
    List<FileInfoModel>  singleListFilesTemp(long user_id , long row_id );
    List<Long> listFilesGetAllRow_id(long user_id , long versioUpdateRows);
}
