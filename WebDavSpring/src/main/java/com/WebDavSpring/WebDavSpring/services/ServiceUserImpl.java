package com.WebDavSpring.WebDavSpring.services;

import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.repository.UsersRepository;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by А д м и н on 19.09.2019.
 */
@Service("ServiceUser")
public class ServiceUserImpl implements ServiceUser
{

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public Iterable<UsersModel> findByAllUser() {
        return usersRepository.findAll();
    }

    @Override
    public UsersModel findByUsername(String username) {
        return usersRepository.findByUsername(username);
    }

    @Override
    public UsersModel findByFileInfoModel(long row_id) {
     return null;
    }


    @Override
    public void saveUsers(UsersModel model) {
        usersRepository.save(model);
    }

    @Override
    public boolean existsByUsername(String username) {
        return usersRepository.existsByUsername(username);
    }
}
