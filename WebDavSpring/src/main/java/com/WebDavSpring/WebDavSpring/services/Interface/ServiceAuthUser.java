package com.WebDavSpring.WebDavSpring.services.Interface;

/**
 * Created by А д м и н on 01.04.2020.
 */
public interface ServiceAuthUser
{
    String currentUsername();
    //true - user
    //false - admin
    Boolean isUser();
    Boolean isAdmin();
    String getNameSpaceUser(long user_id);
}
