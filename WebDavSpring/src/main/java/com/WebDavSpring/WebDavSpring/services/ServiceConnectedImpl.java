package com.WebDavSpring.WebDavSpring.services;

import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.ConnectedModel;
import com.WebDavSpring.WebDavSpring.repository.ConnectedRepository;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUsersConnected;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by А д м и н on 07.04.2020.
 */

@Service("ServiceConnected")
public class ServiceConnectedImpl implements ServiceUsersConnected
{
    @Autowired
    private ConnectedRepository connectedRepository;

    @Override
    public ConnectedModel findByUsername(String username)
    {
        return connectedRepository.findByUsername(username);
    }

    @Override
    public List<ConnectedModel> findByUsersid(int id) {
        return connectedRepository.findByUsersid(id);
    }


    @Override
    public ConnectedModel findBySessionid(String sessionid)
    {
        return connectedRepository.findBySessionid(sessionid);
    }

    @Override
    public ConnectedModel findByClientid(String clientid) {
        return connectedRepository.findByClientid(clientid);
    }

    @Override
    public void addConnectedUsersId(ConnectedModel connectedIdModel) {
        connectedRepository.save(connectedIdModel);
    }

    @Override
    public void deleteConnectedUsersId(ConnectedModel connectedIdModel) {
        connectedRepository.delete(connectedIdModel);
    }

    @Override
    public void deleteAllConnectionUsers(int usersid) {
        connectedRepository.deleteAllConnectionUsers(usersid);
    }

    @Override
    public void deleteConnectionByClientId(String clientid) {
        connectedRepository.deleteConnectionByClientId(clientid);
    }

    @Override
    public void deleteConnectionBySessionId(String sessionid) {
        connectedRepository.deleteConnectionBySessionId(sessionid);
    }

    @Override
    public void clearAllConnected() {
       // List<ConnectedModel> listAll = connectedRepository.findAll();
        connectedRepository.deleteAll();
       // if(!listAll.isEmpty())
        //{
           // for(int a=0; a < listAll.size(); a++)
          //  {
            //    ConnectedModel model = listAll.get(a);

            //    if(model != null)
             //   {
                   // connectedRepository.deleteConnectionBySessionId(model.getSessionid());
              //  }

          //  }
       // }
    }

    @Override
    public ConnectedModel findPublisher(int userid) {
        return connectedRepository.findPublisher(userid);
    }


}
