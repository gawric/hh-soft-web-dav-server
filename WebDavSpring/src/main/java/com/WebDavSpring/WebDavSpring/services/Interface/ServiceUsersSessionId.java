package com.WebDavSpring.WebDavSpring.services.Interface;

import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.UsersPublisherModel;
import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.UsersSessionIdModel;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by А д м и н on 07.04.2020.
 */
public interface ServiceUsersSessionId
{
    List<UsersSessionIdModel> findByUserid(int id);
    void addUsersSessionId(UsersSessionIdModel sessionIdModel);
    void deleteUsersSessionId(UsersSessionIdModel sessionIdModel);
    void deleteAllSessionUsers(int userid);
    void deleteConnectionByClientId(String sessionid);
}
