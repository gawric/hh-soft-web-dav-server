package com.WebDavSpring.WebDavSpring.services.Interface;


import com.WebDavSpring.WebDavSpring.model.WebServerLinkInfoModel;
import org.springframework.data.repository.query.Param;


/**
 * Created by А д м и н on 25.08.2020.
 */
public interface ServiceWebServerLinkInfo {

    WebServerLinkInfoModel findByToken(String token);
    void saveFiles(WebServerLinkInfoModel model);
    void deleteFiles(WebServerLinkInfoModel model);
    void deleteSinglToken(long row_id , long user_id);
}
