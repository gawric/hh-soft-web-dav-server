package com.WebDavSpring.WebDavSpring.services;

import com.WebDavSpring.WebDavSpring.model.ClientInfoSystemModel;
import com.WebDavSpring.WebDavSpring.repository.ClientInfoRepository;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceClientInfo;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by А д м и н on 21.08.2020.
 */
@Service("ServiceClientInfo")
public class ServiceClientInfoImpl implements ServiceClientInfo {

    @Autowired
    private ClientInfoRepository clientInfoRepository;

    @Override
    public void addClientInfo(ClientInfoSystemModel clientInfoSystemModel) {
        clientInfoRepository.save(clientInfoSystemModel);
    }

    @Override
    public void removeClientInfo(ClientInfoSystemModel clientInfoSystemModel) {
        clientInfoRepository.delete(clientInfoSystemModel);
    }

    @Override
    public ClientInfoSystemModel findByMacClientInfo(String clientMacInfo) {
        return clientInfoRepository.findByMacnetwork(clientMacInfo);
    }


}
