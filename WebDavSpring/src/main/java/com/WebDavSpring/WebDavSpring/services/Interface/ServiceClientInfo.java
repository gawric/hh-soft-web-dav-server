package com.WebDavSpring.WebDavSpring.services.Interface;

import com.WebDavSpring.WebDavSpring.model.ClientInfoSystemModel;

/**
 * Created by А д м и н on 21.08.2020.
 */
public interface ServiceClientInfo {

    void addClientInfo(ClientInfoSystemModel clientInfoSystemModel);
    void removeClientInfo(ClientInfoSystemModel clientInfoSystemModel);
    ClientInfoSystemModel findByMacClientInfo(String clientMacInfo);

}
