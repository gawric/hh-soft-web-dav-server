package com.WebDavSpring.WebDavSpring.services;

import com.WebDavSpring.WebDavSpring.model.WebServerLinkInfoModel;
import com.WebDavSpring.WebDavSpring.repository.WebServerLinkInfoRepository;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUsersPublisher;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceWebServerLinkInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by А д м и н on 25.08.2020.
 */
@Service("ServiceWebServerLinkInfo")
public class ServiceWebServerLinkInfoImpl implements ServiceWebServerLinkInfo{


    @Autowired
    private WebServerLinkInfoRepository linkInfoWebServer;


    @Override
    public WebServerLinkInfoModel findByToken(String token) {

        if(token != null)
        {
            if(!token.equals(""))
            {
                return linkInfoWebServer.findByToken(token);
            }
            else
            {
                return new WebServerLinkInfoModel();
            }
        }
        else
        {
            return  new WebServerLinkInfoModel();
        }


    }

    @Override
    public void saveFiles(WebServerLinkInfoModel model) {
        if(model != null)linkInfoWebServer.save(model);
    }

    @Override
    public void deleteFiles(WebServerLinkInfoModel model) {
        if(model != null)linkInfoWebServer.delete(model);
    }

    @Transactional
    @Override
    public void deleteSinglToken(long row_id, long user_id) {
        linkInfoWebServer.deleteSinglToken(row_id , user_id);
    }
}
