package com.WebDavSpring.WebDavSpring.services;

import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceAuthUser;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUser;
import javassist.compiler.ast.Variable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by А д м и н on 01.04.2020.
 */
@Service("ServiceAuthUser")
public class ServiceAuthUserImpl implements ServiceAuthUser
{
    @Autowired
    ServiceUser repositoryService;

    @Override
    public String currentUsername()
    {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @Override
    public Boolean isUser() {
        Collection<? extends GrantedAuthority> roleList  = SecurityContextHolder.getContext().getAuthentication().getAuthorities();

        return  roleList
                    .stream()
                    .anyMatch(entry -> entry.getAuthority().equals("user"));
    }

    @Override
    public Boolean isAdmin() {
        Collection<? extends GrantedAuthority> roleList  = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        return  roleList
                .stream()
                .anyMatch(entry -> entry.getAuthority().equals("admin"));
    }

    @Override
    public String getNameSpaceUser(long user_id) {
        String roleUser = getUserRole(user_id);

        if(roleUser.indexOf("user")!= -1)
        {
            return staticVariable.userLocatorWebDav+"/";
        }
        else
        {
            if(roleUser.indexOf("admin")!= -1)
            {
                return staticVariable.adminLocatorWebDav+"/";
            }
            else
            {
                return "anonymous/";
            }
        }
    }



    private String getUserRole(long user_id)
    {
        int user_id_convert = (int)user_id;

        String[] role = {"anonymous"};
        Iterable<UsersModel> list =  repositoryService.findByAllUser();
        role = iterableRole( list ,  user_id_convert ,  role);


        return role[0];
    }


    private String[]  iterableRole(Iterable<UsersModel> list , int user_id_convert , String[] role)
    {
        list.forEach(obj -> {
            if(obj.getId() == user_id_convert)
            {
                role[0] = obj.getRole();
            }

        });

        return role;
    }
}
