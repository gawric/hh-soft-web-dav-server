package com.WebDavSpring.WebDavSpring.services.Interface;

import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by А д м и н on 25.09.2019.
 */
public interface ServiceListFiles {


    FileInfoModel findUserByRow_id( long row_id , long user_id);

    List<FileInfoModel> findUserByUser_id(long user_id);
    List<FileInfoModel> findUserByUser_idAndType( long user_id , String type1);
    long existsRowId(long row_id , long user_id);

    boolean existsTableFileInfo(long user_id);

    void deleteAllFileUserTableInfo(long user_id);

    void deleteSpring(FileInfoModel file);


    void saveListFiles(FileInfoModel[] list);
    void saveListFilesVersion(FileInfoModel[] list , long newVirsionRows);
    void saveListFilesNewVersionBases(FileInfoModel[] list , long newVirsionBases);
    void saveListFilesCreateAllListNewVersionBases(FileInfoModel[] list , long newVirsionBases);

    void saveFiles(FileInfoModel files);
    void updateFiles(FileInfoModel files);
    void deleteSinglFileInfo(FileInfoModel fileInfoModel);


    long findVersionBasesUserId(long user_id);

    long sizeAllFileInfoByUser_id(long user_id2);

    void updateListFiles(FileInfoModel[] list);

    long getUserMaxVersionRows(long user_id2);

    long getUserMaxVersionBases(long user_id2);

    void deleteSinglFileUserTableInfo(FileInfoModel[] list);

    List<Long> getAllRowsLongByUserId(long user_id);
    List<Long> getVersionUpdateRowsLongByUserId(long user_id2);
    List<Long> listFilesGetAllRow_idAndVersionRowsTemp(long user_id , long versionUpdateRows);
    List<Long> getVersionUpdateRowsLongByUserIdAndByVersionRows(long user_id2 , long versionUpdateRows);

    void deleteSinglRowId(long row_id , long user_id);

    void flush();
    void deleteSinglGroupListFiles( long b_row_id ,  long e_row_id  , long user_id);

    //получает из mysql нужные данные
    List<FileInfoModel> selectGroupListMysql(long b_row_id, long e_row_id, long user_id);

    //вся внутреняя логика обработки полученных данных и возвращает уже в нормальном виде
    List<List<FileInfoModel>> selectGroupAllList(List<Long> allRowsLong , long user_id);

    void updateSinglList(FileInfoModel[] clientListFileInfo , int user_id , Boolean isCopyTemp);

   void  updateSinglFileUserTableInfo( long row_id ,  long user_id ,  FileInfoModel clientInfo);

    void updateSinglFileUserVersionRows(long row_id ,  long user_id  , long newversionUpdateRows , long newversionUpdateBases);

    //получаем максимальный id номер пользователя
    int getUserMaxId(long user_id2);

    List<Long> listFilesGetAllRow_idTemp( long user_id );
    List<FileInfoModel> selectListTemp(List<Long> sortedList, long user_id);
    void setVersionRows(long user_id2 , long versionUpdateRows);
    void setVersionAllRows(long user_id2 , long versionUpdateRows);

    void deleteFileListTempAllUserId(long user_id);

    void setVersionAllRowsBases(long user_id2 , long versionUpdateBases);

}
