package com.WebDavSpring.WebDavSpring.services;

import com.WebDavSpring.ActiveMq.GroupId;
import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariableBases;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.repository.ListFilesRepository;
import com.WebDavSpring.WebDavSpring.repository.ListFilesTempRepository;
import com.WebDavSpring.WebDavSpring.repository.UsersRepository;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceListFiles;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceListFilesTemp;
import org.apache.tomcat.jni.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by А д м и н on 25.09.2019.
 */
@Service("ServiceListFiles")
public class ServiceListFilesImpl implements ServiceListFiles
{

    @Autowired
    private ListFilesRepository listRepository;

    @Autowired
    private ServiceListFilesTemp listFilesTempRepository;


    @Override
    public FileInfoModel findUserByRow_id(long row_id , long user_id)
    {
        //Exception in thread "taskExecutor-6" org.springframework.dao.IncorrectResultSizeDataAccessException: query did not return a unique result: 2; nested exception is javax.persistence.NonUniqueResultException: query did not return a unique result: 2
        //нужно обработать
        System.out.println("Ищем данные на сервере row_id  "+row_id);
        List<FileInfoModel> listModel  = listRepository.findUserByRow_id(row_id , user_id);

        if(listModel.size() > 1) {

            FileInfoModel dubleId = listModel.get(1);

            int maxId = listRepository.getUserMaxId(user_id);
            Optional<FileInfoModel> model = listRepository.findById(String.valueOf(maxId));

            long newRow_id = model.get().getRow_id() + 1;
            dubleId.setRow_id(newRow_id);

            System.out.println("Произошла критическая ошибка: findUserByRow_id вернул 2 результата row_id "+ dubleId.getRow_id() +" в сервере id "+dubleId.getId());
            System.out.println("findUserByRow_id -> пытаемся изменить row_id на " + dubleId.getRow_id());
            listRepository.save(dubleId);
        }

        if(!listModel.isEmpty()){
            return listModel.get(0);
        }
        else
        {
            return null;
        }

    }

    @Override
    public List<FileInfoModel> findUserByUser_id(long user_id)
    {
        return listRepository.findUserByUser_id(user_id);
    }

    @Override
    public List<FileInfoModel> findUserByUser_idAndType(long user_id, String type1)
    {
        return listRepository.findUserByUser_idAndType(user_id , type1);
    }

    @Override
    public long existsRowId(long row_id, long user_id)
    {
        try
        {
            return listRepository.existsRowId(row_id , user_id);
        }
        catch (org.springframework.aop.AopInvocationException s)
        {
            return -1;
        }

    }

    @Override
    public boolean existsTableFileInfo(long user_id) {

        FileInfoModel serverModel = listRepository.existsTableFileInfo(user_id);

        if(serverModel == null)
        {
            return true;
        }
        else
        {
            return false;
        }

    }



    //удаляем все данные юзера
    @Override
    public void deleteAllFileUserTableInfo(long user_id)
    {
        listRepository.deleteAllFileUserTableInfo(user_id);

    }

    @Override
    public void deleteSpring(FileInfoModel file) {
       // listRepository.
    }

    //удаляет только 1 запись
    @Transactional
    @Override
    public void deleteSinglFileUserTableInfo(FileInfoModel[] fileInfoModel)
    {
        if(fileInfoModel != null)
        {
            for(int j = 0; j < fileInfoModel.length; j++)
            {
                if(fileInfoModel[j] != null)
                {
                    long row_id = fileInfoModel[j].getRow_id();
                    long user_id = fileInfoModel[j].getUser_id();
                    listRepository.deleteSinglFileUserTableInfo(row_id , user_id);
                }
            }
        }
        else
        {
            System.out.println("deleteSinglFileUserTableInfo -> удаление не возможно, массив оказался пустым");
        }


    }

    public synchronized void operationSinglUpdate(FileInfoModel clientListFileInfo  , List<Long> deletesortedList , List<FileInfoModel> updatesortedList , long user_id , Boolean isCopyTemp)
    {
        if(clientListFileInfo.getChangeRows().equals("CREATE"))
        {
            //Обновляем version в начале запроса
            clientListFileInfo.setVersionUpdateRows(staticVariableBases.getLastVersionRows(user_id));
            this.saveFiles(clientListFileInfo);
            clientListFileInfo.setChangeRows("CREATE");

            if(isCopyTemp)
            {
                listFilesTempRepository.copySinglFilesToTemp(clientListFileInfo.getUser_id() , clientListFileInfo.getRow_id() , "CREATE");
            }

        }
        else if(clientListFileInfo.getChangeRows().equals("UPDATE"))
        {

            delete(clientListFileInfo , deletesortedList);
            clientListFileInfo.setVersionUpdateRows(staticVariableBases.getLastVersionRows(user_id));
            //только после DELETE
            clientListFileInfo.setChangeRows("UPDATE");
            updatesortedList.add(clientListFileInfo);
        }
        else if(clientListFileInfo.getChangeRows().equals("DELETE"))
        {

            delete(clientListFileInfo , deletesortedList);
        }
    }

    private void delete(FileInfoModel clientListFileInfo , List<Long> deletesortedList)
    {
        //удаление происходит позже (сразу пачками)
        clientListFileInfo.setChangeRows("DELETE");

        if(clientListFileInfo != null)
        {
            deletesortedList.add(clientListFileInfo.getRow_id());
        }
        else
        {
            System.out.println("ServiceListFilesImpl -> deleteSinglFileInfo -> удаление не возможно, FileInfoModel оказался пустым");
        }
    }



    @Override
    public void deleteSinglFileInfo(FileInfoModel fileInfoModel)
    {
        if(fileInfoModel != null)
        {
            long row_id = fileInfoModel.getRow_id();
            long user_id = fileInfoModel.getUser_id();
            listRepository.deleteSinglFileUserTableInfo(row_id , user_id);
        }
        else
        {
            System.out.println("ServiceListFilesImpl -> deleteSinglFileInfo -> удаление не возможно, FileInfoModel оказался пустым");
        }

    }

    @Transactional
    @Override
    public void saveListFiles(FileInfoModel[] list)
    {

        if(list != null)
        {
            for(int d = 0; d < list.length; d++)
            {
                if(list[d] != null)
                {
                    listRepository.saveAndFlush(list[d]);
                    listFilesTempRepository.copySinglFilesToTemp(list[d].getUser_id() , list[d].getRow_id(), "CREATE");
                }

            }
        }


        // System.out.println("База данных обновленна flush выполнен");
    }

    @Transactional
    @Override
    public void saveListFilesVersion(FileInfoModel[] list , long newVirsionRows)
    {

        if(list != null)
        {
            for(int d = 0; d < list.length; d++)
            {
                if(list[d] != null)
                {
                    FileInfoModel fim = list[d];
                    fim.setVersionUpdateRows(newVirsionRows);
                    listRepository.saveAndFlush(list[d]);
                    listFilesTempRepository.copySinglFilesToTemp(fim.getUser_id() , fim.getRow_id(), "CREATE");
                }

            }
        }


        // System.out.println("База данных обновленна flush выполнен");
    }


    @Transactional
    @Override
    public void saveListFilesNewVersionBases(FileInfoModel[] list , long newVirsionBases)
    {

        if(list != null)
        {
            for(int d = 0; d < list.length; d++)
            {
                if(list[d] != null)
                {
                    FileInfoModel fim = list[d];
                    fim.setVersionUpdateBases(newVirsionBases);
                    listRepository.saveAndFlush(fim);
                    listFilesTempRepository.copySinglFilesToTemp(list[d].getUser_id() , list[d].getRow_id(), "CREATE");
                }

            }
        }


        // System.out.println("База данных обновленна flush выполнен");
    }

    @Transactional
    @Override
    public void saveListFilesCreateAllListNewVersionBases(FileInfoModel[] list , long newVirsionBases)
    {

        if(list != null)
        {
            for(int d = 0; d < list.length; d++)
            {
                if(list[d] != null)
                {
                    FileInfoModel fim = list[d];
                    fim.setVersionUpdateBases(newVirsionBases);
                    listRepository.save(fim);
                }

            }
        }
        listRepository.flush();
        // System.out.println("База данных обновленна flush выполнен");
    }

    @Override
    public void saveFiles(FileInfoModel files)
    {
        listRepository.save(files);

    }

    @Override
    public void updateFiles(FileInfoModel files)
    {
        FileInfoModel find = findUserByRow_id(files.getRow_id() , files.getUser_id());

        if(find != null)
        {
            listRepository.deleteSinglFileUserTableInfo(find.getRow_id() , find.getUser_id());
            listRepository.save(files);
        }
        else
        {

            listRepository.save(files);
            //System.out.println("ServiceListFilesImpol: такой row_id существует: " + files.getRow_id());
        }

    }


    public void updateFilesNoCheck(FileInfoModel files)
    {
       // FileInfoModel find = findUserByRow_id(files.getRow_id() , files.getUser_id());

       // if(find != null)
       // {
            listRepository.deleteSinglFileUserTableInfo(files.getRow_id() , files.getUser_id());
            listRepository.save(files);
      //  }
      //  else
      //  {

       // listRepository.deleteSinglFileUserTableInfo(files.getRow_id() , files.getUser_id());
        //listRepository.updateSinglFileUserTableInfo(files.getRow_id(),  files.getUser_id(),  files);
       // listRepository.updateSinglFileUserTableInfo(files.getRow_id() ,  files.getUser_id(),  files.getFilename(),  files.getLocation()  ,  files.getParent(), files.getVersionUpdateBases());
            //System.out.println("ServiceListFilesImpol: такой row_id существует: " + files.getRow_id());
       // }

    }


    @Override
    public long findVersionBasesUserId(long user_id)
    {
        FileInfoModel serverModel = listRepository.existsTableFileInfo(user_id);

        if(serverModel != null)
        {
            return serverModel.getVersionUpdateBases();
        }
        else
        {
            return 0;
        }

    }

    @Override
    public long sizeAllFileInfoByUser_id(long user_id2)
    {
        return listRepository.sizeAllFileInfoByUser_id(user_id2);
    }

    @Override
    public List<Long> getAllRowsLongByUserId(long user_id)
    {
        return listRepository.getAllRowsLongByUserId(user_id);
    }

    @Transactional
    @Override
    public List<Long> getVersionUpdateRowsLongByUserId(long user_id2) {
        try
        {
            clearDublication(user_id2);
            //List<Long> list1 = listRepository.getVersionUpdateRowsLongByUserId(user_id2 , 0l);
            return listRepository.getVersionUpdateRowsLongByUserId(user_id2 , 0l);
        }
        catch (InvalidDataAccessResourceUsageException a)
        {
            return new ArrayList<>();
        }

    }

    @Transactional
    @Override
    public List<Long> getVersionUpdateRowsLongByUserIdAndByVersionRows(long user_id2 , long versionUpdateRows) {
        try
        {
            clearDublication(user_id2);
            //List<Long> list1 = listRepository.getVersionUpdateRowsLongByUserId(user_id2 , 0l);
            return listRepository.getVersionUpdateRowsLongByUserId(user_id2 , versionUpdateRows);
        }
        catch (InvalidDataAccessResourceUsageException a)
        {
            return new ArrayList<>();
        }

    }

    public static List<Long> joinLists(List<Long> a, List<Long> b) {
        if ((a == null) || (a.isEmpty() && (b != null))) return b;
        if ((b == null) || b.isEmpty()) return a;
        ArrayList<Long> result = new ArrayList(a.size() + b.size()); // Закладываем размер достаточный для всех элементов
        result.addAll(a);
        result.addAll(b);
        return result;
    }


    private void clearDublication(long user_id)
    {
        List<FileInfoModel> list = listFilesTempRepository.listFilesGetTempDublicationRow_id(user_id);

        for(int b = 0; b < list.size(); b++)
        {
            FileInfoModel fim = list.get(b);

            if(fim.getChangeRows().equals("CREATE"))
            {
                listFilesTempRepository.deleteSinglTempFilesId(fim.getId() , fim.getUser_id());
               // listFilesTempRepository.deleteSinglTempFiles(fim.getRow_id() , fim.getUser_id());
            }

        }
    }

    @Override
    public void deleteSinglRowId(long row_id, long user_id) {
        listRepository.deleteSinglFileUserTableInfo(row_id , user_id);
    }

    @Override
    public void flush() {
        listRepository.flush();
    }

    @Transactional
    @Override
    public void updateSinglList(FileInfoModel[] clientListFileInfo , int user_id , Boolean isCopyTemp)
    {

        if(clientListFileInfo != null)
        {
            List<Long> deletesortedList = new ArrayList<Long>();
            List<FileInfoModel> updatesortedList = new ArrayList<>();
            staticVariableBases.setLastVersionRows(user_id , staticVariableBases.getLastVersionRows(user_id) + 1);

            singlUpdate(clientListFileInfo , deletesortedList , updatesortedList , user_id , isCopyTemp);
            deleteGroup(deletesortedList , user_id);
            listRepository.flush();

            updateGroup(updatesortedList);
        }


    }

    @Override
    public void updateSinglFileUserTableInfo(long row_id, long user_id, FileInfoModel clientInfo) {
        if(clientInfo != null)
        {
            listRepository.updateSinglFileUserTableInfo(clientInfo.getRow_id() ,  clientInfo.getUser_id(),  clientInfo.getFilename(),  clientInfo.getLocation() ,  clientInfo.getParent(), clientInfo.getVersionUpdateBases() , clientInfo.getVersionUpdateRows());
        }
    }

    @Override
    public void updateSinglFileUserVersionRows(long row_id, long user_id, long newversionUpdateRows, long newversionUpdateBases) {

    }

    @Transactional
    @Override
    public List<List<FileInfoModel>> selectGroupAllList(List<Long> sortedList, long user_id)
    {
        return selectGroup( sortedList ,  (int)user_id);
    }



    @Override
    public List<FileInfoModel> selectListTemp(List<Long> sortedList, long user_id)
    {
        ArrayList<FileInfoModel> listModel = new ArrayList<>();
        for(int i = 0; i < sortedList.size(); i++)
        {
            long row_id = sortedList.get(i);

            if(row_id != -1)
            {
                List<FileInfoModel>  fim = listFilesTempRepository.singleListFilesTemp(user_id , row_id);
                listModel.add(fim.get(0));
            }

        }
        return listModel;
    }

    @Override
    public void setVersionRows(long user_id2, long versionUpdateRows) {
        listRepository.setVersionRows(user_id2 , versionUpdateRows);
    }

    @Override
    public void setVersionAllRows(long user_id2, long versionUpdateRows) {
        listRepository.setVersionAllRows(user_id2 , versionUpdateRows);
    }

    @Override
    public void deleteFileListTempAllUserId(long user_id) {
        listFilesTempRepository.deleteAllUserId(user_id);
    }

    @Override
    public void setVersionAllRowsBases(long user_id2, long versionUpdateBases) {
        listRepository.setVersionAllRowsBases(user_id2 , versionUpdateBases);
    }


    @Override
    public int getUserMaxId(long user_id2) {
        return listRepository.getUserMaxId(user_id2);
    }

    @Override
    public List<Long> listFilesGetAllRow_idTemp(long user_id) {
        return listFilesTempRepository.listFilesGetAllRow_id(user_id);
    }

    @Override
    public List<Long> listFilesGetAllRow_idAndVersionRowsTemp(long user_id , long versionUpdateRows) {
        return listFilesTempRepository.listFilesGetAllRow_id( user_id ,  versionUpdateRows);
    }

    @Override
    public void deleteSinglGroupListFiles(long b_row_id, long e_row_id, long user_id) {
        listRepository.deleteSinglGroupListFiles(b_row_id , e_row_id , user_id);
    }

    @Override
    public List<FileInfoModel> selectGroupListMysql(long b_row_id, long e_row_id, long user_id) {
        return listRepository.selectGroupListFiles( b_row_id ,  e_row_id  ,user_id);
    }

    @Override
    public void updateListFiles(FileInfoModel[] list)
    {

        if(list != null)
        {

            for(int f = 0 ; f < list.length; f++)
            {
                if(list[f] != null)
                {
                    long row_id = list[f].getRow_id();
                    long user_id = list[f].getUser_id();
                    FileInfoModel serverModel = findUserByRow_id(row_id , user_id);
                    FileInfoModel clientModel = list[f];
                    if(serverModel != null)
                    {

                        serverModel.setRow_id(clientModel.getRow_id());
                        serverModel.setChangeRows(clientModel.getChangeRows());
                        serverModel.setSizebyte(clientModel.getSizebyte());
                        serverModel.setType(clientModel.getType());
                        serverModel.setVersionUpdateRows(clientModel.getVersionUpdateRows());
                        serverModel.setChangeDate(clientModel.getChangeDate());
                        serverModel.setCreateDate(clientModel.getCreateDate());
                        serverModel.setParent(clientModel.getParent());
                        serverModel.setLastOpenDate(clientModel.getLastOpenDate());
                        serverModel.setLocation(clientModel.getLocation());
                        serverModel.setFilename(clientModel.getFilename());

                        listRepository.saveAndFlush(clientModel);
                    }
                    else
                    {
                        listRepository.saveAndFlush(clientModel);
                    }

                }

            }

        }


    }


    @Override
    public long getUserMaxVersionRows(long user_id2) {
        try
        {
            return listRepository.getUserMaxVersionRows(user_id2);
        } catch (org.springframework.aop.AopInvocationException ex) {
           return  0;
        }

    }

    @Override
    public long getUserMaxVersionBases(long user_id2) {
        try
        {
            return listRepository.getUserMaxVersionBases(user_id2);
        } catch (org.springframework.aop.AopInvocationException ex) {
            return  0;
        }
    }


    private void singlUpdate(FileInfoModel[] clientListFileInfo , List<Long> sortedList , List<FileInfoModel> updatesortedList , long user_id , Boolean isCopyTemp)
    {
        if(clientListFileInfo != null)
        {
            for(int j = 0 ; j < clientListFileInfo.length; j++)
            {
                FileInfoModel clientModel = clientListFileInfo[j];

                if(clientModel != null)
                {
                    operationSinglUpdate(clientModel , sortedList , updatesortedList , user_id , isCopyTemp);
                }
            }
        }

    }

    private List<List<FileInfoModel>> selectGroup(List<Long> sortedList , int user_id)
    {
        if(!sortedList.isEmpty())
        {
            // Object[] sortedArray = sortedList.toArray();
            List<Object[]>  list = combine(sortedList);
            return  selectCombineGroup( list  ,  user_id);
        }
        else
        {
            return new ArrayList<>();
        }
    }
    private void deleteGroup(List<Long> sortedList , int user_id)
    {

        updateVersionRows(user_id);

        if(!sortedList.isEmpty())
        {
            // Object[] sortedArray = sortedList.toArray();
            List<Object[]>  list = combine(sortedList);
            deleteCombineGroup( list  ,  user_id);
        }
    }

    private void updateVersionRows(long user_id)
    {
        //Обновляем Version в начале запроса
        long newVersion = staticVariableBases.getLastVersionRows(user_id);
        listRepository.setVersionRows(user_id , newVersion);
        staticVariableBases.setLastVersionRows(user_id , newVersion);
    }
    private void updateGroup(List<FileInfoModel> updatesortedList)
    {
        if(!updatesortedList.isEmpty())
        {
            listRepository.saveAll(updatesortedList);
        }
        updatesortedList = null;
    }

    private void deleteCombineGroup(List<Object[]>  list  , int user_id)
    {
        for(int  k = 0 ; k < list.size(); k++)
        {
            Object[] deleteArr = list.get(k);
            long begin = Long.parseLong(deleteArr[0].toString());
            long end = Long.parseLong(deleteArr[deleteArr.length - 1].toString());

            //запись в базу темпов что-бы сказать другим подкл. что мы его киляли
            listFilesTempRepository.copyGroupListFilesToTemp(user_id ,  begin , end , "DELETE");

            listRepository.deleteSinglGroupListFiles(begin , end , user_id);

        }
    }

    private List<List<FileInfoModel>> selectCombineGroup(List<Object[]>  list  , int user_id)
    {
        List<List<FileInfoModel>> listGroup = new ArrayList<>();
        for(int  k = 0 ; k < list.size(); k++)
        {
            Object[] deleteArr = list.get(k);
            long begin = Long.parseLong(deleteArr[0].toString());
            long end = Long.parseLong(deleteArr[deleteArr.length - 1].toString());

            listGroup.add(listRepository.selectGroupListFiles(begin , end , user_id));
        }

        return listGroup;
    }


    private List<Object[]> combine(List<Long> sortedList)
    {
        GroupId group = new GroupId();
        Long[] sortedArray = sortedList.stream().toArray( n -> new Long[n]);
        List<Object[]>  list = group.startCombineId(sortedArray);

        return list;
    }
}
