package com.WebDavSpring.WebDavSpring.services;

import com.WebDavSpring.WebDavSpring.model.ErrorAlertModel;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.repository.ConnectedRepository;
import com.WebDavSpring.WebDavSpring.repository.ErrorAlertRepository;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceErrorAlert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by А д м и н on 09.07.2020.
 */

@Service("ServiceErrorAlert")
public class ServiceErrorAlertImpl implements ServiceErrorAlert {

    @Autowired
    private ErrorAlertRepository errorAlertRepository;

    @Override
    public void saveError(ErrorAlertModel errorModel) {
        errorAlertRepository.save(errorModel);
    }

    @Override
    public void deleteError(ErrorAlertModel errorModel) {
        errorAlertRepository.delete(errorModel);
    }

    @Override
    public void deleteError(int row_id) {
        errorAlertRepository.deleteById(String.valueOf(row_id));
    }
}
