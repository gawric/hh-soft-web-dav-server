package com.WebDavSpring.WebDavSpring.services.Interface;

import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.UsersPublisherModel;

/**
 * Created by А д м и н on 07.04.2020.
 */
public interface ServiceUsersPublisher {

    UsersPublisherModel findByClientid(long clientId);
    UsersPublisherModel findBySessionId(long sessionId);
    UsersPublisherModel findByUserid(long userId);
}
