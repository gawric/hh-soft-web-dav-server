package com.WebDavSpring.WebDavSpring.services;

import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.UsersPublisherModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUsersPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by А д м и н on 07.04.2020.
 */
@Service("ServiceUsersPublisher")
public class ServiceUsersPublisherImpl implements ServiceUsersPublisher
{

    @Autowired
    private ServiceUsersPublisher usersPublisherRepository;

    @Override
    public UsersPublisherModel findByClientid(long clientId)
    {
        return usersPublisherRepository.findByClientid(clientId);
    }

    @Override
    public UsersPublisherModel findBySessionId(long sessionId)
    {
        return usersPublisherRepository.findBySessionId(sessionId);
    }

    @Override
    public UsersPublisherModel findByUserid(long userId)
   {
        return usersPublisherRepository.findByUserid(userId);
    }
}
