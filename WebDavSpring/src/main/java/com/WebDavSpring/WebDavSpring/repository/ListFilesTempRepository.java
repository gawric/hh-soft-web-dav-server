package com.WebDavSpring.WebDavSpring.repository;

import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by А д м и н on 22.10.2020.
 */
public interface ListFilesTempRepository extends JpaRepository<FileInfoModel, String> {

   // @Modifying
    //@Query(value = "INSERT INTO listfiles_temp SELECT * FROM listfiles WHERE user_id=:user_id AND row_id=:row_id" , nativeQuery = true)
   // void copyListFilesToTemp(@Param("user_id") long user_id , @Param("row_id") long row_id);

   // @Query(value = "delete from listfiles where row_id >= :b_row_id and row_id <= :e_row_id and user_id=:user_id" , nativeQuery = true)

    @Modifying
    @Query(value = "INSERT INTO listfiles_temp (row_id , filename , create_date , change_date , last_open_date , attribute , location , type , sizebyte , parent , version_update_bases , version_update_rows , user_id , change_rows , save_topc) SELECT row_id , filename , create_date , change_date , last_open_date , attribute , location , type , sizebyte , parent , version_update_bases , version_update_rows , user_id , :changeRows , save_topc FROM listfiles WHERE row_id = :row_id AND user_id=:user_id" , nativeQuery = true)
    void copySinglFilesToTemp(@Param("user_id") long user_id , @Param("row_id") long row_id ,@Param("changeRows") String changeRows);


    @Modifying
    @Query(value = "INSERT INTO listfiles_temp ( row_id , filename , create_date , change_date , last_open_date , attribute , location , type , sizebyte , parent , version_update_bases , version_update_rows , user_id , change_rows , save_topc) SELECT  row_id , filename , create_date , change_date , last_open_date , attribute , location , type , sizebyte , parent , version_update_bases , version_update_rows , user_id , :changeRows , save_topc FROM listfiles WHERE row_id >= :b_row_id and row_id <= :e_row_id  AND user_id=:user_id" , nativeQuery = true)
    void copyGroupListFilesToTemp(@Param("user_id") long user_id , @Param("b_row_id") long b_row_id , @Param("e_row_id") long e_row_id, @Param("changeRows") String changeRows);

    @Modifying
    @Query(value = "UPDATE listfiles_temp u SET u.change_rows=:changeRows  WHERE user_id=:user_id AND row_id=:row_id" , nativeQuery = true)
    void updateSinglFileChangeRows(@Param("user_id") long user_id , @Param("row_id") long row_id  , @Param("changeRows") long changeRows);

    @Modifying
    @Query(value = "SELECT * FROM listfiles_temp WHERE row_id IN(SELECT row_id FROM listfiles_temp WHERE user_id = :user_id GROUP BY row_id HAVING COUNT(row_id) > 1 ) ORDER BY row_id" , nativeQuery = true)
    List<FileInfoModel> listFilesGetTempDublicationRow_id(@Param("user_id") long user_id );


    @Query(value = "SELECT * FROM listfiles_temp WHERE row_id = :row_id and user_id = :user_id" , nativeQuery = true)
    List<FileInfoModel>  singleListFilesTemp(@Param("user_id") long user_id , @Param("row_id") long row_id);


    @Modifying
    @Query(value = "SELECT row_id FROM listfiles_temp WHERE user_id = :user_id" , nativeQuery = true)
    List<Long> listFilesGetAllRow_id(@Param("user_id") long user_id );

    @Modifying
    @Query(value = "SELECT row_id FROM listfiles_temp WHERE user_id = :user_id and version_update_rows > :versionUpdateRows " , nativeQuery = true)
    List<Long> listFilesGetAllRow_idAndVersionRows(@Param("user_id") long user_id , @Param("versionUpdateRows") long versionUpdateRows );

    //удаляет одну строку
    // @Transactional
    @Modifying
    @Query(value = "DELETE FROM listfiles_temp WHERE row_id=:row_id AND user_id=:user_id" , nativeQuery = true)
    void deleteSinglTempFiles(@Param("row_id") long row_id , @Param("user_id") long user_id);


    @Modifying
    @Query(value = "DELETE FROM listfiles_temp WHERE id=:id AND user_id=:user_id" , nativeQuery = true)
    void deleteSinglTempFilesId(@Param("id") long id , @Param("user_id") long user_id);


    @Modifying
    @Query(value = "DELETE FROM listfiles_temp WHERE user_id=:user_id" , nativeQuery = true)
    void deleteAllUserId(@Param("user_id") long user_id);





}
