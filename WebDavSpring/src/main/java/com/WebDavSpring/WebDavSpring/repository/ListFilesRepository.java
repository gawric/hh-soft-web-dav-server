package com.WebDavSpring.WebDavSpring.repository;

import com.WebDavSpring.WebDavSpring.model.FileInfoModel;

import com.WebDavSpring.WebDavSpring.model.UsersModel;
import org.apache.tomcat.jni.FileInfo;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by А д м и н on 25.09.2019.
 */
public interface ListFilesRepository extends JpaRepository<FileInfoModel, String> {


    //получить строчку по row_id
    @Query("SELECT u FROM FileInfoModel u WHERE u.row_id = :row_id and u.user_id = :user_id")
    List<FileInfoModel> findUserByRow_id(@Param("row_id") long row_id , @Param("user_id") long user_id);

    //существует запись или нет
    @Query("SELECT u.row_id,u.user_id FROM FileInfoModel u WHERE u.row_id = :row_id and u.user_id = :user_id")
    long existsRowId(@Param("row_id") long row_id , @Param("user_id") long user_id);

    //получить все строчки user_id
    @Query("SELECT u FROM FileInfoModel u WHERE u.user_id = :user_id")
    List<FileInfoModel> findUserByUser_id(@Param("user_id") long user_id);

    //Получить все строчки юзера по типу
    @Query("SELECT u FROM FileInfoModel u WHERE u.user_id = :user_id and u.type = :type1")
    List<FileInfoModel> findUserByUser_idAndType(@Param("user_id") long user_id , @Param("type1") String type1);



    @Query(value = "SELECT MAX(version_update_rows) FROM listfiles where user_id = :user_id2", nativeQuery = true)
    long getUserMaxVersionRows(@Param("user_id2") long user_id2);

    @Query(value = "SELECT MAX(version_update_bases) FROM listfiles where user_id = :user_id2", nativeQuery = true)
    long getUserMaxVersionBases(@Param("user_id2") long user_id2);


    //проверка существует таблица или нет
    @Query(value = "SELECT * FROM listfiles  WHERE user_id = ?1 LIMIT 1" , nativeQuery = true)
    FileInfoModel existsTableFileInfo(long user_id2);


    @Query(value = "SELECT MAX(ID) FROM listfiles where user_id = :user_id2", nativeQuery = true)
    int getUserMaxId(long user_id2);



    //подсчитываем кол-во записей в таблице
    @Query("SELECT COUNT(u) FROM FileInfoModel u WHERE u.user_id=:user_id2")
    long sizeAllFileInfoByUser_id(@Param("user_id2") long user_id2);

    //удаляет все строчки user_id
    @Transactional
    @Modifying
    @Query("delete from FileInfoModel b where b.user_id=:user_id")
    void deleteAllFileUserTableInfo(@Param("user_id") long user_id);


    //удаляет одну строку
    // @Transactional
    @Modifying
    @Query(value = "delete from listfiles where row_id >= :b_row_id and row_id <= :e_row_id and user_id=:user_id" , nativeQuery = true)
    void deleteSinglGroupListFiles(@Param("b_row_id") long b_row_id , @Param("e_row_id") long e_row_id  ,@Param("user_id") long user_id);


    @Modifying
    @Query(value = "SELECT * FROM listfiles WHERE row_id >= :b_row_id and row_id <= :e_row_id and user_id=:user_id" , nativeQuery = true)
    List<FileInfoModel> selectGroupListFiles(@Param("b_row_id") long b_row_id , @Param("e_row_id") long e_row_id  ,@Param("user_id") long user_id);


    //удаляет одну строку
   // @Transactional
    @Modifying
    @Query(value = "delete from listfiles where row_id=:row_id and user_id=:user_id" , nativeQuery = true)
    void deleteSinglFileUserTableInfo(@Param("row_id") long row_id , @Param("user_id") long user_id);



    @Modifying
    @Query(value = "update listfiles u set u.version_update_rows=:versionUpdateRows ,  u.version_update_bases=:versionUpdateBases  where row_id=:row_id and user_id=:user_id" , nativeQuery = true)
    void updateSinglFileUserVersionRows(@Param("row_id") long row_id , @Param("user_id") long user_id  , @Param("versionUpdateRows") long versionUpdateRows , @Param("versionUpdateBases") long versionUpdateBases );


    @Modifying
    @Query(value = "update listfiles u set u.filename=:filename , u.location=:location , u.parent=:parent , u.version_update_bases=:versionUpdateBases , u.version_update_rows=:versionUpdateRows  where row_id=:row_id and user_id=:user_id" , nativeQuery = true)
    void updateSinglFileUserTableInfo(@Param("row_id") long row_id , @Param("user_id") long user_id , @Param("filename") String  filename , @Param("location") String location  , @Param("parent") long parent , @Param("versionUpdateBases") long versionUpdateBases , @Param("versionUpdateRows") long versionUpdateRows);


    //поиск всех row_id по user_id
    @Transactional
    @Modifying
    @Query("SELECT u.row_id FROM FileInfoModel u WHERE u.user_id=:user_id2 ORDER BY u.row_id")
    List<Long>getAllRowsLongByUserId(@Param("user_id2") long user_id2);


    @Transactional
    @Modifying
    @Query(value = "SELECT row_id , user_id , version_update_rows  FROM listfiles WHERE user_id=:user_id2 AND version_update_rows > :versionUpdateRows ORDER BY row_id", nativeQuery = true)
    List<Long>getVersionUpdateRowsLongByUserId(@Param("user_id2") long user_id2 , @Param("versionUpdateRows") long versionUpdateRows);


    @Transactional
    @Modifying
    @Query(value = "UPDATE listfiles SET version_update_rows = :versionUpdateRows WHERE user_id = :user_id2  LIMIT 1", nativeQuery = true)
    void setVersionRows(@Param("user_id2") long user_id2 , @Param("versionUpdateRows") long versionUpdateRows);

    @Transactional
    @Modifying
    @Query(value = "UPDATE listfiles SET version_update_rows = :versionUpdateRows WHERE user_id = :user_id2", nativeQuery = true)
    void setVersionAllRows(@Param("user_id2") long user_id2 , @Param("versionUpdateRows") long versionUpdateRows);

    @Transactional
    @Modifying
    @Query(value = "UPDATE listfiles SET version_update_bases = :versionUpdateBases WHERE user_id = :user_id2", nativeQuery = true)
    void setVersionAllRowsBases(@Param("user_id2") long user_id2 , @Param("versionUpdateBases") long versionUpdateBases);




}
