package com.WebDavSpring.WebDavSpring.repository;

import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.UsersPublisherModel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by А д м и н on 07.04.2020.
 */
public interface UsersPublisherRepository  extends JpaRepository<UsersPublisherModel, String>
{
    UsersPublisherModel findByClientid(long clientId);
    UsersPublisherModel findBySessionid(long sessionId);
    UsersPublisherModel findByUserid(long userId);
}
