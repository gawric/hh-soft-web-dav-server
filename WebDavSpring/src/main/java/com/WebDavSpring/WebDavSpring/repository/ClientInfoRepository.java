package com.WebDavSpring.WebDavSpring.repository;

import com.WebDavSpring.WebDavSpring.model.ClientInfoSystemModel;
import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.ConnectedModel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by А д м и н on 21.08.2020.
 */

public interface ClientInfoRepository extends JpaRepository<ClientInfoSystemModel, String> {
    ClientInfoSystemModel findByMacnetwork(String macClient);
}
