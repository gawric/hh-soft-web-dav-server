package com.WebDavSpring.WebDavSpring.repository;


import com.WebDavSpring.WebDavSpring.model.WebServerLinkInfoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by А д м и н on 25.08.2020.
 */
public interface WebServerLinkInfoRepository extends JpaRepository<WebServerLinkInfoModel, String> {

    WebServerLinkInfoModel findByToken(String token);
    WebServerLinkInfoModel findByListFilesId(long listFilesId);

    @Modifying
    @Query(value = "delete from webserverlinkinfo where list_files_id=:row_id and user_id=:user_id" , nativeQuery = true)
    void deleteSinglToken(@Param("row_id") long row_id , @Param("user_id") long user_id);
}
