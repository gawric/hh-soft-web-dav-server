package com.WebDavSpring.WebDavSpring.repository;


import com.WebDavSpring.WebDavSpring.model.UsersModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;




public interface UsersRepository extends CrudRepository<UsersModel, String>
{
    UsersModel findByUsername(String username);
    boolean existsByUsername(String username);
}