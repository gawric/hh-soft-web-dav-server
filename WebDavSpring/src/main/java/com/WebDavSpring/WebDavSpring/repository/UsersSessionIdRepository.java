package com.WebDavSpring.WebDavSpring.repository;

import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.UsersSessionIdModel;
import org.apache.activemq.command.SessionId;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by А д м и н on 07.04.2020.
 */
public interface UsersSessionIdRepository  extends CrudRepository<UsersSessionIdModel, String>
{
   // List<UsersSessionIdModel> findByUserid(int userid);

    //удаляет все строчки user_id
    @Transactional
    @Modifying
    @Query("delete from UsersSessionIdModel b where b.userid=:userid")
    void deleteAllSessionUsers(@Param("userid") int userid);


    //удаляет только нужную строчку
    @Transactional
    @Modifying
    @Query("delete from UsersSessionIdModel b where b.sessionid=:sessionid")
    void deleteConnectionByClientId(@Param("sessionid") String sessionid);
}
