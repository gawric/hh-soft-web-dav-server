package com.WebDavSpring.WebDavSpring.repository;


import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.usersConnectionModel.ConnectedModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by А д м и н on 07.04.2020.
 */

public interface ConnectedRepository extends JpaRepository<ConnectedModel, String>
{
    ConnectedModel findByUsername(String username);
    List<ConnectedModel> findByUsersid(int id);

    ConnectedModel findBySessionid(String sessionid);
    ConnectedModel findByClientid(String clientid);

    @Transactional
    @Modifying
    @Query("delete from ConnectedModel b where b.usersid=:usersid")
    void deleteAllConnectionUsers(@Param("usersid") int usersid);



    @Transactional
    @Modifying
    @Query("delete from ConnectedModel b where b.clientid=:clientid")
    void deleteConnectionByClientId(@Param("clientid") String clientid);



    @Transactional
    @Modifying
    @Query("delete from ConnectedModel b where b.sessionid=:sessionid")
    void deleteConnectionBySessionId(@Param("sessionid") String sessionid);


    @Query("SELECT b FROM ConnectedModel b WHERE b.publisher = true AND  b.usersid = :usersid")
    ConnectedModel findPublisher(@Param("usersid") int usersid);
}
