package com.WebDavSpring.WebDavSpring.repository;

import com.WebDavSpring.WebDavSpring.model.ErrorAlertModel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by А д м и н on 09.07.2020.
 */
public interface ErrorAlertRepository extends JpaRepository<ErrorAlertModel, String> {
}
