package com.WebDavSpring.WebDavSpring.model;

import javax.persistence.*;

/**
 * Created by А д м и н on 25.08.2020.
 */
@Entity
@Table(name="webserverlinkinfo")
public class WebServerLinkInfoModel {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    private String token;
    private String username;
    private String filesystemlink;
    private String type;
    private String fileName;
    private long listFilesId;
    private long user_id;



    public long getListFilesId() {
        return listFilesId;
    }

    public void setListFilesId(long listFilesId) {
        this.listFilesId = listFilesId;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFilesystemlink() {
        return filesystemlink;
    }

    public void setFilesystemlink(String filesystemlink) {
        this.filesystemlink = filesystemlink;
    }



    public void setListFilesId(int listFilesId) {
        this.listFilesId = listFilesId;
    }
}
