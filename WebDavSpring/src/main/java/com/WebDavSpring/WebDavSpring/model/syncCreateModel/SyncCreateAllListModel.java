package com.WebDavSpring.WebDavSpring.model.syncCreateModel;

import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.ActiveMq.UpdateSqlCollectionUsers;
import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.TrainingSendMessage;

import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceListFiles;

import java.util.PriorityQueue;

/**
 * Created by А д м и н on 07.05.2020.
 */
public class SyncCreateAllListModel {

    private FileInfoObjectTopic modelObjectClient;
    private String userRequestParties;
    private String[] receivedConnectionID;
    private FileInfoModel[] clientListFileInfo;
    private UsersModel userModelServer;
    private PriorityQueue<Integer> myPriorityQueue;
    private UpdateSqlCollectionUsers updateSql;
    private SendMessage message;
    private ServiceListFiles mysqlServiceFileList;


    public TrainingSendMessage getTrainingSendMessage() {
        return trainingSendMessage;
    }

    public void setTrainingSendMessage(TrainingSendMessage trainingSendMessage) {
        this.trainingSendMessage = trainingSendMessage;
    }

    private TrainingSendMessage trainingSendMessage;






    public ServiceListFiles getMysqlServiceFileList() {
        return mysqlServiceFileList;
    }

    public void setMysqlServiceFileList(ServiceListFiles mysqlServiceFileList) {
        this.mysqlServiceFileList = mysqlServiceFileList;
    }

    public SendMessage getMessage() {
        return message;
    }

    public void setMessage(SendMessage message) {
        this.message = message;
    }

    public FileInfoObjectTopic getModelObjectClient() {
        return modelObjectClient;
    }

    public void setModelObjectClient(FileInfoObjectTopic modelObjectClient) {
        this.modelObjectClient = modelObjectClient;
    }

    public String getUserRequestParties() {
        return userRequestParties;
    }

    public void setUserRequestParties(String userRequestParties) {
        this.userRequestParties = userRequestParties;
    }

    public String[] getReceivedConnectionID() {
        return receivedConnectionID;
    }

    public void setReceivedConnectionID(String[] receivedConnectionID) {
        this.receivedConnectionID = receivedConnectionID;
    }

    public FileInfoModel[] getClientListFileInfo() {
        return clientListFileInfo;
    }

    public void setClientListFileInfo(FileInfoModel[] clientListFileInfo) {
        this.clientListFileInfo = clientListFileInfo;
    }

    public UsersModel getUserModelServer() {
        return userModelServer;
    }

    public void setUserModelServer(UsersModel userModelServer) {
        this.userModelServer = userModelServer;
    }

    public PriorityQueue<Integer> getMyPriorityQueue() {
        return myPriorityQueue;
    }

    public void setMyPriorityQueue(PriorityQueue<Integer> myPriorityQueue) {
        this.myPriorityQueue = myPriorityQueue;
    }

    public UpdateSqlCollectionUsers getUpdateSql() {
        return updateSql;
    }

    public void setUpdateSql(UpdateSqlCollectionUsers updateSql) {
        this.updateSql = updateSql;
    }



}
