package com.WebDavSpring.WebDavSpring.model.wokingModel;


import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.ActiveMq.UpdateSqlCollectionUsers;
import com.WebDavSpring.ActiveMq.usersconnected.UserProperties;
import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.TrainingSendMessage;

import com.WebDavSpring.MailSender.JavaMail;
import com.WebDavSpring.WebDavSpring.services.Interface.*;
import com.WebDavSpring.WebDavSpring.services.ServiceAuthUserImpl;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.PriorityQueue;

/**
 * Created by А д м и н on 06.05.2020.
 */
public class SyncModel {

    private UpdateSqlCollectionUsers updateSql;
    private SendMessage message;
    private ServiceListFiles mysqlServiceFileList;
    private PriorityQueue<Integer> myPriorityQueue;
    private java.util.PriorityQueue<Integer> myPriorityDeleteQueue;
    private PriorityQueue<Integer> myPriorityPasteQueue;
    private PriorityQueue<Integer> myPrioritySinglUpdateQueue;
    private ServiceUsersConnected serviceConnected;
    private ServiceErrorAlert serviceErrorAlert;
    private ServiceClientInfo serviceClientInfo;
    private JavaMail javaMail;
    private ServiceWebServerLinkInfo serviceWebServerLinkInfo;
    private long lastVersionUpdateBases = 0;
    private ServiceAuthUser serviceAuthUser;

    public long getLastVersionUpdateRows() {
        return lastVersionUpdateBases;
    }

    public synchronized  void setLastVersionUpdateRows(long lastVersionUpdateRows) {
        this.lastVersionUpdateBases = lastVersionUpdateRows;
    }



    public ServiceAuthUser getServiceAuthUser() {
        return serviceAuthUser;
    }

    public void setServiceAuthUser(ServiceAuthUser serviceAuthUser) {
        this.serviceAuthUser = serviceAuthUser;
    }



    public int getWebDavServerPort() {
        return webDavServerPort;
    }

    public void setWebDavServerPort(int webDavServerPort) {
        this.webDavServerPort = webDavServerPort;
    }

    private int webDavServerPort;

    public ServiceWebServerLinkInfo getServiceWebServerLinkInfo() {
        return serviceWebServerLinkInfo;
    }

    public void setServiceWebServerLinkInfo(ServiceWebServerLinkInfo serviceWebServerLinkInfo) {
        this.serviceWebServerLinkInfo = serviceWebServerLinkInfo;
    }

    public JavaMail getJavaMail() {
        return javaMail;
    }

    public void setJavaMail(JavaMail javaMail) {
        this.javaMail = javaMail;
    }


    private UserProperties userProperties;
    private  ThreadPoolTaskExecutor task;

    public ServiceClientInfo getServiceClientInfo() {
        return serviceClientInfo;
    }

    public void setServiceClientInfo(ServiceClientInfo serviceClientInfo) {
        this.serviceClientInfo = serviceClientInfo;
    }


    public UpdateSqlCollectionUsers getUpdateSql() {
        return updateSql;
    }

    public void setUpdateSql(UpdateSqlCollectionUsers updateSql) {
        this.updateSql = updateSql;
    }

    public SendMessage getMessage() {
        return message;
    }

    public void setMessage(SendMessage message) {
        this.message = message;
    }

    public ServiceListFiles getMysqlServiceFileList() {
        return mysqlServiceFileList;
    }

    public void setMysqlServiceFileList(ServiceListFiles mysqlServiceFileList) {
        this.mysqlServiceFileList = mysqlServiceFileList;
    }

    public PriorityQueue<Integer> getMyPriorityQueue() {
        return myPriorityQueue;
    }

    public void setMyPriorityQueue(PriorityQueue<Integer> myPriorityQueue) {
        this.myPriorityQueue = myPriorityQueue;
    }

    public PriorityQueue<Integer> getMyPriorityDeleteQueue() {
        return myPriorityDeleteQueue;
    }

    public void setMyPriorityDeleteQueue(PriorityQueue<Integer> myPriorityDeleteQueue) {
        this.myPriorityDeleteQueue = myPriorityDeleteQueue;
    }

    public PriorityQueue<Integer> getMyPriorityPasteQueue() {
        return myPriorityPasteQueue;
    }

    public void setMyPriorityPasteQueue(PriorityQueue<Integer> myPriorityPasteQueue) {
        this.myPriorityPasteQueue = myPriorityPasteQueue;
    }

    public PriorityQueue<Integer> getMyPrioritySinglUpdateQueue() {
        return myPrioritySinglUpdateQueue;
    }

    public void setMyPrioritySinglUpdateQueue(PriorityQueue<Integer> myPrioritySinglUpdateQueue) {
        this.myPrioritySinglUpdateQueue = myPrioritySinglUpdateQueue;
    }

    public ServiceUsersConnected getServiceConnected() {
        return serviceConnected;
    }

    public void setServiceConnected(ServiceUsersConnected serviceConnected) {
        this.serviceConnected = serviceConnected;
    }


    public UserProperties getUserProperties() {
        return userProperties;
    }

    public void setUserProperties(UserProperties userProperties) {
        this.userProperties = userProperties;
    }

    public ThreadPoolTaskExecutor getTask() {
        return task;
    }

    public void setTask(ThreadPoolTaskExecutor task) {
        this.task = task;
    }

    public ServiceErrorAlert getServiceErrorAlert() {
        return serviceErrorAlert;
    }

    public void setServiceErrorAlert(ServiceErrorAlert serviceErrorAlert) {
        this.serviceErrorAlert = serviceErrorAlert;
    }




    public TrainingSendMessage getTrainingSendMessage() {
        return trainingSendMessage;
    }

    public void setTrainingSendMessage(TrainingSendMessage trainingSendMessage) {
        this.trainingSendMessage = trainingSendMessage;
    }

    private TrainingSendMessage trainingSendMessage;




}
