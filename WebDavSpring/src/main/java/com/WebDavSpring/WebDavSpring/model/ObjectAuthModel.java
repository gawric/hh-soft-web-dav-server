package com.WebDavSpring.WebDavSpring.model;

import com.WebDavSpring.WebDavSpring.services.Interface.ServiceAuthUser;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUser;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by А д м и н on 01.04.2020.
 */
public class ObjectAuthModel
{
    public ServiceUser getRepository()
    {
        return repository;
    }

    public void setRepository(ServiceUser repository)
    {
        this.repository = repository;
    }

    public ServiceAuthUser getAuthUser() {
        return authUser;
    }

    public void setAuthUser(ServiceAuthUser authUser)
    {
        this.authUser = authUser;
    }

    private ServiceUser repository;
    private ServiceAuthUser authUser;
}
