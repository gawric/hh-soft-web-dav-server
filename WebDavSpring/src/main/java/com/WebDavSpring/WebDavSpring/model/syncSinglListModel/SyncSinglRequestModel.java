package com.WebDavSpring.WebDavSpring.model.syncSinglListModel;

import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;

/**
 * Created by А д м и н on 07.05.2020.
 */
public class SyncSinglRequestModel {

    private String userRequestParties;
    private UsersModel userModelServer;
    private String[] receivedConnectionID;
    private FileInfoModel[] clientListFileInfo;
    private FileInfoObjectTopic modelObjectClient;



    public String getUserRequestParties() {
        return userRequestParties;
    }

    public void setUserRequestParties(String userRequestParties) {
        this.userRequestParties = userRequestParties;
    }

    public UsersModel getUserModelServer() {
        return userModelServer;
    }

    public void setUserModelServer(UsersModel userModelServer) {
        this.userModelServer = userModelServer;
    }

    public String[] getReceivedConnectionID() {
        return receivedConnectionID;
    }

    public void setReceivedConnectionID(String[] receivedConnectionID) {
        this.receivedConnectionID = receivedConnectionID;
    }

    public FileInfoModel[] getClientListFileInfo() {
        return clientListFileInfo;
    }

    public void setClientListFileInfo(FileInfoModel[] clientListFileInfo) {
        this.clientListFileInfo = clientListFileInfo;
    }

    public FileInfoObjectTopic getModelObjectClient() {
        return modelObjectClient;
    }

    public void setModelObjectClient(FileInfoObjectTopic modelObjectClient) {
        this.modelObjectClient = modelObjectClient;
    }


}
