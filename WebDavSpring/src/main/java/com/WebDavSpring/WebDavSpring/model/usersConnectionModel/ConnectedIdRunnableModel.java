package com.WebDavSpring.WebDavSpring.model.usersConnectionModel;

/**
 * Created by А д м и н on 14.04.2020.
 */
public class ConnectedIdRunnableModel implements Cloneable {

    private String serverConnectedId;
    private int userid;

    public String getServerConnectedId() {
        return serverConnectedId;
    }

    public synchronized void setServerConnectedId(String serverConnectedId) {
        this.serverConnectedId = serverConnectedId;
    }

    public int getUserid() {
        return userid;
    }



    public synchronized void setUserid(int userid) {
        this.userid = userid;
    }

    public Object clone() throws
            CloneNotSupportedException {
        return super.clone();

    }
}