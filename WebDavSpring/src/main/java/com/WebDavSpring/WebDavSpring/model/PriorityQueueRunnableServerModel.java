package com.WebDavSpring.WebDavSpring.model;

/**
 * Created by А д м и н on 18.05.2020.
 */
public class PriorityQueueRunnableServerModel {

    private int id;
    private String parties;
    private FileInfoObjectTopic infoObjectTopic;

    public PriorityQueueRunnableServerModel(int id , String parties , FileInfoObjectTopic infoObjectTopic){
        this.id = id;
        this.parties = parties;
        this.infoObjectTopic = infoObjectTopic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getParties() {
        return parties;
    }

    public void setParties(String parties) {
        this.parties = parties;
    }

    public FileInfoObjectTopic getInfoObjectTopic() {
        return infoObjectTopic;
    }

    public void setInfoObjectTopic(FileInfoObjectTopic infoObjectTopic) {
        this.infoObjectTopic = infoObjectTopic;
    }


}
