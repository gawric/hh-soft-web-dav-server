package com.WebDavSpring.WebDavSpring.model.usersConnectionModel;

import javax.persistence.*;

/**
 * Created by А д м и н on 07.04.2020.
 */
@Entity
@Table(name="connected_users")
public class ConnectedModel
{

    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    private String username;

    private String clientid;

    //здесь возникает ошибка если ставить int т.к она не имеет нулового значения при инициализации полей
    private Integer usersid;

    //здесь возникает ошибка если ставить int т.к она не имеет нулового значения при инициализации полей
    private String sessionid;

    //он издатель новых сообщений или нет
    private boolean publisher;


    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getClientid() {
        return clientid;
    }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }

    public Integer getUsersid() {
        return usersid;
    }

    public void setUsersid(Integer usersid) {
        this.usersid = usersid;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public boolean isPublisher() {
        return publisher;
    }

    public void setPublisher(boolean publisher) {
        this.publisher = publisher;
    }
}
