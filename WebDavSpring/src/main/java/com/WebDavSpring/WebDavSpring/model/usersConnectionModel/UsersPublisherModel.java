package com.WebDavSpring.WebDavSpring.model.usersConnectionModel;

import javax.persistence.*;


@Entity
@Table(name="publisher_users")
public class UsersPublisherModel
{

    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    private boolean isWorking;
    private String sessionid;
    private String clientid;
    private Integer userid;
    private String username;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isWorking() {
       return isWorking;
    }

    public void setWorking(boolean working) {
        isWorking = working;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getClientid() {
        return clientid;
   }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }

 public int getUserid() {
    return userid;
 }

    public void setUserid(int userid) {
    this.userid = userid;
 }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


}
