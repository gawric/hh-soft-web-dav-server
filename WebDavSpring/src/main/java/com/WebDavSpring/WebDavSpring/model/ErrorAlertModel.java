package com.WebDavSpring.WebDavSpring.model;

import javax.persistence.*;

/**
 * Created by А д м и н on 09.07.2020.
 */
@Entity
@Table(name="erroralert")
public class ErrorAlertModel {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    private String username;
    private String errortext;
    @Lob
    @Column(name = "textblock", columnDefinition="BLOB")
    private String textblock;
    private String errorDate;
    private int userid;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getErrortext() {
        return errortext;
    }

    public void setErrortext(String errortext) {
        this.errortext = errortext;
    }

    public String getTextblock() {
        return textblock;
    }

    public void setTextblock(String textblock) {
        this.textblock = textblock;
    }

    public String getErrorDate() {
        return errorDate;
    }

    public void setErrorDate(String errorDate) {
        this.errorDate = errorDate;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }



}
