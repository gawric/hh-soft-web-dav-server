package com.WebDavSpring.WebDavSpring.model;

import javax.persistence.*;

/**
 * Created by А д м и н on 21.08.2020.
 */
@Entity
@Table(name="clientinfo")
public class ClientInfoSystemModel {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    private String  username;
    private String  osVerison;
    public  String  recordingtime;
    private String  osUserName;
    private String  macnetwork;
    private String  ipLocal;
    private String  ipNetwork;


    public String getRecordingtime() {
        return recordingtime;
    }

    public void setRecordingtime(String recordingtime) {
        this.recordingtime = recordingtime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOsVerison() {
        return osVerison;
    }

    public void setOsVerison(String osVerison) {
        this.osVerison = osVerison;
    }

    public String getOsUserName() {
        return osUserName;
    }

    public void setOsUserName(String osUserName) {
        this.osUserName = osUserName;
    }

    public String getMacnetwork() {
        return macnetwork;
    }

    public void setMacnetwork(String macnetwork) {
        this.macnetwork = macnetwork;
    }

    public String getIpLocal() {
        return ipLocal;
    }

    public void setIpLocal(String ipLocal) {
        this.ipLocal = ipLocal;
    }

    public String getIpNetwork() {
        return ipNetwork;
    }

    public void setIpNetwork(String ipNetwork) {
        this.ipNetwork = ipNetwork;
    }

}
