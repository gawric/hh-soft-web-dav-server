package com.WebDavSpring.WebDavSpring.model.webDavModel;

import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.DavServletRequest;
import org.apache.jackrabbit.webdav.DavServletResponse;

/**
 * Created by А д м и н on 01.04.2020.
 */
public class WebDavLocator {

    public DavResourceLocator getLocator() {
        return locator;
    }

    public void setLocator(DavResourceLocator locator) {
        this.locator = locator;
    }

    public DavServletRequest getRequest() {
        return request;
    }

    public void setRequest(DavServletRequest request) {
        this.request = request;
    }

    public DavServletResponse getResponse() {
        return response;
    }

    public void setResponse(DavServletResponse response) {
        this.response = response;
    }

    private DavResourceLocator locator ;
    private DavServletRequest request;
    private DavServletResponse response;
}
