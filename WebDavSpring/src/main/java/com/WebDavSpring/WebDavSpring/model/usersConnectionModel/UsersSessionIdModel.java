package com.WebDavSpring.WebDavSpring.model.usersConnectionModel;

import javax.persistence.*;

/**
 * Created by А д м и н on 07.04.2020.
 */
@Entity
@Table(name="sessionid_users")
public class UsersSessionIdModel
{

    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    private String sessionid;

    private Integer userid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

}
