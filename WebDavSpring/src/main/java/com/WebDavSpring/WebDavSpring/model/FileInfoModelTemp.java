package com.WebDavSpring.WebDavSpring.model;



import javax.persistence.*;

/**
 * Created by А д м и н on 09.09.2019.
 */

@Entity
@Table(name="listfiles_temp")
public class FileInfoModelTemp {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    private long row_id;
    private String  filename;
    private String  createDate;
    private String  changeDate;
    private String  lastOpenDate;
    private String  attribute;

    @Lob
    @Column(name = "location", columnDefinition="BLOB")
    private String  location;

    private String type;
    private long sizebyte;
    private long parent;
    private long versionUpdateBases;
    private long versionUpdateRows;
    private long user_id;
    private String changeRows;
    private int saveTopc;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRow_id() {
        return row_id;
    }

    public void setRow_id(long row_id) {
        this.row_id = row_id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(String changeDate) {
        this.changeDate = changeDate;
    }

    public String getLastOpenDate() {
        return lastOpenDate;
    }

    public void setLastOpenDate(String lastOpenDate) {
        this.lastOpenDate = lastOpenDate;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getSizebyte() {
        return sizebyte;
    }

    public void setSizebyte(long sizebyte) {
        this.sizebyte = sizebyte;
    }

    public long getParent() {
        return parent;
    }

    public void setParent(long parent) {
        this.parent = parent;
    }

    public long getVersionUpdateBases() {
        return versionUpdateBases;
    }

    public void setVersionUpdateBases(long versionUpdateBases) {
        this.versionUpdateBases = versionUpdateBases;
    }

    public long getVersionUpdateRows() {
        return versionUpdateRows;
    }

    public void setVersionUpdateRows(long versionUpdateRows) {
        this.versionUpdateRows = versionUpdateRows;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getChangeRows() {
        return changeRows;
    }

    public void setChangeRows(String changeRows) {
        this.changeRows = changeRows;
    }

    public int getSaveTopc() {
        return saveTopc;
    }

    public void setSaveTopc(int saveTopc) {
        this.saveTopc = saveTopc;
    }


}
