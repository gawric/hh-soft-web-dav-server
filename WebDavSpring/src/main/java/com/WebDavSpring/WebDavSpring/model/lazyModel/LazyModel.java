package com.WebDavSpring.WebDavSpring.model.lazyModel;

import com.WebDavSpring.ActiveMq.warehouse.warehouseRequestActiveMq.sendActiveMq.SendMessage;
import com.WebDavSpring.ActiveMq.UpdateSqlCollectionUsers;
import com.WebDavSpring.ActiveMq.usersconnected.UserProperties;
import com.WebDavSpring.WebDavSpring.model.FileInfoModel;
import com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic;
import com.WebDavSpring.WebDavSpring.model.UsersModel;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceListFiles;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUsersConnected;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.PriorityQueue;

/**
 * Created by А д м и н on 06.05.2020.
 */
public class LazyModel {


    private String userRequestParties;
    private UsersModel userModelServer;
    private ServiceListFiles mysqlServiceFileList;
    private FileInfoModel[] clientListFileInfo;
    private FileInfoObjectTopic modelObjectClient;
    private SendMessage message;
    private PriorityQueue<Integer> myPrioritySinglUpdateQueue;

    private String[] receivedConnectionID;
    private ThreadPoolTaskExecutor task;
    private UserProperties userProperties;




    public String getUserRequestParties() {
        return userRequestParties;
    }

    public void setUserRequestParties(String userRequestParties) {
        this.userRequestParties = userRequestParties;
    }

    public UsersModel getUserModelServer() {
        return userModelServer;
    }

    public void setUserModelServer(UsersModel userModelServer) {
        this.userModelServer = userModelServer;
    }

    public ServiceListFiles getMysqlServiceFileList() {
        return mysqlServiceFileList;
    }

    public void setMysqlServiceFileList(ServiceListFiles mysqlServiceFileList) {
        this.mysqlServiceFileList = mysqlServiceFileList;
    }

    public FileInfoModel[] getClientListFileInfo() {
        return clientListFileInfo;
    }

    public void setClientListFileInfo(FileInfoModel[] clientListFileInfo) {
        this.clientListFileInfo = clientListFileInfo;
    }

    public FileInfoObjectTopic getModelObjectClient() {
        return modelObjectClient;
    }

    public void setModelObjectClient(FileInfoObjectTopic modelObjectClient) {
        this.modelObjectClient = modelObjectClient;
    }

    public SendMessage getMessage() {
        return message;
    }

    public void setMessage(SendMessage message) {
        this.message = message;
    }

    public PriorityQueue<Integer> getMyPrioritySinglUpdateQueue() {
        return myPrioritySinglUpdateQueue;
    }

    public void setMyPrioritySinglUpdateQueue(PriorityQueue<Integer> myPrioritySinglUpdateQueue) {
        this.myPrioritySinglUpdateQueue = myPrioritySinglUpdateQueue;
    }





    public String[] getReceivedConnectionID() {
        return receivedConnectionID;
    }

    public void setReceivedConnectionID(String[] receivedConnectionID) {
        this.receivedConnectionID = receivedConnectionID;
    }

    public ThreadPoolTaskExecutor getTask() {
        return task;
    }

    public void setTask(ThreadPoolTaskExecutor task) {
        this.task = task;
    }



    public UserProperties getUserProperties() {
        return userProperties;
    }

    public void setUserProperties(UserProperties userProperties) {
        this.userProperties = userProperties;
    }



    public ServiceUsersConnected getServiceConnected() {
        return serviceConnected;
    }

    public void setServiceConnected(ServiceUsersConnected serviceConnected) {
        this.serviceConnected = serviceConnected;
    }

    private ServiceUsersConnected serviceConnected;

    public SendMessage getMessageJson() {
        return messageJson;
    }

    public void setMessageJson(SendMessage messageJson) {
        this.messageJson = messageJson;
    }

    private SendMessage messageJson;

    public UpdateSqlCollectionUsers getUpdateSql() {
        return updateSql;
    }

    public void setUpdateSql(UpdateSqlCollectionUsers updateSql) {
        this.updateSql = updateSql;
    }

    private UpdateSqlCollectionUsers updateSql;
}
