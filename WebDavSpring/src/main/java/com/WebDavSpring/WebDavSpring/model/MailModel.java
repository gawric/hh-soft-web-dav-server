package com.WebDavSpring.WebDavSpring.model;

/**
 * Created by А д м и н on 09.07.2020.
 */
public class MailModel {

    private String Namequest;
    private String Emailguest;
    private String Phonequest;
    private String Subject;
    private String Text;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    private String  Status;


    public String getNamequest() {
        return Namequest;
    }

    public void setNamequest(String namequest) {
        Namequest = namequest;
    }

    public String getEmailguest() {
        return Emailguest;
    }

    public void setEmailguest(String emailguest) {
        Emailguest = emailguest;
    }

    public String getPhonequest() {
        return Phonequest;
    }

    public void setPhonequest(String phonequest) {
        Phonequest = phonequest;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

}
