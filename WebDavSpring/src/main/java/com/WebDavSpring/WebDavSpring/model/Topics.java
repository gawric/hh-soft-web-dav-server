package com.WebDavSpring.WebDavSpring.model;

import java.util.List;

/**
 * Created by А д м и н on 04.05.2020.
 */
public class Topics {


    public List<TopicXmlModel> getTopic() {
        return topic;
    }

    public void setTopic(List<TopicXmlModel> topic) {
        this.topic = topic;
    }

    private List<TopicXmlModel> topic;
}
