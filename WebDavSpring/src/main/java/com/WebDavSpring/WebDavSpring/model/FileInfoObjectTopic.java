package com.WebDavSpring.WebDavSpring.model;

import java.util.List;

/**
 * Created by А д м и н on 19.09.2019.
 */
public class FileInfoObjectTopic implements Cloneable  {

    private String username;
    private String[] textmessage;
    private FileInfoModel[] listFileInfo;
    private String[] connectionId;



    private String webdavnamespace;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String[] getTextmessage() {
        return textmessage;
    }

    public void setTextmessage(String[] textmessage) {
        this.textmessage = textmessage;
    }

    public FileInfoModel[] getListFileInfo() {
        return listFileInfo;
    }

    public void setListFileInfo(FileInfoModel[] listFileInfo) {
        this.listFileInfo = listFileInfo;
    }

    public String[] getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String[] connectionId) {
        this.connectionId = connectionId;
    }

    public String getWebdavnamespace() {
        return webdavnamespace;
    }

    public void setWebdavnamespace(String webdavnamespace) {
        this.webdavnamespace = webdavnamespace;
    }

    public Object clone()
    {
        try
        {
            return super.clone();
        } catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
