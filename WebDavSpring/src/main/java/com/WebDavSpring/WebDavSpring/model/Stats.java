package com.WebDavSpring.WebDavSpring.model;

/**
 * Created by А д м и н on 04.05.2020.
 */
public class Stats {
    private String size;
    private String consumerCount;
    private String enqueueCount;

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getConsumerCount() {
        return consumerCount;
    }

    public void setConsumerCount(String consumerCount) {
        this.consumerCount = consumerCount;
    }

    public String getEnqueueCount() {
        return enqueueCount;
    }

    public void setEnqueueCount(String enqueueCount) {
        this.enqueueCount = enqueueCount;
    }

    public String getDequeueCount() {
        return dequeueCount;
    }

    public void setDequeueCount(String dequeueCount) {
        this.dequeueCount = dequeueCount;
    }

    private String  dequeueCount;
}
