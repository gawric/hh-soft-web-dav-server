package com.WebDavSpring.WebDavSpring.WebDav.Server;

/**
 * Created by А д м и н on 07.08.2019.
 */

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

class WebDavServerModule {

    private static final int MAX_PENDING_REQUESTS = 400;
    private static final int MAX_THREADS = 100;
    private static final int THREAD_IDLE_SECONDS = 60;
    private static final String ROOT_PATH = "/";
    private static final String PKCS12_KEYSTORE_TYPE = "pkcs12";
    private static final String[] WHITELISTED_CIPHER_SUITES = { //
            "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384", "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", // EC (RSA keys) + GCM + SHA2
            "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384", "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256", // DHE (RSA keys) + GCM + SHA2
            "TLS_DHE_RSA_WITH_AES_256_CBC_SHA", "TLS_DHE_RSA_WITH_AES_128_CBC_SHA", // DHE (RSA keys) + CBC + SHA
            "TLS_RSA_WITH_AES_256_GCM_SHA384", "TLS_RSA_WITH_AES_128_GCM_SHA256", // RSA + GCM + SHA2
            "TLS_RSA_WITH_AES_256_CBC_SHA256", "TLS_RSA_WITH_AES_128_CBC_SHA256", // RSA + CBC + SHA2
            "TLS_RSA_WITH_AES_256_CBC_SHA", "TLS_RSA_WITH_AES_128_CBC_SHA" // RSA + CBC + SHA1
    };

    private final Optional<String> pkcs12KeystorePath;
    private final Optional<String> pkcs12KeystorePassword;

    WebDavServerModule(Optional<String> pkcs12KeystorePath, Optional<String> pkcs12KeystorePassword) {
        this.pkcs12KeystorePath = pkcs12KeystorePath;
        this.pkcs12KeystorePassword = pkcs12KeystorePassword;
    }


    ExecutorService provideExecutorService() {
        // set core pool size = MAX_THREADS and allow coreThreadTimeOut to enforce spawning threads till the maximum even if the queue is not full
        BlockingQueue<Runnable> queue = new LinkedBlockingQueue<>(MAX_PENDING_REQUESTS);
        AtomicInteger threadNum = new AtomicInteger(1);
        ThreadPoolExecutor executor = new ThreadPoolExecutor(MAX_THREADS, MAX_THREADS, THREAD_IDLE_SECONDS, TimeUnit.SECONDS, queue, r -> {
            Thread t = new Thread(r, String.format("Server thread %03d", threadNum.getAndIncrement()));
            t.setDaemon(true);
            return t;
        });
        executor.allowCoreThreadTimeOut(true);
        return executor;
    }

//
//    Server provideServer(ExecutorService executor, ContextHandlerCollection servletCollection)
//    {
//        ThreadPool threadPool = new ExecutorThreadPool(executor);
//        Server Server = new Server(threadPool);
//        Server.unmanage(threadPool); // prevent threadpool from being shutdown when stopping the Server
//        Server.setHandler(servletCollection);
//        return Server;
//    }
//
//
//    Optional<SslContextFactory> provideSslContextFactory() {
//        if (pkcs12KeystorePath.isPresent() && pkcs12KeystorePassword.isPresent()) {
//            SslContextFactory sslContextFactory = new SslContextFactory();
//            sslContextFactory.setKeyStorePath(pkcs12KeystorePath.get());
//            sslContextFactory.setKeyStorePassword(pkcs12KeystorePassword.get());
//            sslContextFactory.setKeyStoreType(PKCS12_KEYSTORE_TYPE);
//            sslContextFactory.setIncludeCipherSuites(WHITELISTED_CIPHER_SUITES);
//            return Optional.of(sslContextFactory);
//        } else {
//            return Optional.empty();
//        }
//    }
//
//
//    ServerConnector provideServerConnector(Server Server, Optional<SslContextFactory> sslContextFactory) {
//        final ServerConnector connector;
//        if (sslContextFactory.isPresent()) {
//            connector = new ServerConnector(Server, sslContextFactory.get());
//        } else {
//            connector = new ServerConnector(Server);
//        }
//        Server.setConnectors(new Connector[] {connector});
//        return connector;
//    }
//
////
//    ContextHandlerCollection provideContextHandlerCollection(@CatchAll ServletContextHandler catchAllServletHandler) {
//        ContextHandlerCollection collection = new ContextHandlerCollection();
//        collection.addHandler(catchAllServletHandler);
//        return collection;
//    }
////
////
////похоже есть сервелет заглушка если юзер попадает на root страницу его выкидывает с ошибкой 404
//    ServletContextHandler provideServletContextHandler(DefaultServlet servlet) {
//        final ServletContextHandler servletContext = new ServletContextHandler(null, ROOT_PATH, ServletContextHandler.NO_SESSIONS);
//        final ServletHolder servletHolder = new ServletHolder(ROOT_PATH, servlet);
//        servletContext.addServlet(servletHolder, ROOT_PATH);
//        return servletContext;
//    }


    Collection<String> provideContextPaths() {
        return new HashSet<>();
    }


}