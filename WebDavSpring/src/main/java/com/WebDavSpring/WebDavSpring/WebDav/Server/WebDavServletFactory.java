package com.WebDavSpring.WebDavSpring.WebDav.Server;

/**
 * Created by А д м и н on 07.08.2019.
 */

import com.WebDavSpring.WebDavSpring.WebDav.Server.servlet.WebDavServletComponent;
import com.WebDavSpring.WebDavSpring.WebDav.Server.servlet.WebDavServletModule;

import java.nio.file.Path;
import java.util.Collection;

//import javax.inject.Inject;
//import javax.inject.Singleton;

//import org.cryptomator.frontend.webdav.WebDavServerModule.ContextPaths;
//import org.cryptomator.frontend.webdav.servlet.WebDavServletComponent;
//import org.cryptomator.frontend.webdav.servlet.WebDavServletModule;


class WebDavServletFactory {

    private final WebDavServerComponent component;
    private final Collection<String> contextPaths;


    public WebDavServletFactory(WebDavServerComponent component,  Collection<String> contextPaths) {
        this.component = component;
        this.contextPaths = contextPaths;
    }


    public WebDavServletComponent create(Path rootPath, String contextPath)
    {
        WebDavServletModule webDavServletModule = new WebDavServletModule(rootPath, contextPath);
        contextPaths.add(contextPath);
        return component.newWebDavServletComponent(webDavServletModule);
    }

}