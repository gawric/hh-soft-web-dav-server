package com.WebDavSpring.WebDavSpring.WebDav.Server.servlet;

/**
 * Created by А д м и н on 07.08.2019.
 */
//import javax.inject.Inject;


import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavSession;
import org.apache.jackrabbit.webdav.DavSessionProvider;
import org.apache.jackrabbit.webdav.WebdavRequest;
import org.springframework.stereotype.Component;
//import org.cryptomator.frontend.webdav.servlet.WebDavServletModule.PerServlet;

@Component
class DavSessionProviderImpl implements DavSessionProvider
{



    @Override
    public boolean attachSession(WebdavRequest request) throws DavException
    {
        // every request gets a new session
        final DavSession session = new DavSessionImpl();
        session.addReference(request);
        request.setDavSession(session);
        return true;
    }

    @Override
    public void releaseSession(WebdavRequest request) {
        final DavSession session = request.getDavSession();
        if (session != null) {
            session.removeReference(request);
            request.setDavSession(null);
        }
    }

}