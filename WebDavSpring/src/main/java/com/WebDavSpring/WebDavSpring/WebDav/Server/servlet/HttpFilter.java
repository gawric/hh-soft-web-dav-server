package com.WebDavSpring.WebDavSpring.WebDav.Server.servlet;

/**
 * Created by А д м и н on 07.08.2019.
 */
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


interface HttpFilter extends Filter
{
    //проверка HttServelet или нет
    @Override
    default void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse)
        {
            doFilterHttp((HttpServletRequest) request, (HttpServletResponse) response, chain);
        }
        else
        {
            chain.doFilter(request, response);
        }
    }

    void doFilterHttp(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException;

}
