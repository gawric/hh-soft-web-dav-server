package com.WebDavSpring.WebDavSpring.WebDav.Server.servlet;

/**
 * Created by А д м и н on 07.08.2019.
 */

import java.util.HashSet;

import org.apache.jackrabbit.webdav.DavSession;
import org.springframework.stereotype.Component;

@Component
class DavSessionImpl implements DavSession {

    private final HashSet<String> lockTokens = new HashSet<String>();
    private final HashSet<Object> references = new HashSet<Object>();

    @Override
    public void addReference(Object reference) {
        references.add(reference);
    }

    @Override
    public void removeReference(Object reference) {
        references.remove(reference);
    }

    @Override
    public void addLockToken(String token) {
        lockTokens.add(token);
    }

    @Override
    public String[] getLockTokens() {
        return lockTokens.toArray(new String[lockTokens.size()]);
    }

    @Override
    public void removeLockToken(String token) {
        lockTokens.remove(token);
    }

}