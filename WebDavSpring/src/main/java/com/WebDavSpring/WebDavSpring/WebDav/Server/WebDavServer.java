package com.WebDavSpring.WebDavSpring.WebDav.Server;

/**
 * Created by А д м и н on 07.08.2019.
 */

import com.WebDavSpring.WebDavSpring.WebDav.Server.servlet.WebDavServletComponent;
import com.WebDavSpring.WebDavSpring.WebDav.Server.servlet.WebDavServletController;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
//import org.eclipse.jetty.Server.Server;
//import org.eclipse.jetty.Server.ServerConnector;

public class WebDavServer {



      //  private final Server Server;
      //  private final ExecutorService executorService;
      //  private final ServerConnector localConnector;
       // private final WebDavServletFactory servletFactory;
//
//        WebDavServer(Server Server, ExecutorService executorService, ServerConnector connector, WebDavServletFactory servletContextFactory)
//        {
//            this.Server = Server;
//            this.executorService = executorService;
//            this.localConnector = connector;
//            this.servletFactory = servletContextFactory;
//        }
//
//
//        public variable WebDavServer create() {
//            return create(null, null);
//        }
//
//
//        public variable WebDavServer create(String pkcs12KeystorePath, String pkcs12KeystorePassword) {
//            WebDavServerModule module = new WebDavServerModule(Optional.ofNullable(pkcs12KeystorePath), Optional.ofNullable(pkcs12KeystorePassword));
////            WebDavServerComponent comp = DaggerWebDavServerComponent.builder().webDavServerModule(module).build();
////            return comp.Server();
////        }
//
//
//        public void bind(String bindAddr, int port) {
//            this.bind(InetSocketAddress.createUnresolved(bindAddr, port));
//        }
//
//
//        public void bind(InetSocketAddress socketBindAddress) {
//            try {
//                localConnector.stop();
//                LOG.info("Binding Server socket to {}:{}", socketBindAddress.getHostString(), socketBindAddress.getPort());
//                localConnector.setHost(socketBindAddress.getHostString());
//                localConnector.setPort(socketBindAddress.getPort());
//                localConnector.start();
//            } catch (Exception e) {
//                throw new ServerLifecycleException("Failed to restart socket.", e);
//            }
//        }
//
//
//        public boolean isRunning() {
//            return Server.isRunning();
//        }
//
//
//        public synchronized void start() throws ServerLifecycleException {
//            if (executorService.isShutdown()) {
//                throw new IllegalStateException("Server has already been terminated.");
//            }
//            try {
//                Server.start();
//                LOG.info("WebDavServer started.");
//            } catch (Exception e) {
//                throw new ServerLifecycleException("Server couldn't be started", e);
//            }
//        }
//
//
//        public synchronized void stop() throws ServerLifecycleException {
//            try {
//                Server.stop();
//                LOG.info("WebDavServer stopped.");
//            } catch (Exception e) {
//                throw new ServerLifecycleException("Server couldn't be stopped", e);
//            }
//        }


        public synchronized void terminate() throws ServerLifecycleException {
           // stop();
            //executorService.shutdownNow();
        }

        //точка входа для монтирования
        //1 путь к файлу
        //2 www где будет доступен
        public WebDavServletController createWebDavServlet(Path rootPath, String contextPath)
        {
            WebDavServerComponent component = null;
            Collection<String> contextPaths = new ArrayList<>();

            WebDavServletFactory servletFactory = new WebDavServletFactory(component, contextPaths);

            WebDavServletComponent servletComp = servletFactory.create(rootPath, contextPath);
            return servletComp.servlet();
        }
}
