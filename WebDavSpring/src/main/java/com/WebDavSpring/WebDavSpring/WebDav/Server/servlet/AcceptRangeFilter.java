package com.WebDavSpring.WebDavSpring.WebDav.Server.servlet;

/**
 * Created by А д м и н on 07.08.2019.
 */

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Adds an <code>Accept-Range: bytes</code> header to all <code>GET</code> requests.
 */

@Component
@Order(5)
public class AcceptRangeFilter implements HttpFilter {



    private static final String METHOD_GET = "GET";
    private static final String HEADER_ACCEPT_RANGES = "Accept-Ranges";
    private static final String HEADER_ACCEPT_RANGE_VALUE = "bytes";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {

    }

    @Override
    public void doFilterHttp(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
       // System.out.println("diFilterHttp getMethod " + request.getMethod());
        if (METHOD_GET.equalsIgnoreCase(request.getMethod())) {
            response.addHeader(HEADER_ACCEPT_RANGES, HEADER_ACCEPT_RANGE_VALUE);
        }
        chain.doFilter(request, response);

    }

    @Override
    public void destroy() {

    }

}
