package com.WebDavSpring.WebDavSpring.WebDav.Server.servlet;

/**
 * Created by А д м и н on 07.08.2019.
 */

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Responds with status code 415, if an attempt is made to create a collection with a body.
 *
 * See https://tools.ietf.org/html/rfc2518#section-8.3.1:
 * "If the Server receives a MKCOL request entity type it does not support or understand
 * it MUST respond with a 415 (Unsupported Media Type) status code."
 */

@Component
@Order(4)
public class MkcolComplianceFilter implements HttpFilter {

    private static final Logger LOG = LoggerFactory.getLogger(MkcolComplianceFilter.class);
    private static final String METHOD_MKCOL = "MKCOL";
    private static final String HEADER_TRANSFER_ENCODING = "Transfer-Encoding";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }
    //MKCOL создание папки или файла
    @Override
    public void doFilterHttp(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        boolean hasBody = request.getContentLengthLong() > 0 || request.getHeader(HEADER_TRANSFER_ENCODING) != null;
        if (METHOD_MKCOL.equalsIgnoreCase(request.getMethod()) && hasBody) {
            LOG.warn("Blocked invalid MKCOL request to {}", request.getRequestURI());
            response.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, "MKCOL with body not supported.");
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }

}
