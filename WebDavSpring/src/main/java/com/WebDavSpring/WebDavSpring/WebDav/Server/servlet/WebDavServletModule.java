package com.WebDavSpring.WebDavSpring.WebDav.Server.servlet;

/**
 * Created by А д м и н on 07.08.2019.
 */

import java.nio.file.Path;

//import javax.inject.Qualifier;
//import javax.inject.Scope;

//import org.eclipse.jetty.servlet.ServletContextHandler;
//import org.eclipse.jetty.servlet.ServletHolder;

import com.google.common.base.CharMatcher;

//import dagger.Module;
//import dagger.Provides;

//возможно предется сделать его статическим
public class WebDavServletModule {

    private static final String WILDCARD = "/*";
//
    private final Path rootPath;
    private final String contextPath;
//
    public WebDavServletModule(Path rootPath, String contextPath) {
        String trimmedCtxPath = CharMatcher.is('/').trimTrailingFrom(contextPath);
        this.rootPath = rootPath;
        this.contextPath = trimmedCtxPath.startsWith("/") ? trimmedCtxPath : "/" + trimmedCtxPath;
    }
//
//
//    public Path provideRootPath() {
//        return rootPath;
//    }
//
//
//    public String provideContextRootUri() {
//        return contextPath;
//    }
//
//
//    public ServletContextHandler provideServletContext(WebDavServlet servlet) {
//        final ServletContextHandler servletContext = new ServletContextHandler(null, contextPath, ServletContextHandler.SESSIONS);
//        final ServletHolder servletHolder = new ServletHolder(contextPath, servlet);
//        servletContext.addServlet(servletHolder, WILDCARD);
//        servletContext.addFilter(LoggingFilter.class, WILDCARD, EnumSet.of(DispatcherType.REQUEST));
//        servletContext.addFilter(UnicodeResourcePathNormalizationFilter.class, WILDCARD, EnumSet.of(DispatcherType.REQUEST));
//        servletContext.addFilter(PostRequestBlockingFilter.class, WILDCARD, EnumSet.of(DispatcherType.REQUEST));
//        servletContext.addFilter(MkcolComplianceFilter.class, WILDCARD, EnumSet.of(DispatcherType.REQUEST));
//        servletContext.addFilter(AcceptRangeFilter.class, WILDCARD, EnumSet.of(DispatcherType.REQUEST));
//        servletContext.addFilter(MacChunkedPutCompatibilityFilter.class, WILDCARD, EnumSet.of(DispatcherType.REQUEST));
//        return servletContext;
//    }
//
//
//    @interface ContextPath {
//    }
//
//
//    @interface RootPath {
//    }
//
//
//    @interface PerServlet {
//    }

}
