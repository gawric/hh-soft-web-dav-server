package com.WebDavSpring.WebDavSpring.WebDav.Server.servlet;

/**
 * Created by А д м и н on 07.08.2019.
 */

import java.io.EOFException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;


import javax.servlet.ServletException;

import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.ObjectAuthModel;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavLocatorFactory;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavServletResponse;
import org.apache.jackrabbit.webdav.DavSession;
import org.apache.jackrabbit.webdav.DavSessionProvider;
import org.apache.jackrabbit.webdav.WebdavRequest;
import org.apache.jackrabbit.webdav.WebdavResponse;
import org.apache.jackrabbit.webdav.header.IfHeader;
import org.apache.jackrabbit.webdav.lock.ActiveLock;
import org.apache.jackrabbit.webdav.lock.Scope;
import org.apache.jackrabbit.webdav.lock.Type;
import org.apache.jackrabbit.webdav.server.AbstractWebdavServlet;
//import org.cryptomator.frontend.webdav.servlet.WebDavServletModule.PerServlet;
//import org.eclipse.jetty.io.EofException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import org.springframework.security.core.Authentication;


public class WebDavServlet extends AbstractWebdavServlet {

    private static final String NO_LOCK = "DAV:no-lock";
    private static final Logger LOG = LoggerFactory.getLogger(WebDavServlet.class);

    private final DavSessionProvider davSessionProvider;
    private final DavLocatorFactory davLocatorFactory;
    private final DavResourceFactory davResourceFactory;

    private Path rootPath;
    private ExclusiveSharedLockManager lockManager;

    private ObjectAuthModel objAuth;




    public void createResurses()
    {
         lockManager = new ExclusiveSharedLockManager();
         try
         {
             if(Paths.get(staticVariable.hrefToLocationRootFolder).toFile().exists())
             {
                 //не забываем что к этому пути еще прибавляется папка тест
                 //путь должен быть /media/hdd_tomcat/disk/admin/test
                 rootPath = Paths.get(staticVariable.hrefToLocationRootFolder);
                 //rootPath = Paths.get("D:\\XiaoMi\\XiaoMiFlash\\");
             }
             else
             {
                 Files.createDirectories(Paths.get(staticVariable.hrefToLocationRootFolder));
                 rootPath = Paths.get(staticVariable.hrefToLocationRootFolder);
             }

         }
         catch (Exception l)
         {
             System.out.println(l.toString());

         }

    }

   // public WebDavServlet(DavSessionProviderImpl davSessionProvider, DavLocatorFactoryImpl davLocatorFactory, DavResourceFactoryImpl davResourceFactory) {
    //    this.davSessionProvider = davSessionProvider = new DavSessionProviderImpl();
    //    this.davLocatorFactory = davLocatorFactory = new DavLocatorFactoryImpl();

    //    createResurses();
    //    this.davResourceFactory = davResourceFactory = new DavResourceFactoryImpl(rootPath, lockManager);
  //  }

    public WebDavServlet(ObjectAuthModel objAuth)
    {
        this.davSessionProvider  = new DavSessionProviderImpl();
        this.davLocatorFactory  = new DavLocatorFactoryImpl();
        //хранение юзеров
        this.objAuth = objAuth;
        createResurses();

        this.davResourceFactory  = new DavResourceFactoryImpl(rootPath, lockManager , objAuth);
    }

    @Override
    protected boolean isPreconditionValid(WebdavRequest request, DavResource resource)
    {
        IfHeader ifHeader = new IfHeader(request);
        if (ifHeader.hasValue() && Iterators.all(ifHeader.getAllTokens(), Predicates.equalTo(NO_LOCK)))
        {
            // https://tools.ietf.org/html/rfc4918#section-10.4.8:
            // "DAV:no-lock" is known to never represent a current lock token.
            return false;
        }
        else if (ifHeader.hasValue() && Iterators.any(ifHeader.getAllNotTokens(), Predicates.equalTo(NO_LOCK)))
        {
            // by applying "Not" to a state token that is known not to be current, the Condition always evaluates to true.
            return true;
        }
        else
        {
            return request.matchesIfHeader(resource);
        }
    }

    @Override
    public DavSessionProvider getDavSessionProvider() {

        return davSessionProvider;
    }

    @Override
    public void setDavSessionProvider(DavSessionProvider davSessionProvider) {
        throw new UnsupportedOperationException("Setting davSessionProvider not supported.");
    }

    @Override
    public DavLocatorFactory getLocatorFactory() {
        return davLocatorFactory;
    }

    @Override
    public void setLocatorFactory(DavLocatorFactory locatorFactory) {
        throw new UnsupportedOperationException("Setting locatorFactory not supported.");
    }

    @Override
    public DavResourceFactory getResourceFactory() {
        return davResourceFactory;
    }

    @Override
    public void setResourceFactory(DavResourceFactory resourceFactory) {
        throw new UnsupportedOperationException("Setting resourceFactory not supported.");
    }

	/* Unchecked DAV exception rewrapping and logging */

    @Override
    protected boolean execute(WebdavRequest request, WebdavResponse response, int method, DavResource resource) throws ServletException, IOException, DavException {
        try {
            try {
                return super.execute(request, response, method, resource);
            } catch (UncheckedDavException e) {
                throw e.toDavException();
            }
        } catch (DavException e) {
            if (e.getErrorCode() == DavServletResponse.SC_INTERNAL_SERVER_ERROR) {
                LOG.error("Unexpected DavException.", e);
            }
            throw e;
        }
    }



    @Override
    protected void doGet(WebdavRequest request, WebdavResponse response, DavResource resource) throws IOException, DavException {
        try
        {
            super.doGet(request, response, resource);
        }
        catch (EOFException e)
        {

            LOG.trace("Unexpected end of stream during GET (client hung up).");
        }
    }


    @Override
    protected int validateDestination(DavResource destResource, WebdavRequest request, boolean checkHeader) throws DavException {
        if (isLocked(destResource) && !hasCorrectLockTokens(request.getDavSession(), destResource)) {
            throw new DavException(DavServletResponse.SC_LOCKED, "The destination resource is locked");
        }
        return super.validateDestination(destResource, request, checkHeader);
    }

    @Override
    protected void doPut(WebdavRequest request, WebdavResponse response, DavResource resource) throws IOException, DavException {
        if (isLocked(resource) && !hasCorrectLockTokens(request.getDavSession(), resource)) {
            throw new DavException(DavServletResponse.SC_LOCKED, "The resource is locked");
        }
        super.doPut(request, response, resource);
    }

    @Override
    protected void doDelete(WebdavRequest request, WebdavResponse response, DavResource resource) throws IOException, DavException {
        if (isLocked(resource) && !hasCorrectLockTokens(request.getDavSession(), resource)) {
            throw new DavException(DavServletResponse.SC_LOCKED, "The resource is locked");
        }
        super.doDelete(request, response, resource);
    }

    @Override
    protected void doMove(WebdavRequest request, WebdavResponse response, DavResource resource) throws IOException, DavException {
        if (isLocked(resource) && !hasCorrectLockTokens(request.getDavSession(), resource)) {
            throw new DavException(DavServletResponse.SC_LOCKED, "The source resource is locked");
        }
        super.doMove(request, response, resource);
    }

    @Override
    protected void doPropPatch(WebdavRequest request, WebdavResponse response, DavResource resource) throws IOException, DavException {
        if (isLocked(resource) && !hasCorrectLockTokens(request.getDavSession(), resource)) {
            throw new DavException(DavServletResponse.SC_LOCKED, "The resource is locked");
        }
        super.doPropPatch(request, response, resource);
    }

    //срабатывает при копировании файла
    private boolean hasCorrectLockTokens(DavSession session, DavResource resource) {
        boolean access = false;

        final Set<String> providedLockTokens = ImmutableSet.copyOf(session.getLockTokens());
        for (ActiveLock lock : resource.getLocks()) {
            access |= providedLockTokens.contains(lock.getToken());
        }
        return access;
    }

    private boolean isLocked(DavResource resource) {
        return resource.hasLock(Type.WRITE, Scope.EXCLUSIVE) || resource.hasLock(Type.WRITE, Scope.SHARED);
    }

}