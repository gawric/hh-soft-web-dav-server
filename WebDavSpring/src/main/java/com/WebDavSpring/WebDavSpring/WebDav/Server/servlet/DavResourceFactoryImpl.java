package com.WebDavSpring.WebDavSpring.WebDav.Server.servlet;

/**
 * Created by А д м и н on 07.08.2019.
 */

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;

//import javax.inject.Inject;

import com.WebDavSpring.WebDavSpring.configuration.variable.staticVariable;
import com.WebDavSpring.WebDavSpring.model.ObjectAuthModel;
import com.WebDavSpring.WebDavSpring.model.webDavModel.WebDavLocator;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceAuthUser;
import com.WebDavSpring.WebDavSpring.services.Interface.ServiceUser;
import com.google.common.collect.ImmutableSet;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavMethods;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.DavServletRequest;
import org.apache.jackrabbit.webdav.DavServletResponse;
import org.apache.jackrabbit.webdav.DavSession;
import org.apache.jackrabbit.webdav.lock.LockManager;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
//import org.cryptomator.frontend.webdav.servlet.ByteRange.MalformedByteRangeException;
//import org.cryptomator.frontend.webdav.servlet.ByteRange.UnsupportedRangeException;
//import org.cryptomator.frontend.webdav.servlet.WebDavServletModule.PerServlet;
//import org.cryptomator.frontend.webdav.servlet.WebDavServletModule.RootPath;
////import org.eclipse.jetty.http.HttpHeader;

//import com.google.common.collect.ImmutableSet;


public class DavResourceFactoryImpl implements DavResourceFactory {

    private  Path rootPath;
    private  LockManager lockManager;
    private  ObjectAuthModel objAuth;

    public  DavResourceFactoryImpl(Path rootPath, ExclusiveSharedLockManager lockManager , ObjectAuthModel objAuth) {
        this.rootPath = rootPath;
        this.lockManager = lockManager;
        this.objAuth = objAuth;
    }

    @Override
    public DavResource createResource(DavResourceLocator locator, DavServletRequest request, DavServletResponse response) throws DavException
    {
        boolean roleUser  = objAuth.getAuthUser().isUser();
        boolean roleAdmin  = objAuth.getAuthUser().isAdmin();
        String currentUsername = objAuth.getAuthUser().currentUsername();

        WebDavLocator webDavLocator = createLocatorWebDav( locator,  request,  response);
        String resourcesPath =  locator.getResourcePath();

        if(!isUserPath( resourcesPath ,  currentUsername)) throw new IllegalArgumentException("User error webdavnamespace " + webDavLocator.getLocator().getClass());

        //юзер иди админ
        if(roleUser )
        {

            //проверка правильно ли передали resource path или юзер ввел правильный путь userwebdav, а то может попробовать ввести adminwebdav
            //и попытаться получить доступ к файлам админа
            boolean check = checkRoleLocator(staticVariable.userLocatorWebDav , resourcesPath);
            return returnLocator( check ,  webDavLocator);
        }
        else
        {
            if(roleAdmin)
            {
                boolean check = checkRoleLocator(staticVariable.adminLocatorWebDav , resourcesPath);
                return returnLocator( check ,  webDavLocator);
            }

            boolean check = checkRoleLocator(staticVariable.adminLocatorWebDav , resourcesPath);
            return returnLocator( check ,  webDavLocator);
        }

    }
    //проверяет разрешено ли юзеру доступ к этой папке
    private Boolean isUserPath(String resourcesPath , String userName)
    {
        Boolean check = false;


        int index = resourcesPath.indexOf(userName);

        String searchVariable = staticVariable.WEBDAVPUBLIC;
        int indexPublic = resourcesPath.indexOf(searchVariable);



        if(index != -1)check = true;
        if(indexPublic != -1)check = true;

        return check;
    }


    private DavResource returnLocator(boolean checkRoleLocator , WebDavLocator webDavLocator) throws DavException {
        if(checkRoleLocator)
        {
            return getResources(webDavLocator.getLocator(), webDavLocator.getRequest(),  webDavLocator.getResponse());
        }
        else
        {

            throw new IllegalArgumentException("ResourcesPath locator of not Found " + webDavLocator.getLocator().getClass());
        }

    }

    private WebDavLocator createLocatorWebDav(DavResourceLocator locator, DavServletRequest request, DavServletResponse response)
    {
        WebDavLocator webDavLocator = new WebDavLocator();
        webDavLocator.setLocator(locator);
        webDavLocator.setRequest(request);
        webDavLocator.setResponse(response);

        return webDavLocator;
    }
    private boolean checkRoleLocator(String locatorWebDav , String resourcePath){


        if(resourcePath.lastIndexOf(locatorWebDav) > -1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private DavResource getResources(DavResourceLocator locator, DavServletRequest request, DavServletResponse response) throws DavException
    {
        if (locator instanceof DavLocatorImpl && locator.equals(request.getRequestLocator()))
        {
            return createRequestResource((DavLocatorImpl) locator, request, response);
        }
        else if (locator instanceof DavLocatorImpl && locator.equals(request.getDestinationLocator()))
        {
            return createDestinationResource((DavLocatorImpl) locator, request, response);
        }
        else
        {
            throw new IllegalArgumentException("Unsupported locator of type " + locator.getClass());
        }
    }

    //проверяет существует ли папка где буду жить все webDav клиенты
    //т.е /opt/media/resourses/webdavclient/test/ - результат проверки возвращает в responce
    private DavResource createRequestResource(DavLocatorImpl locator, DavServletRequest request, DavServletResponse response) throws DavException
    {
        assert locator.equals(request.getRequestLocator());

        Path p = rootPath.resolve(locator.getResourcePath());
        Optional<BasicFileAttributes> attr = readBasicFileAttributes(p);

        if (!attr.isPresent() && DavMethods.METHOD_PUT.equals(request.getMethod()))
        {
            return createFile(locator, p, Optional.empty(), request.getDavSession());

        } else if (!attr.isPresent() && DavMethods.METHOD_MKCOL.equals(request.getMethod()))
        {
            return createFolder(locator, p, Optional.empty(), request.getDavSession());
        }
        else if (!attr.isPresent() && DavMethods.METHOD_LOCK.equals(request.getMethod()))
        {
            // locking non-existing resources must create a non-collection resource:
            // https://tools.ietf.org/html/rfc4918#section-9.10.4
            // See also: DavFile#lock(...)
            return createFile(locator, p, Optional.empty(), request.getDavSession());
        }
        else if (!attr.isPresent())
        {
            throw new DavException(DavServletResponse.SC_NOT_FOUND);
        } else if (attr.get().isDirectory())
        {
            return createFolder(locator, p, attr, request.getDavSession());
        }
        else if (DavMethods.METHOD_GET.equals(request.getMethod()) && request.getHeader(HttpHeaders.RANGE.toString()) != null)
        {
            return createFileRange(locator, p, attr.get(), request.getDavSession(), request, response);
        }
        else
            {
            return createFile(locator, p, attr, request.getDavSession());
        }
    }

    private DavResource createDestinationResource(DavLocatorImpl locator, DavServletRequest request, DavServletResponse response) throws DavException {
        assert locator.equals(request.getDestinationLocator());
        assert ImmutableSet.of(DavMethods.METHOD_MOVE, DavMethods.METHOD_COPY).contains(request.getMethod());
        Path srcP = rootPath.resolve(request.getRequestLocator().getResourcePath());
        Path dstP = rootPath.resolve(locator.getResourcePath());
        Optional<BasicFileAttributes> srcAttr = readBasicFileAttributes(srcP);
        Optional<BasicFileAttributes> dstAttr = readBasicFileAttributes(dstP);
        if (!srcAttr.isPresent()) {
            throw new DavException(DavServletResponse.SC_NOT_FOUND);
        } else if (srcAttr.get().isDirectory()) {
            return createFolder(locator, dstP, dstAttr, request.getDavSession());
        } else {
            return createFile(locator, dstP, dstAttr, request.getDavSession());
        }
    }

    @Override
    public DavResource createResource(DavResourceLocator locator, DavSession session) throws DavException {
        if (locator instanceof DavLocatorImpl) {
            return createResourceInternal((DavLocatorImpl) locator, session);
        } else {
            throw new IllegalArgumentException("Unsupported locator of type " + locator.getClass());
        }
    }

    private DavResource createResourceInternal(DavLocatorImpl locator, DavSession session) throws DavException {
        Path p = rootPath.resolve(locator.getResourcePath());
        Optional<BasicFileAttributes> attr = readBasicFileAttributes(p);
        if (!attr.isPresent()) {
            throw new DavException(DavServletResponse.SC_NOT_FOUND);
        } else if (attr.get().isDirectory()) {
            return createFolder(locator, p, attr, session);
        } else {
            return createFile(locator, p, attr, session);
        }
    }

    /**
     * @return BasicFileAttributes or {@link Optional#empty()} if the file/folder for the given path does not exist.
     * @throws DavException If an {@link IOException} occured during {@link Files#readAttributes(Path, Class, java.nio.file.LinkOption...)}.
     */
    private Optional<BasicFileAttributes> readBasicFileAttributes(Path path) throws DavException
    {
        try
        {
            return Optional.of(Files.readAttributes(path, BasicFileAttributes.class));

        } catch (NoSuchFileException e)
        {
            return Optional.empty();
        } catch (IOException e)
        {
            throw new DavException(DavServletResponse.SC_INTERNAL_SERVER_ERROR, e);
        }
    }
    //срабатывает при создании файла или папки
    DavFolder createFolder(DavLocatorImpl locator, Path path, Optional<BasicFileAttributes> attr, DavSession session) {
        return new DavFolder(this, lockManager, locator, path, attr, session);
    }

    DavFile createFile(DavLocatorImpl locator, Path path, Optional<BasicFileAttributes> attr, DavSession session) {
        return new DavFile(this, lockManager, locator, path, attr, session);
    }

    private DavFile createFileRange(DavLocatorImpl locator, Path path, BasicFileAttributes attr, DavSession session, DavServletRequest request, DavServletResponse response) throws DavException {
        // 200 for "normal" resources, if if-range is not satisified:
        final String ifRangeHeader = request.getHeader(HttpHeaders.IF_RANGE.toString());
        if (!isIfRangeHeaderSatisfied(attr, ifRangeHeader)) {
            return createFile(locator, path, Optional.of(attr), session);
        }

        final String rangeHeader = request.getHeader(HttpHeaders.RANGE.toString());
        try {
            // 206 for ranged resources:
            final ByteRange byteRange = ByteRange.parse(rangeHeader);
            response.setStatus(DavServletResponse.SC_PARTIAL_CONTENT);
            return new DavFileWithRange(this, lockManager, locator, path, attr, session, byteRange);
        } catch (ByteRange.UnsupportedRangeException ex) {
            return createFile(locator, path, Optional.of(attr), session);
        } catch (ByteRange.MalformedByteRangeException e) {
            throw new DavException(DavServletResponse.SC_BAD_REQUEST, "Malformed range header: " + rangeHeader);
        }
    }

    /**
     * @return <code>true</code> if a partial response should be generated according to an If-Range precondition.
     */
    private boolean isIfRangeHeaderSatisfied(BasicFileAttributes attr, String ifRangeHeader) throws DavException {
        if (ifRangeHeader == null) {
            // no header set -> satisfied implicitly
            return true;
        } else {
            try {
                Instant expectedTime = Instant.from(DateTimeFormatter.RFC_1123_DATE_TIME.parse(ifRangeHeader));
                Instant actualTime = attr.lastModifiedTime().toInstant();
                return expectedTime.compareTo(actualTime) == 0;
            } catch (DateTimeParseException e) {
                throw new DavException(DavServletResponse.SC_BAD_REQUEST, "Unsupported If-Range header: " + ifRangeHeader);
            }
        }
    }

}