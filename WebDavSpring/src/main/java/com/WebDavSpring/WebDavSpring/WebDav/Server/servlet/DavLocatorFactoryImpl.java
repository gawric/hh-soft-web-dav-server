package com.WebDavSpring.WebDavSpring.WebDav.Server.servlet;

/**
 * Created by А д м и н on 07.08.2019.
 */

//import javax.inject.Inject;

import org.apache.jackrabbit.webdav.DavLocatorFactory;
import org.apache.jackrabbit.webdav.util.EncodeUtil;
//import org.cryptomator.frontend.webdav.servlet.WebDavServletModule.PerServlet;



class DavLocatorFactoryImpl implements DavLocatorFactory {


    public DavLocatorFactoryImpl() {
    }

    //основная ошибка не парвильно вычисляет папку куда складывать файлы
    //из -за Spring помечаем папку d:/xiaomi/xiaomi
    //возвращает D:/xiaomi/xiaomi/test <- test - это префикс заапроса(глючит только в spring)
    @Override
    public DavLocatorImpl createResourceLocator(String prefix, String href) {
        final String canonicalPrefix = prefix.endsWith("/") ? prefix : prefix + "/";
        final String canonicalHref = href.startsWith("/") ? href.substring(1) : href;
        final String hrefWithoutPrefix = canonicalHref.startsWith(canonicalPrefix) ? canonicalHref.substring(canonicalPrefix.length()) : canonicalHref;
        final String resourcePath = EncodeUtil.unescape(hrefWithoutPrefix);
        return createResourceLocator(canonicalPrefix, null, resourcePath);
    }

    @Override
    public DavLocatorImpl createResourceLocator(String prefix, String workspacePath, String resourcePath) {
        return new DavLocatorImpl(this, prefix, resourcePath);
    }

    @Override
    public DavLocatorImpl createResourceLocator(String prefix, String workspacePath, String path, boolean isResourcePath) {
        // ignore isResourcePath. This impl doesn't distinguish resourcePath and repositoryPath.
        return createResourceLocator(prefix, workspacePath, path);
    }

}