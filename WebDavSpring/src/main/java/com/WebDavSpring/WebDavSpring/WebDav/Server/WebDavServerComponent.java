package com.WebDavSpring.WebDavSpring.WebDav.Server;

import com.WebDavSpring.WebDavSpring.WebDav.Server.servlet.WebDavServletComponent;
import com.WebDavSpring.WebDavSpring.WebDav.Server.servlet.WebDavServletModule;

/**
 * Created by А д м и н on 07.08.2019.
 */
//import javax.inject.Singleton;

//import org.cryptomator.frontend.webdav.servlet.WebDavServletComponent;
//import org.cryptomator.frontend.webdav.servlet.WebDavServletModule;

//import dagger.Component;


interface WebDavServerComponent {

    WebDavServer server();

    WebDavServletComponent newWebDavServletComponent(WebDavServletModule webDavServletModule);

}
