package com.WebDavSpring.WebDavSpring.WebDav.Server;


public class ServerLifecycleException extends RuntimeException {

    public ServerLifecycleException(String message, Throwable cause) {
        super(message, cause);
    }

}
