package com.WebDavSpring.WebDavSpring;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@EnableJpaRepositories("com.WebDavSpring.WebDavSpring.repository")
@ComponentScan(basePackages = "com.WebDavSpring.*")
public class WebDavSpringApplication {

		public static void main(String[] args)
		{
			SpringApplication.run(WebDavSpringApplication.class, args);
		}
}
