# Описание #

Сервер для клиента webdavserver https://bitbucket.org/gawric/hh-soft-oblachnyi-klient-failov/src/master/

### Используемые библиотеки ###

* Apache Jackrabbit
* Apache ActiveMq
* Spring boot 2.1.7
* Spring Security
* Spring Data
* Hibernate
* Jackson data bind
* Json web token
* Maven 3.8.6
* Mysql
* Intellij idea

### Реализовано ###

* Сканирование диска для пользователя
* Получение через MQ клиента новых данных
* Синхронизация данных с Mysql
* Проверка логина и пароля
* Отправка уведомлений на email администратора
* Соединение jack rabbit и spring
* Хранение данных(после сканирования) в mysql
* Временное хранение(после переподключения) в mysql
* Все запросы синхронизируется через FIFO 
* Оптимизированны запросы для mysql (минимум задержек)


### связаться с нами ###

* admin@hh-soft.ru
